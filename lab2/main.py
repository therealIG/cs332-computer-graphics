import numpy as np
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene
from PyQt5 import QtCore, QtGui, uic
import cv2 as cv
import matplotlib.pyplot as plt
from PyQt5.QtGui import QImage, QPixmap
from mpl_toolkits.mplot3d import Axes3D
import os
from time import strftime, localtime


qtCreatorFile = "design.ui"


Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self._hsv_shifts = np.array([0, 0, 0])
        self.pushButton.clicked.connect(self.prepare)
        self.saturation_value.setPlainText('0')
        self.hue_value.setPlainText('0')
        self.brightness_value.setPlainText('0')

        scene = QGraphicsScene()
        pixmap = QtGui.QPixmap('lena_std.tif')
        scene.addPixmap(pixmap)
        self.graphicsView_1.setScene(scene)
        self.graphicsView_1.show()

    def prepare(self):
        img = cv.imread('lena_std.tif')
        argument_saturation = int(self.saturation_value.toPlainText())
        argument_brightness = int(self.brightness_value.toPlainText())
        argument_hue = int(self.hue_value.toPlainText())
        self.task3(img, argument_saturation, argument_brightness, argument_hue)

    def task3(self, img, argument_saturation, argument_brightness, argument_hue):
        self.statusBar().showMessage('processing started!')

        hsvimg = rbg2hsv(img)
        hsvimg = handler(hsvimg, argument_hue, argument_saturation, argument_brightness)
        hsvimg = cv.cvtColor(hsvimg, cv.COLOR_HSV2RGB)

        self._hsv_shifts = np.array([argument_hue, argument_saturation, argument_brightness])

        # displaying RGB image after processing
        scene1 = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(hsvimg.data, hsvimg.shape[1], hsvimg.shape[0], hsvimg.strides[0], QImage.Format_RGB888))
        scene1.addPixmap(pixmap1)
        self.graphicsView_2.setScene(scene1)
        self.graphicsView_2.show()
        task2(cv.cvtColor(hsvimg, cv.COLOR_RGB2BGR), save=False)
        self.statusBar().clearMessage()


def handler(hsv: np.ndarray, h: int, s: int, v: int):
    """
    update HSV with gotten constant
    :param hsv: image as HSV, numpy array
    :param h: hue shift, in range from -180 to 180
    :param s: saturation shift, in range from -100 to 100
    :param v: value shift, in range from -100 to 100
    :return: image as HSV, numpy array
    """

    return np.apply_along_axis(lambda x: np.array([
        (x[0] + h) % 360,
        np.max([0, np.min([255, x[1] + s])]),
        np.max([0, np.min([255, x[2] + v])])
    ]), -1, hsv).astype(np.uint8)


def convert(rgb: np.ndarray):
    """

    :param rgb:
    :return:
    """
    cmax = max(rgb)
    cmin = min(rgb)

    v = cmax * 255

    if cmax != 0:
        s = 1 - cmin / cmax
    else:
        s = 0
    s = s * 255

    if cmax == cmin:
        h = 0
    elif cmax == rgb[0] and rgb[1] >= rgb[2]:
        h = 60 * (rgb[1] - rgb[2]) / (cmax - cmin)
    elif cmax == rgb[0] and rgb[1] < rgb[2]:
        h = 60 * (rgb[1] - rgb[2]) / (cmax - cmin) + 360
    elif cmax == rgb[1]:
        h = 60 * (rgb[2] - rgb[0]) / (cmax - cmin) + 120
    else:
        h = 60 * (rgb[0] - rgb[1]) / (cmax - cmin) + 240
    h = h / 2

    return np.array([h, s, v])


def rbg2hsv(image: np.ndarray):
    """
    convert BGR to HSV
    :param image: BGR image as numpy array
    :return: HSV image as numpy array
    """
    image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
    return np.round(np.apply_along_axis(convert, -1, image/255.)).astype(np.uint8)


def average_grayscale(img):
    """
    transforms BGR image to grayscale with average method
    :param img: CVImage
    :return: grayscale image
    """
    return np.average(img, axis=2)


def weighted_grayscale(img, r_weight, g_weight, b_weight):
    """
    transforms BGR image to grayscale with average method
    :param img: CVImage
    :param r_weight: double
    :param g_weight: double
    :param b_weight: double
    :return: grayscale image
    """
    b_channel = img[:, :, 0] * b_weight
    g_channel = img[:, :, 1] * g_weight
    r_channel = img[:, :, 2] * r_weight
    return b_channel + g_channel + r_channel


def hist3d(hist: np.ndarray) -> (np.ndarray, np.ndarray, np.ndarray):
    """
    Preparing 3D histogram to visualizing
    :param hist: calculated over all channels histogram as numpy array
    :return: x, y and z axis data as numpy array
    """
    #
    # Create an X-Y mesh of the same dimension as the 2D data. You can
    # think of this as the floor of the plot.
    #
    x_data, y_data = np.meshgrid(np.arange(hist.shape[1]),
                                 np.arange(hist.shape[0]))
    #
    # Flatten out the arrays so that they may be passed to "ax.bar3d".
    # Basically, ax.bar3d expects three one-dimensional arrays:
    # x_data, y_data, z_data. The following call boils down to picking
    # one entry from each array and plotting a bar to from
    # (x_data[i], y_data[i], 0) to (x_data[i], y_data[i], z_data[i]).
    #
    x_data = x_data.flatten()
    y_data = y_data.flatten()
    z_data = hist.flatten()

    return x_data, y_data, z_data


def task1(img, r_weight, g_weight, b_weight):
    """
    implements task1
    :param img: CVImage
    :param r_weight: double
    :param g_weight: double
    :param b_weight: double
    :return: histogram image
    """
    average_gray = average_grayscale(img)
    weighted_gray = weighted_grayscale(img, r_weight, g_weight, b_weight)
    diff = weighted_gray - average_gray
    diff += abs(np.min(diff)) if np.min(diff) < 0 else 0
    diff = diff.astype(np.uint8)
    cv.imshow('average', average_gray.astype(np.uint8))
    cv.waitKey(0)
    cv.imshow('weighted', weighted_gray.astype(np.uint8))
    cv.waitKey(0)
    cv.imshow('diff', diff)
    cv.waitKey(0)
    plt.hist(diff)
    plt.savefig('./data/task1.png')


def task2(img: np.ndarray, save: bool = True) -> None:
    """
    implements task2
    :param img: CVImage
    :param save:
    :return: None
    """
    b_channel = img.copy()
    b_channel[:, :, 1:] = np.zeros_like(b_channel[:, :, 1:])
    cv.imwrite('./data/task2_bchannel.png', b_channel)

    g_channel = img.copy()
    g_channel[:, :, 0] = np.zeros_like(g_channel[:, :, 0])
    g_channel[:, :, 2] = np.zeros_like(g_channel[:, :, 2])
    cv.imwrite('./data/task2_gchannel.png', g_channel)

    r_channel = img.copy()
    r_channel[:, :, :2] = np.zeros_like(r_channel[:, :, :2])
    cv.imwrite('./data/task2_rchannel.png', r_channel)

    bins = 256
    b_frq, b_edges = np.histogram(img[:, :, 0], bins)
    g_frq, g_edges = np.histogram(img[:, :, 1], bins)
    r_frq, r_edges = np.histogram(img[:, :, 2], bins)
    hist = np.concatenate([
        np.expand_dims(b_frq, -1),
        np.expand_dims(g_frq, -1),
        np.expand_dims(r_frq, -1)
    ], -1)

    fig, ax = plt.subplots(2, 2, figsize=(20, 10))
    ax3d = fig.add_subplot(224, projection='3d')
    fig.canvas.set_window_title("Histograms " + strftime("%Y-%m-%d %H:%M:%S", localtime()))
    fig.tight_layout()

    ax[0, 0].bar(b_edges[:-1], b_frq, align="edge", width=1.2, ec='k', color="blue")
    ax[0, 0].set_title('Blue channel')
    ax[0, 1].bar(g_edges[:-1], g_frq, align="center", width=1.2, ec='k', color="green")
    ax[0, 1].set_title('Green channel')
    ax[1, 0].bar(r_edges[:-1], r_frq, align="center", width=1.2, ec='k', color="red")
    ax[1, 0].set_title('Red channel')
    x_data, y_data, z_data = hist3d(hist)
    colors = ['b', 'g', 'r']
    for i in range(y_data.shape[0]):
        ax3d.bar3d(x_data[i], y_data[i], np.zeros(len(z_data))[i], 1, 1, z_data[i],
                   alpha=0.6, color=colors[i % 3])
    ax3d.set_title('3D histogram')

    if save:
        plt.savefig("./data/task2_hists.png")
    plt.show()


def main():
    if not os.path.exists("./data"):
        os.mkdir("./data")
    img = cv.imread('lena_std.tif')
    r_weight = 0.25
    g_weight = 0.35
    b_weight = 0.4
    task1(img, r_weight, g_weight, b_weight)
    # task2(img)


if __name__ == '__main__':
    main()
    app = QApplication(sys.argv)
    window = MyApp()
    window.setFixedSize(1086, 645)
    window.show()
    sys.exit(app.exec_())


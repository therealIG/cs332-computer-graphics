import sys
import ctypes
import numpy as np
import OpenGL.GL as gl
import OpenGL.GLUT as glut
import OpenGL.GLU as glu
import pygame
from openGL.lab12.task1 import read_shader

x_rot = 0.0
y_rot = 0.0
z_rot = 0.0
width_ = 800
height_ = 400
# shape_color = [0.0, 0.0, 0.8, 0.9]
shape_color = None
mix_ratio = 0.5

vertex_code = read_shader('./single_texture_vertex')
fragment_code = read_shader('./double_texture_fragment')


def load_texture(path):
    img = pygame.image.load(path)
    img_data = pygame.image.tostring(img, 'RGBA', 1)
    width, height = img.get_size()

    texture = gl.glGenTextures(1)
    gl.glBindTexture(gl.GL_TEXTURE_2D, texture)
    gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR_MIPMAP_LINEAR)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
    gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA, width, height, 0, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, img_data)
    gl.glGenerateMipmap(gl.GL_TEXTURE_2D)
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
    return texture


def display():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    glu.gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()
    glu.gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    gl.glRotatef(x_rot, 1.0, 0.0, 0.0)
    gl.glRotatef(y_rot, 0.0, 1.0, 0.0)
    gl.glRotatef(z_rot, 0.0, 0.0, 1.0)

    gl.glDrawArrays(gl.GL_TRIANGLES, 0, 12*3)
    glut.glutSwapBuffers()


def reshape(width, height):
    gl.glViewport(0, 0, width, height)


def keyboard(key, x, y):
    global x_rot, y_rot, z_rot
    if key == b'\x1b':
        sys.exit()
    if key == glut.GLUT_KEY_UP:
        x_rot += 2.0
    if key == glut.GLUT_KEY_DOWN:
        x_rot -= 2.0
    if key == glut.GLUT_KEY_LEFT:
        y_rot += 2.0
    if key == glut.GLUT_KEY_RIGHT:
        y_rot -= 2.0
    if key == glut.GLUT_KEY_HOME:
        z_rot += 2.0
    if key == glut.GLUT_KEY_END:
        z_rot -= 2.0
    if key == glut.GLUT_KEY_INSERT:
        glut.glutLeaveMainLoop()
    glut.glutPostRedisplay()


glut.glutInit()
glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGBA | glut.GLUT_DEPTH)
glut.glutCreateWindow('textures!')
glut.glutReshapeWindow(width_, height_)
glut.glutReshapeFunc(reshape)
glut.glutDisplayFunc(display)
glut.glutIdleFunc(display)
glut.glutSpecialFunc(keyboard)

data = np.zeros(12*3, [("position", np.float32, 3),
                       ("tex_pos", np.float32, 2)])
vertexes = [(-1., -1., -1.), (-1., -1., 1.), (1., -1., 1.), (1., -1., -1.),
            (-1., 1., -1.), (-1., 1., 1.), (1., 1., 1.), (1., 1., -1.)]
tex_vertexes_coords = [(0, 0), (0, 1), (1, 1), (1, 0)]
data['position'] = [vertexes[0], vertexes[1], vertexes[2],
                    vertexes[0], vertexes[3], vertexes[2],
                    # -----
                    vertexes[0], vertexes[1], vertexes[5],
                    vertexes[0], vertexes[4], vertexes[5],
                    # -----
                    vertexes[0], vertexes[4], vertexes[7],
                    vertexes[0], vertexes[3], vertexes[7],
                    # -----
                    # -----------------------------------
                    vertexes[6], vertexes[7], vertexes[4],
                    vertexes[6], vertexes[5], vertexes[4],
                    # -----
                    vertexes[6], vertexes[5], vertexes[1],
                    vertexes[6], vertexes[2], vertexes[1],
                    # -----
                    vertexes[6], vertexes[2], vertexes[3],
                    vertexes[6], vertexes[7], vertexes[3],
                    # -----
                    ]
data['tex_pos'] = [tex_vertexes_coords[0], tex_vertexes_coords[1], tex_vertexes_coords[2],
                   tex_vertexes_coords[0], tex_vertexes_coords[3], tex_vertexes_coords[2],
                   # -----
                   tex_vertexes_coords[0], tex_vertexes_coords[1], tex_vertexes_coords[2],
                   tex_vertexes_coords[0], tex_vertexes_coords[3], tex_vertexes_coords[2],
                   # -----
                   tex_vertexes_coords[0], tex_vertexes_coords[1], tex_vertexes_coords[2],
                   tex_vertexes_coords[0], tex_vertexes_coords[3], tex_vertexes_coords[2],
                   # -----
                   # --------------------------------------------------------------------
                   tex_vertexes_coords[0], tex_vertexes_coords[1], tex_vertexes_coords[2],
                   tex_vertexes_coords[0], tex_vertexes_coords[3], tex_vertexes_coords[2],
                   # -----
                   tex_vertexes_coords[0], tex_vertexes_coords[1], tex_vertexes_coords[2],
                   tex_vertexes_coords[0], tex_vertexes_coords[3], tex_vertexes_coords[2],
                   # -----
                   tex_vertexes_coords[0], tex_vertexes_coords[1], tex_vertexes_coords[2],
                   tex_vertexes_coords[1], tex_vertexes_coords[2], tex_vertexes_coords[3],
                   # -----
                   ]

program = gl.glCreateProgram()
vertex = gl.glCreateShader(gl.GL_VERTEX_SHADER)
fragment = gl.glCreateShader(gl.GL_FRAGMENT_SHADER)

gl.glShaderSource(vertex, vertex_code)
gl.glShaderSource(fragment, fragment_code)

gl.glCompileShader(vertex)
if not gl.glGetShaderiv(vertex, gl.GL_COMPILE_STATUS):
    error = gl.glGetShaderInfoLog(vertex).decode()
    print(error)
    raise RuntimeError("Shader compilation error")

gl.glCompileShader(fragment)
gl.glCompileShader(fragment)
if not gl.glGetShaderiv(fragment, gl.GL_COMPILE_STATUS):
    error = gl.glGetShaderInfoLog(fragment).decode()
    print(error)
    raise RuntimeError("Shader compilation error")
gl.glAttachShader(program, vertex)
gl.glAttachShader(program, fragment)

gl.glLinkProgram(program)
if not gl.glGetProgramiv(program, gl.GL_LINK_STATUS):
    print(gl.glGetProgramInfoLog(program))
    raise RuntimeError('Linking error')

gl.glDetachShader(program, vertex)
gl.glDetachShader(program, fragment)

gl.glUseProgram(program)

buffer = gl.glGenBuffers(1)

gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buffer)

gl.glBufferData(gl.GL_ARRAY_BUFFER, data.nbytes, data, gl.GL_DYNAMIC_DRAW)

stride = data.strides[0]
offset = ctypes.c_void_p(0)
loc = gl.glGetAttribLocation(program, "position")
gl.glEnableVertexAttribArray(loc)
gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buffer)
gl.glVertexAttribPointer(loc, 3, gl.GL_FLOAT, False, stride, offset)

offset = ctypes.c_void_p(data.dtype["position"].itemsize)
loc = gl.glGetAttribLocation(program, "tex_pos")
gl.glEnableVertexAttribArray(loc)
gl.glBindBuffer(gl.GL_ARRAY_BUFFER, buffer)
gl.glVertexAttribPointer(loc, 4, gl.GL_FLOAT, False, stride, offset)

texture = load_texture("./tex.jpg")
loc_tex1 = gl.glGetUniformLocation(program, "texture")
gl.glUniform1i(loc_tex1, 0)

texture2 = load_texture("./tex2.jpg")
loc_tex2 = gl.glGetUniformLocation(program, "texture2")

if loc_tex2 != -1:
    gl.glUniform1i(loc_tex2, 1)

gl.glActiveTexture(gl.GL_TEXTURE0)
gl.glBindTexture(gl.GL_TEXTURE_2D, texture)

gl.glActiveTexture(gl.GL_TEXTURE1)
gl.glBindTexture(gl.GL_TEXTURE_2D, texture2)

loc = gl.glGetUniformLocation(program, "ratio")
if loc != -1:
    gl.glUniform1f(loc, mix_ratio)

loc_color = gl.glGetUniformLocation(program, "color")
if loc_color != -1 and shape_color is not None:
    gl.glUniform4f(loc_color, shape_color[0], shape_color[1], shape_color[2], shape_color[3])


gl.glEnable(gl.GL_DEPTH_TEST)
glut.glutMainLoop()

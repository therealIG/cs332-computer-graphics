from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import numpy as np
from PIL import Image

width_ = 800
height_ = 400
xrot = 0.0
yrot = 0.0
zrot = 0.0
xscale = 1.9
yscale = 1.0
shape_color = (0.6, 0.8, 0.3, 1.0)
background_color = (1., 0., 0., 1.)
hatched_color = (0., 0., 1., 1.)
hatch_width = 15.  # 0 < X < 100
interline_width = 30.  # 0 < X < hatch_width
hatching_dir = 0  # 0 - horizontal, 1 - vertical
angle = 45


cube = [[[-1.0, -1.0, -1.0], [-1.0, -1.0, 1.0], [-1.0, 1.0, 1.0]],
        [[1.0, 1.0, -1.0], [-1.0, -1.0, -1.0], [-1.0, 1.0, -1.0]],
        [[1.0, -1.0, 1.0], [-1.0, -1.0, -1.0], [1.0, -1.0, -1.0]],
        [[1.0, 1.0, -1.0], [1.0, -1.0, -1.0], [-1.0, -1.0, -1.0]],
        [[-1.0, -1.0, -1.0], [-1.0, 1.0, 1.0], [-1.0, 1.0, -1.0]],
        [[1.0, -1.0, 1.0], [-1.0, -1.0, 1.0], [-1.0, -1.0, -1.0]],
        [[-1.0, 1.0, 1.0], [-1.0, -1.0, 1.0], [1.0, -1.0, 1.0]],
        [[1.0, 1.0, 1.0], [1.0, -1.0, -1.0], [1.0, 1.0, -1.0]],
        [[1.0, -1.0, -1.0], [1.0, 1.0, 1.0], [1.0, -1.0, 1.0]],
        [[1.0, 1.0, 1.0], [1.0, 1.0, -1.0], [-1.0, 1.0, -1.0]],
        [[1.0, 1.0, 1.0], [-1.0, 1.0, -1.0], [-1.0, 1.0, 1.0]],
        [[1.0, 1.0, 1.0], [-1.0, 1.0, 1.0], [1.0, -1.0, 1.0]]]


def load_texture(filename: str):
    image = Image.open(filename)
    try:
        ix, iy, image_bytes = image.size[0], image.size[1], image.tostring("raw", "RGBA", 0, -1)
    except SystemError:
        ix, iy, image_bytes = image.size[0], image.size[1], image.tostring("raw", "RGBX", 0, -1)

    texID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texID)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

    glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    return texID


def rotate_around_y(angle) -> np.ndarray:
    angle = np.radians(angle)
    cos = np.cos(-angle)
    sin = np.sin(-angle)
    mtx = np.array([[cos, 0, -sin, 0],
                    [0, 1, 0, 0],
                    [sin, 0, cos, 0],
                    [0, 0, 0, 1]
                    ])
    return mtx.T


def rotate_around_x(angle) -> np.ndarray:
    angle = np.radians(angle)
    cos = np.cos(angle)
    sin = np.sin(angle)
    mtx = np.array([[1, 0, 0, 0],
                    [0, cos, -sin, 0],
                    [0, sin, cos, 0],
                    [0, 0, 0, 1]
                    ])
    return mtx.T


def rotate_around_z(angle) -> np.ndarray:
    angle = np.radians(angle)
    cos = np.cos(angle)
    sin = np.sin(angle)
    mtx = np.array([[cos, -sin, 0, 0],
                    [sin, cos, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return mtx.T


def special_keys(key, x, y):
    global xrot, yrot, zrot, xscale, yscale
    if key == GLUT_KEY_UP:
        xrot += 2.0
    if key == GLUT_KEY_DOWN:
        xrot -= 2.0
    if key == GLUT_KEY_LEFT:
        yrot += 2.0
    if key == GLUT_KEY_RIGHT:
        yrot -= 2.0
    if key == GLUT_KEY_HOME:
        zrot += 2.0
    if key == GLUT_KEY_END:
        zrot -= 2.0
    if key == GLUT_KEY_INSERT:
        glutLeaveMainLoop()


def create_shader(shader_type, source):
    shader = glCreateShader(shader_type)
    glShaderSource(shader, source)
    glCompileShader(shader)
    return shader


def read_shader(path):
    with open(path, mode='r') as file:
        contents = file.read()
    return contents


def draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)

    glEnableClientState(GL_VERTEX_ARRAY)

    glVertexPointer(3, GL_FLOAT, 0, cube)

    glDrawArrays(GL_TRIANGLES, 0, 12*3)

    glDisableClientState(GL_VERTEX_ARRAY)

    glutSwapBuffers()


def main():
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(width_, height_)
    glutInitWindowPosition(50, 50)
    glutInit(sys.argv)
    glutCreateWindow(b"Shaders!")
    glutDisplayFunc(draw)
    glutIdleFunc(draw)
    glutSpecialFunc(special_keys)
    glClearColor(0.2, 0.2, 0.2, 1)

    # vertex = create_shader(GL_VERTEX_SHADER, read_shader('./single_color_vertex'))
    # fragment = create_shader(GL_FRAGMENT_SHADER, read_shader('./single_color_fragment'))

    vertex = create_shader(GL_VERTEX_SHADER, read_shader('known_position_vertex'))
    fragment = create_shader(GL_FRAGMENT_SHADER, read_shader('./hatched_fragment'))
    
    program = glCreateProgram()
    glAttachShader(program, vertex)
    glAttachShader(program, fragment)
    glLinkProgram(program)
    glUseProgram(program)

    loc_xscale = glGetUniformLocation(program, "x_scale")
    if loc_xscale != -1:
        glUniform1f(loc_xscale, xscale)

    loc_yscale = glGetUniformLocation(program, "y_scale")
    if loc_yscale != -1:
        glUniform1f(loc_yscale, yscale)

    h_width = glGetUniformLocation(program, "hatch_width")
    if h_width != -1:
        glUniform1f(h_width, hatch_width)

    i_width = glGetUniformLocation(program, "interline_dist")
    if i_width != -1:
        glUniform1f(i_width, interline_width)

    h_dir = glGetUniformLocation(program, "ind_first")
    if i_width != -1:
        glUniform1i(h_dir, hatching_dir % 2)

    loc_color = glGetUniformLocation(program, "vertex_color")
    if loc_color != -1:
        glUniform4f(loc_color, shape_color[0], shape_color[1], shape_color[2], shape_color[3])

    b_color = glGetUniformLocation(program, "background_color")
    if b_color != -1:
        glUniform4f(b_color, background_color[0], background_color[1], background_color[2], background_color[3])

    h_color = glGetUniformLocation(program, "hatching_color")
    if h_color != -1:
        glUniform4f(h_color, hatched_color[0], hatched_color[1], hatched_color[2], hatched_color[3])

    loc_rotation = glGetUniformLocation(program, "rotation")
    if loc_rotation != -1:
        glUniformMatrix4fv(loc_rotation, 1, GL_FALSE, rotate_around_y(angle))

    glEnable(GL_DEPTH_TEST)
    glutMainLoop()


if __name__ == "__main__":
    main()

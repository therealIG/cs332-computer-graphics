from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import numpy as np
import sys

projection = False
width_ = 800
height_ = 500
xrot = 0.0
yrot = 0.0
zrot = 0.0
center = [5.0, 5.0, 5.0]
x_step = np.random.randint(3, 5)
y_step = np.random.randint(3, 5)
sizes = np.random.random(20)

def init():
    glClearColor(0.0, 0.0, 0.0, 1)
    glEnable(GL_DEPTH_TEST)
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, (1.0, 1.0, 1.0, 1))
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_LIGHT0)


def single_tree(x, y, z, size):
    glTranslated(x, y, z)
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glColor3ub(89, 60, 31))
    glutSolidCylinder(0.2 * size, 1 * size, 50, 50)
    glTranslated(0, 0, 1 * size)
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glColor3ub(11, 74, 27))
    glutSolidCone(1 * size, 2 * size, 50, 50)
    glTranslated(0, 0, 1.8 * size)
    glutSolidCone(0.8 * size, 1.8 * size, 50, 50)
    glTranslated(0, 0, 1.5 * size)
    glutSolidCone(0.5 * size, 1.2 * size, 50, 50)


def draw_forest():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    if not projection:
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(-10, 10, -10, 10, -10, 10)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
    else:
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(0.0, 0.0, 25.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    glPushMatrix()

    idx = 0
    for x in range(-8, 15, x_step):
        for y in range(-10, 15, y_step):
            glRotatef(xrot, 1.0, 0.0, 0.0)
            glRotatef(yrot, 0.0, 1.0, 0.0)
            glRotatef(zrot, 0.0, 0.0, 1.0)
            single_tree(x, y, 0, sizes[idx % 20])
            glPopMatrix()
            glPushMatrix()
            idx += 1

    glPopMatrix()
    glFlush()
    glutSwapBuffers()


def special_keys(key, x, y):
    global xrot, yrot, zrot, projection
    if key == GLUT_KEY_UP:
        xrot += 2.0
    if key == GLUT_KEY_DOWN:
        xrot -= 2.0
    if key == GLUT_KEY_LEFT:
        yrot += 2.0
    if key == GLUT_KEY_RIGHT:
        yrot -= 2.0
    if key == GLUT_KEY_HOME:
        zrot += 2.0
    if key == GLUT_KEY_END:
        zrot -= 2.0
    if key == GLUT_KEY_PAGE_UP:
        # xrot = yrot = zrot = 0.0
        projection = False
    if key == GLUT_KEY_PAGE_DOWN:
        # xrot = yrot = zrot = 0.0
        projection = True
    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(width_, height_)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Task 3")
    glutDisplayFunc(draw_forest)
    glutSpecialFunc(special_keys)
    init()
    glutMainLoop()


if __name__ == "__main__":
    main()

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys

# F1 - каждый кубик вокруг своей оси
# F2 - весь пьедестал вокруг центра сцены
# F3 - все кубики вокруг центра пьедестала почета
# HOME, END - вращение по Z
# ARROW_UP, ARROW_DOWN - вращение по X
# ARROW_LEFT, ARROW_RIGHT - вращение по Y
# PAGE_UP, PAGE_DOWN - Projections

xrot = 0.0
yrot = 0.0
zrot = 0.0
mode = 0
projection = False  # False - Ortho, True - Perspective
first = [4.5, 1.0, 0.0]
second = [2.0, 0.5, 0.0]
third = [6.8, 0.3, 0.0]
center = [4.5, 0.0, 0.0]
width_ = 800
height_ = 500
golden = (1.0, 223.0 / 255, 0.0)
bronze = (205.0 / 255, 127.0 / 255, 50.0 / 255)
silver = (192.0 / 255, 192.0 / 255, 192.0 / 255)


def init():
    glClearColor(0.0, 0.0, 0.0, 1)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_LIGHT0)


def reshape(width, height):
    global width_, height_
    width_ = width
    height_ = height


def draw_podium():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    if not projection:
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(-10, 10, -10, 10, -10, 10)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
    else:
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(0.0, 0.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    glPushMatrix()

    if not mode:
        glTranslated(first[0], first[1], first[2])
    if mode == 2:
        glTranslated(center[0], center[1], center[2])

    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)

    if mode == 1:
        glTranslated(first[0], first[1], first[2])
    if mode == 2:
        glTranslated(-center[0], -center[1], -center[2])
        glTranslated(first[0], first[1], first[2])

    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glColor3f(golden[0], golden[1], golden[2]))
    glutSolidCube(3)

    glPopMatrix()
    glPushMatrix()

    if not mode:
        glTranslated(third[0], third[1], third[2])
    if mode == 2:
        glTranslated(center[0], center[1], center[2])

    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)

    if mode == 1:
        glTranslated(third[0], third[1], third[2])
    if mode == 2:
        glTranslated(-center[0], -center[1], -center[2])
        glTranslated(third[0], third[1], third[2])

    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glColor3f(bronze[0], bronze[1], bronze[2]))
    glutSolidCube(1.6)

    glPopMatrix()

    if not mode:
        glTranslated(second[0], second[1], second[2])
    if mode == 2:
        glTranslated(center[0], center[1], center[2])

    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)

    if mode == 1:
        glTranslated(second[0], second[1], second[2])
    if mode == 2:
        glTranslated(-center[0], -center[1], -center[2])
        glTranslated(second[0], second[1], second[2])

    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glColor3f(silver[0], silver[1], silver[2]))
    glutSolidCube(2)

    glFlush()
    glutSwapBuffers()


def special_keys(key, x, y):
    global xrot, yrot, zrot, mode, projection
    if key == GLUT_KEY_UP:
        xrot += 2.0
    if key == GLUT_KEY_DOWN:
        xrot -= 2.0
    if key == GLUT_KEY_LEFT:
        yrot += 2.0
    if key == GLUT_KEY_RIGHT:
        yrot -= 2.0
    if key == GLUT_KEY_HOME:
        zrot += 2.0
    if key == GLUT_KEY_END:
        zrot -= 2.0
    if key == GLUT_KEY_F1:
        xrot = yrot = zrot = 0.0
        mode = 0
    if key == GLUT_KEY_F2:
        xrot = yrot = zrot = 0.0
        mode = 1
    if key == GLUT_KEY_F3:
        xrot = yrot = zrot = 0.0
        mode = 2
    if key == GLUT_KEY_PAGE_UP:
        xrot = yrot = zrot = 0.0
        projection = False
    if key == GLUT_KEY_PAGE_DOWN:
        xrot = yrot = zrot = 0.0
        projection = True
    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(width_, height_)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Task 2")
    glutDisplayFunc(draw_podium)
    glutSpecialFunc(special_keys)
    init()
    glutMainLoop()


if __name__ == "__main__":
    main()
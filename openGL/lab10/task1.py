from OpenGL.GL import *
from OpenGL.GLUT import *
import numpy as np
import sys

# HOME, END - вращение по Z
# ARROW_UP, ARROW_DOWN - вращение по X
# ARROW_LEFT, ARROW_RIGHT - вращение по Y

xrot = 0.0
yrot = 0.0
zrot = 0.0
w = 500
h = 400


def rand_color():
    r1, r2, r3 = np.random.random(3)
    return r1, r2, r3


current_color = rand_color()
current_shape = 0


def draw_rect():
    global xrot, yrot, zrot
    r1, r2, r3 = current_color[0], current_color[1], current_color[2]
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)
    glBegin(GL_QUADS)
    glColor3f(r1, r2, r3)
    glVertex2f(-0.5, -0.5)
    glColor3f(r1, r2, r3)
    glVertex2f(-0.5, 0.5)
    glColor3f(r1, r2, r3)
    glVertex2f(0.5, 0.5)
    glColor3f(r1, r2, r3)
    glVertex2f(0.5, -0.5)
    glEnd()
    glFlush()
    glutSwapBuffers()


def draw_triangle():
    global xrot, yrot, zrot
    r1, r2, r3 = current_color[0], current_color[1], current_color[2]
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)
    glBegin(GL_TRIANGLES)
    glColor3f(r1, r2, r3)
    glVertex2f(-0.5, -0.5)
    glColor3f(r1, r2, r3)
    glVertex2f(-0.5, 0.5)
    glColor3f(r1, r2, r3)
    glVertex2f(0.5, 0.5)
    glEnd()
    glFlush()
    glutSwapBuffers()


def random_draw():
    global current_shape
    if current_shape % 2 == 0:
        draw_rect()
    else:
        draw_triangle()


def special_keys(key, x, y):
    global xrot, yrot, zrot
    if key == GLUT_KEY_UP:
        xrot += 2.0
    if key == GLUT_KEY_DOWN:
        xrot -= 2.0
    if key == GLUT_KEY_LEFT:
        yrot += 2.0
    if key == GLUT_KEY_RIGHT:
        yrot -= 2.0
    if key == GLUT_KEY_HOME:
        zrot += 2.0
    if key == GLUT_KEY_END:
        zrot -= 2.0
    glutPostRedisplay()


def special_mouse(button, state, x, y):
    global current_color, current_shape
    if state == GLUT_DOWN:
        current_color = rand_color()
        current_shape += 1
        glutPostRedisplay()


def reshape(width, height):
    global w, h
    w = width
    h = height


def main():
    global xrot, yrot, zrot, current_shape
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE)
    glutInitWindowSize(w, h)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Task 1")
    glutDisplayFunc(random_draw)
    glutSpecialFunc(special_keys)
    glutMouseFunc(special_mouse)
    glutMainLoop()


if __name__ == "__main__":
    main()

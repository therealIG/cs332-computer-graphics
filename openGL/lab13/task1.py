from openGL.lab13.parser import OBJParser
import numpy as np
import OpenGL.GL as gl
import OpenGL.GLUT as glut
import OpenGL.GLU as glu
import sys

width_ = 800
height_ = 400
x_rot = 0.0
y_rot = 0.0
z_rot = 0.0


def read_shader(path):
    with open(path, mode='r') as file:
        contents = file.read()
    return contents


vertex_code = read_shader('./data/color.vertex')
fragment_code = read_shader('./data/color.fragment')
model = OBJParser("./data/cylinder.obj")

vertexes = model.vertex()
indexes = model.vertex_idx()
colors = []

for _ in range(len(vertexes)):
    colors.append(list(np.random.random(4)))


def keyboard(key, x, y):
    global x_rot, y_rot, z_rot
    if key == b'\x1b':
        sys.exit()
    if key == glut.GLUT_KEY_UP:
        x_rot += 2.0
    if key == glut.GLUT_KEY_DOWN:
        x_rot -= 2.0
    if key == glut.GLUT_KEY_LEFT:
        y_rot += 2.0
    if key == glut.GLUT_KEY_RIGHT:
        y_rot -= 2.0
    if key == glut.GLUT_KEY_HOME:
        z_rot += 2.0
    if key == glut.GLUT_KEY_END:
        z_rot -= 2.0
    if key == glut.GLUT_KEY_INSERT:
        glut.glutLeaveMainLoop()
    glut.glutPostRedisplay()


def reshape(width, height):
    gl.glViewport(0, 0, width, height)


def draw():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    glu.gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()
    glu.gluLookAt(0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    gl.glRotatef(x_rot, 1.0, 0.0, 0.0)
    gl.glRotatef(y_rot, 0.0, 1.0, 0.0)
    gl.glRotatef(z_rot, 0.0, 0.0, 1.0)

    gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
    gl.glEnableClientState(gl.GL_COLOR_ARRAY)
    gl.glEnableClientState(gl.GL_INDEX_ARRAY)

    gl.glVertexPointer(3, gl.GL_FLOAT, 0, vertexes)
    gl.glIndexPointer(gl.GL_INT, 0, indexes)
    gl.glColorPointer(4,  gl.GL_FLOAT, 0, colors)

    gl.glDrawElements(gl.GL_TRIANGLES, 3 * len(indexes), gl.GL_UNSIGNED_INT, indexes)

    gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
    gl.glDisableClientState(gl.GL_INDEX_ARRAY)
    gl.glDisableClientState(gl.GL_COLOR_ARRAY)

    glut.glutSwapBuffers()


glut.glutInit()
glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGBA | glut.GLUT_DEPTH)
glut.glutCreateWindow('textures!')
glut.glutReshapeWindow(width_, height_)
glut.glutReshapeFunc(reshape)
glut.glutDisplayFunc(draw)
glut.glutIdleFunc(draw)
glut.glutSpecialFunc(keyboard)

program = gl.glCreateProgram()
vertex = gl.glCreateShader(gl.GL_VERTEX_SHADER)
fragment = gl.glCreateShader(gl.GL_FRAGMENT_SHADER)

gl.glShaderSource(vertex, vertex_code)
gl.glShaderSource(fragment, fragment_code)

gl.glCompileShader(vertex)
if not gl.glGetShaderiv(vertex, gl.GL_COMPILE_STATUS):
    error = gl.glGetShaderInfoLog(vertex).decode()
    print(error)
    raise RuntimeError("Shader compilation error")

gl.glCompileShader(fragment)
gl.glCompileShader(fragment)
if not gl.glGetShaderiv(fragment, gl.GL_COMPILE_STATUS):
    error = gl.glGetShaderInfoLog(fragment).decode()
    print(error)
    raise RuntimeError("Shader compilation error")
gl.glAttachShader(program, vertex)
gl.glAttachShader(program, fragment)

gl.glLinkProgram(program)
if not gl.glGetProgramiv(program, gl.GL_LINK_STATUS):
    print(gl.glGetProgramInfoLog(program))
    raise RuntimeError('Linking error')

gl.glDetachShader(program, vertex)
gl.glDetachShader(program, fragment)

gl.glUseProgram(program)

gl.glEnable(gl.GL_DEPTH_TEST)
glut.glutMainLoop()

from openGL.lab13.parser import OBJParser
import pygame
import numpy as np
import OpenGL.GL as gl
import OpenGL.GLUT as glut
import OpenGL.GLU as glu
import sys

width_ = 800
height_ = 400
x_rot = 0.0
y_rot = 0.0
z_rot = 0.0
x_shift = 0.0
y_shift = 0.0
z_shift = 0.0
global_ambient = [.9, .05, .05, .1]
light_ambient = [.2, .2, .2, 1.0]
light_diffuse = [0.4, 0.4, 0.4, 1]
light_location = [2, 2, 10]
material_ambient = [.2, .2, .2, 1.0]
material_diffuse = [1, 1, 1, 1]


def read_shader(path):
    with open(path, mode='r') as file:
        contents = file.read()
    return contents


def preprocess_obj(obj: OBJParser):
    textures = obj.textures()
    normals = obj.normals()
    tex_sorted = np.empty((len(textures), 2))
    norm_sorted = np.empty((len(normals), 3))
    for vind, tind, nind in zip(obj.vertex_idx(), obj.tex_idx(), obj.normal_idx()):
        for i in range(0, 3, 1):
            tex_sorted[vind[i]] = np.array(textures[tind[i]])
            norm_sorted[vind[i]] = np.array(normals[nind[i]])

    return tex_sorted, norm_sorted


def load_texture(path):
    img = pygame.image.load(path)
    img_data = pygame.image.tostring(img, 'RGBA', 1)
    width, height = img.get_size()

    texture = gl.glGenTextures(1)
    gl.glBindTexture(gl.GL_TEXTURE_2D, texture)
    gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR_MIPMAP_LINEAR)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
    gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
    gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA, width, height, 0, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, img_data)
    gl.glGenerateMipmap(gl.GL_TEXTURE_2D)
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
    return texture


vertex_code = read_shader('data/phong_vertex')
fragment_code = read_shader('./data/phong_color_fragment')
model = OBJParser("./data/african_head.obj")

textures, normals = preprocess_obj(model)


def keyboard(key, x, y):
    global x_rot, y_rot, z_rot, light_location, x_shift, y_shift, z_shift
    if key == b'\x1b':
        sys.exit()
    if key == glut.GLUT_KEY_UP:
        x_rot += 2.0
    if key == glut.GLUT_KEY_DOWN:
        x_rot -= 2.0
    if key == glut.GLUT_KEY_LEFT:
        y_rot += 2.0
    if key == glut.GLUT_KEY_RIGHT:
        y_rot -= 2.0
    if key == glut.GLUT_KEY_HOME:
        z_rot += 2.0
    if key == glut.GLUT_KEY_END:
        z_rot -= 2.0
    if key == glut.GLUT_KEY_INSERT:
        glut.glutLeaveMainLoop()
    if key == glut.GLUT_KEY_F1:
        light_location[0] -= 2
    if key == glut.GLUT_KEY_F2:
        light_location[0] += 2
    if key == glut.GLUT_KEY_F3:
        light_location[1] -= 2
    if key == glut.GLUT_KEY_F4:
        light_location[1] += 2
    if key == glut.GLUT_KEY_F5:
        light_location[2] -= 2
    if key == glut.GLUT_KEY_F6:
        light_location[2] += 2
    if key == glut.GLUT_KEY_F7:
        x_shift -= 0.2
    if key == glut.GLUT_KEY_F8:
        x_shift += 0.2
    if key == glut.GLUT_KEY_F9:
        y_shift -= 0.2
    if key == glut.GLUT_KEY_F10:
        y_shift += 0.2
    if key == glut.GLUT_KEY_F11:
        z_shift -= 0.2
    if key == glut.GLUT_KEY_F12:
        z_shift += 0.2
    glut.glutPostRedisplay()


def reshape(width, height):
    gl.glViewport(0, 0, width, height)


def draw():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    glu.gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()
    glu.gluLookAt(0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)


    gl.glRotatef(x_rot, 1.0, 0.0, 0.0)
    gl.glRotatef(y_rot, 0.0, 1.0, 0.0)
    gl.glRotatef(z_rot, 0.0, 0.0, 1.0)
    gl.glTranslated(x_shift, y_shift, z_shift)

    loc = gl.glGetUniformLocation(program, 'Global_ambient')
    if loc not in (None, -1):
        gl.glUniform4f(loc, global_ambient[0], global_ambient[1], global_ambient[2], global_ambient[3])

    loc = gl.glGetUniformLocation(program, 'Light_ambient')
    if loc not in (None, -1):
        gl.glUniform4f(loc, light_ambient[0], light_ambient[1], light_ambient[2], light_ambient[3])

    loc = gl.glGetUniformLocation(program, 'Light_diffuse')
    if loc not in (None, -1):
        gl.glUniform4f(loc, light_diffuse[0], light_diffuse[1], light_diffuse[2], light_diffuse[3])

    loc = gl.glGetUniformLocation(program, 'Light_location')
    if loc not in (None, -1):
        gl.glUniform3f(loc, light_location[0], light_location[1], light_location[2])

    loc = gl.glGetUniformLocation(program, 'Eye_position')
    if loc not in (None, -1):
        gl.glUniform3f(loc, 0.0, 0.0, 2.0)

    loc = gl.glGetUniformLocation(program, 'Material_ambient')
    if loc not in (None, -1):
        gl.glUniform4f(loc, material_ambient[0], material_ambient[1], material_ambient[2], material_ambient[3])

    loc = gl.glGetUniformLocation(program, 'Material_diffuse')
    if loc not in (None, -1):
        gl.glUniform4f(loc, material_diffuse[0], material_diffuse[1], material_diffuse[2], material_diffuse[3])

    gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
    gl.glEnableClientState(gl.GL_TEXTURE_COORD_ARRAY)
    gl.glEnableClientState(gl.GL_NORMAL_ARRAY)
    gl.glEnableClientState(gl.GL_INDEX_ARRAY)

    gl.glVertexPointer(3, gl.GL_FLOAT, 0, vertexes)
    gl.glTexCoordPointer(2, gl.GL_FLOAT, 0, textures)
    gl.glNormalPointer(gl.GL_FLOAT, 0, normals)
    gl.glIndexPointer(gl.GL_INT, 0, indexes)

    gl.glDrawElements(gl.GL_TRIANGLES, 3 * len(indexes), gl.GL_UNSIGNED_INT, indexes)

    gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
    gl.glDisableClientState(gl.GL_INDEX_ARRAY)
    gl.glDisableClientState(gl.GL_TEXTURE_COORD_ARRAY)
    gl.glDisableClientState(gl.GL_NORMAL_ARRAY)

    glut.glutSwapBuffers()


glut.glutInit()
glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGBA | glut.GLUT_DEPTH)
glut.glutCreateWindow('textures!')
glut.glutReshapeWindow(width_, height_)
glut.glutReshapeFunc(reshape)
glut.glutDisplayFunc(draw)
glut.glutIdleFunc(draw)
glut.glutSpecialFunc(keyboard)

vertexes = model.vertex()
indexes = model.vertex_idx()

program = gl.glCreateProgram()
vertex = gl.glCreateShader(gl.GL_VERTEX_SHADER)
fragment = gl.glCreateShader(gl.GL_FRAGMENT_SHADER)

gl.glShaderSource(vertex, vertex_code)
gl.glShaderSource(fragment, fragment_code)

gl.glCompileShader(vertex)
if not gl.glGetShaderiv(vertex, gl.GL_COMPILE_STATUS):
    error = gl.glGetShaderInfoLog(vertex).decode()
    print(error)
    raise RuntimeError("Shader compilation error")

gl.glCompileShader(fragment)
gl.glCompileShader(fragment)
if not gl.glGetShaderiv(fragment, gl.GL_COMPILE_STATUS):
    error = gl.glGetShaderInfoLog(fragment).decode()
    print(error)
    raise RuntimeError("Shader compilation error")
gl.glAttachShader(program, vertex)
gl.glAttachShader(program, fragment)

gl.glLinkProgram(program)
if not gl.glGetProgramiv(program, gl.GL_LINK_STATUS):
    print(gl.glGetProgramInfoLog(program))
    raise RuntimeError('Linking error')

gl.glDetachShader(program, vertex)
gl.glDetachShader(program, fragment)

gl.glUseProgram(program)

texture = load_texture("./data/tex.tga")
loc_tex1 = gl.glGetUniformLocation(program, "texture")
gl.glUniform1i(loc_tex1, 0)
gl.glActiveTexture(gl.GL_TEXTURE0)
gl.glBindTexture(gl.GL_TEXTURE_2D, texture)

gl.glEnable(gl.GL_DEPTH_TEST)
glut.glutMainLoop()


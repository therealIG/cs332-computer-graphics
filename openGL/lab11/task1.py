import numpy
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import pygame
import sys

width_ = 800
height_ = 800
light_simple = -1
light_car = -1
car_coordinate_x = 0
car_coordinate_y = 0
current_movement = 0
car_rotation = 0
val_for_div = 0.1
xrot = 0.0
yrot = 0.0
zrot = 0.0
current_state = 0
next_angle = 0
normal_state = [0, 0]


def reshape(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-1.2, 1.2, -1.2, 1.2, -1, 1)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def load_texture(texture_url):
    tex_id = glGenTextures(1)
    tex = pygame.image.load(texture_url)
    tex_surface = pygame.image.tostring(tex, 'RGBA')
    tex_width, tex_height = tex.get_size()
    glBindTexture(GL_TEXTURE_2D, tex_id)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_width, tex_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_surface)
    glBindTexture(GL_TEXTURE_2D, 0)
    return tex_id


def init():
    glClearColor(0.3, 0.3, 0.3, 0.0)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LIGHTING)
    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_NORMALIZE)


def draw_car(x, y, texture):
    glBindTexture(GL_TEXTURE_2D, texture)

    # Передняя
    glBegin(GL_POLYGON)
    glTexCoord(0, 0)
    glVertex3f(x - 0.1, y + 0.1, 0.0)
    glTexCoord(0, 1)
    glVertex3f(x + 0.1, y + 0.1, 0.0)
    glTexCoord(1, 1)
    glVertex3f(x + 0.1, y + 0.1, 0.5)
    glTexCoord(1, 0)
    glVertex3f(x - 0.1, y + 0.1, 0.5)
    glEnd()

    # Верхняя
    glBegin(GL_POLYGON)
    glTexCoord(0, 0)
    glVertex3f(x - 0.1, y + 0.1, 0.5)
    glTexCoord(0, 1)
    glVertex3f(x + 0.1, y + 0.1, 0.5)
    glTexCoord(1, 1)
    glVertex3f(x + 0.1, y - 0.1, 0.5)
    glTexCoord(1, 0)
    glVertex3f(x - 0.1, y - 0.1, 0.5)
    glEnd()

    # Задняя
    glBegin(GL_POLYGON)
    glTexCoord(0, 0)
    glVertex3f(x - 0.1, y - 0.1, 0.0)
    glTexCoord(0, 1)
    glVertex3f(x + 0.1, y - 0.1, 0.0)
    glTexCoord(1, 1)
    glVertex3f(x + 0.1, y - 0.1, 0.5)
    glTexCoord(1, 0)
    glVertex3f(x - 0.1, y - 0.1, 0.5)
    glEnd()

    # Левая
    glBegin(GL_POLYGON)
    glTexCoord(0, 0)
    glVertex3f(x - 0.09, y - 0.1, 0.0)
    glTexCoord(0, 1)
    glVertex3f(x - 0.09, y + 0.1, 0.0)
    glTexCoord(1, 1)
    glVertex3f(x - 0.09, y + 0.1, 0.5)
    glTexCoord(1, 0)
    glVertex3f(x - 0.09, y - 0.1, 0.5)
    glEnd()

    # Правая
    glBegin(GL_POLYGON)
    glTexCoord(0, 0)
    glVertex3f(x + 0.09, y - 0.1, 0.0)
    glTexCoord(0, 1)
    glVertex3f(x + 0.09, y + 0.1, 0.0)
    glTexCoord(1, 1)
    glVertex3f(x + 0.09, y + 0.1, 0.5)
    glTexCoord(1, 0)
    glVertex3f(x + 0.09, y - 0.1, 0.5)
    glEnd()

    #  Колеса
    glPushMatrix()
    glTranslated(x + 0.1, y + 0.03, 0.04)
    glRotatef(90, 0.0, 1.0, 0.0)
    glutSolidTorus(0.01, 0.02, 32, 32)
    glPopMatrix()

    glPushMatrix()
    glTranslated(x - 0.1, y + 0.03, 0.04)
    glRotatef(90, 0.0, 1.0, 0.0)
    glutSolidTorus(0.01, 0.02, 32, 32)
    glPopMatrix()

    glPushMatrix()
    glTranslated(x - 0.1, y - 0.03, 0.04)
    glRotatef(90, 0.0, 1.0, 0.0)
    glutSolidTorus(0.01, 0.02, 32, 32)
    glPopMatrix()

    glPushMatrix()
    glTranslated(x + 0.1, y - 0.03, 0.04)
    glRotatef(90, 0.0, 1.0, 0.0)
    glutSolidTorus(0.01, 0.02, 32, 32)
    glPopMatrix()

    # Фары
    glPushMatrix()
    glTranslated(x + 0.07, y + 0.106, 0.3)
    glutSolidSphere(0.04, 32, 32)
    glPopMatrix()

    glPushMatrix()
    glTranslated(x - 0.07, y + 0.106, 0.3)
    glutSolidSphere(0.04, 32, 32)
    glPopMatrix()


def draw_car_ligth(x, y):
    if light_car == 1:
        glEnable(GL_LIGHT6)
        light3_diffuse = [1.0, 1.0, 1.0, 1.0]
        light3_position = [x + 0.08, y, 0.3, 1.0]
        light3_spot = [0.1, 0.3, -0.1]
        glLightfv(GL_LIGHT6, GL_DIFFUSE, light3_diffuse)
        glLightfv(GL_LIGHT6, GL_POSITION, light3_position)
        glLightf(GL_LIGHT6, GL_SPOT_CUTOFF, 30)
        glLightfv(GL_LIGHT6, GL_SPOT_DIRECTION, light3_spot)
        glLightf(GL_LIGHT6, GL_CONSTANT_ATTENUATION, 0.0)
        glLightf(GL_LIGHT6, GL_LINEAR_ATTENUATION, 0.4)
        glLightf(GL_LIGHT6, GL_QUADRATIC_ATTENUATION, 0.9)

        glEnable(GL_LIGHT7)
        light3_diffuse = [1.0, 1.0, 1.0, 1.0]
        light3_position = [x - 0.08, y, 0.3, 1.0]
        light3_spot = [-0.1, 0.3, -0.1]
        glLightfv(GL_LIGHT7, GL_DIFFUSE, light3_diffuse)
        glLightfv(GL_LIGHT7, GL_POSITION, light3_position)
        glLightf(GL_LIGHT7, GL_SPOT_CUTOFF, 30)
        glLightfv(GL_LIGHT7, GL_SPOT_DIRECTION, light3_spot)
        glLightf(GL_LIGHT7, GL_CONSTANT_ATTENUATION, 0.0)
        glLightf(GL_LIGHT7, GL_LINEAR_ATTENUATION, 0.4)
        glLightf(GL_LIGHT7, GL_QUADRATIC_ATTENUATION, 0.9)

    if light_car == -1:
        glDisable(GL_LIGHT6)
        glDisable(GL_LIGHT7)


def single_tree(x, y, texture):
    glBindTexture(GL_TEXTURE_2D, texture)
    glBegin(GL_QUADS)
    glTexCoord(0, 0)
    glVertex3f(x, y, 0)
    glTexCoord(0, 1)
    glVertex2f(x, y + val_for_div, 0)
    glTexCoord(1, 1)
    glVertex3f(x + val_for_div, y + val_for_div, 0)
    glTexCoord(1, 0)
    glVertex3f(x + val_for_div, y, 0)
    glEnd()


def single_cylindr(x, y):
    glPushMatrix()
    glTranslated(x, y, 0.0)
    glutSolidCylinder(0.03, 0.3, 32, 32)
    glPopMatrix()


def draw_forest():
    global xrot, yrot, zrot, current_state, next_angle, car_coordinate_y, car_coordinate_x, current_movement, normal_state
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(65.0, width_ / height_, 0.1, 1000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)
    glRotatef(xrot, 1.0, 0.0, 0.0)
    glRotatef(yrot, 0.0, 1.0, 0.0)
    glRotatef(zrot, 0.0, 0.0, 1.0)

    material_diffuse = [1.0, 1.0, 1.0, 1.0]
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_diffuse)
    glEnable(GL_TEXTURE_2D)
    list_lights = [GL_LIGHT0, GL_LIGHT1, GL_LIGHT2, GL_LIGHT3, GL_LIGHT4, GL_LIGHT5]
    if light_simple == 1:
        ind = 0
        for x in numpy.arange(-1.0, 1.1, 1.0):
            light1_diffuse = [1.0, 1.0, 1.0, 1.0]
            light1_position = [x, 0.0, 0.3, 1.0]
            glEnable(list_lights[ind])

            glLightfv(list_lights[ind], GL_DIFFUSE, light1_diffuse)
            glLightfv(list_lights[ind], GL_POSITION, light1_position)
            glLightf(list_lights[ind], GL_CONSTANT_ATTENUATION, 0.0)
            glLightf(list_lights[ind], GL_LINEAR_ATTENUATION, 0.4)
            glLightf(list_lights[ind], GL_QUADRATIC_ATTENUATION, 0.8)

            ind += 1

    glPushMatrix()
    texture = load_texture("test.png")
    texture2 = load_texture("index.png")

    for i in numpy.arange(-1.0, 1.0, val_for_div):
        for j in numpy.arange(-1.0, 1.0, val_for_div):
            single_tree(i, j, texture)

    for x in numpy.arange(-1.0, 1.1, 1.0):
        single_cylindr(x, 0.0)

    if current_state == 1:
        glTranslate(car_coordinate_x, car_coordinate_y, 0.0)
        glRotatef(-90, 0.0, 0.0, 1.0)
        glTranslate(-car_coordinate_x, -car_coordinate_y, 0.0)
    elif current_state == 2:
        glTranslate(car_coordinate_x, car_coordinate_y, 0.0)
        glRotatef(-180, 0.0, 0.0, 1.0)
        glTranslate(-car_coordinate_x, -car_coordinate_y, 0.0)
    elif current_state == 3:
        glTranslate(car_coordinate_x, car_coordinate_y, 0.0)
        glRotatef(-270, 0.0, 0.0, 1.0)
        glTranslate(-car_coordinate_x, -car_coordinate_y, 0.0)
    #
    if next_angle == 1:
        current_state = (current_state + 1) % 4
    if next_angle == -1:
        current_state = (current_state - 1 + 4) % 4

    if current_state == 0 and current_movement == 0.009:
        car_coordinate_y += 0.009
    elif current_state == 0 and current_movement == -0.009:
        car_coordinate_y -= 0.009
    elif current_state == 1 and current_movement == 0.009:
        car_coordinate_x += 0.009
    elif current_state == 1 and current_movement == -0.009:
        car_coordinate_x -= 0.009

    elif current_state == 2 and current_movement == 0.009:
        car_coordinate_y -= 0.009
    elif current_state == 2 and current_movement == -0.009:
        car_coordinate_y += 0.009

    elif current_state == 3 and current_movement == 0.009:
        car_coordinate_x -= 0.009
    elif current_state == 3 and current_movement == -0.009:
        car_coordinate_x += 0.009

    draw_car(car_coordinate_x, car_coordinate_y, texture2)
    draw_car_ligth(car_coordinate_x, car_coordinate_y)

    next_angle = 0
    current_movement = 0

    glPopMatrix()
    for light in list_lights:
        glDisable(light)
    glutSwapBuffers()


def special_keys(key, x, y):
    global light_simple, car_coordinate_x, car_coordinate_y, car_rotation, light_car, xrot, yrot, zrot, current_angle, next_angle, current_movement
    if key == GLUT_KEY_UP:
        current_movement = 0.009
    if key == GLUT_KEY_DOWN:
        current_movement = -0.009
    if key == GLUT_KEY_LEFT:
        next_angle = -1
    if key == GLUT_KEY_RIGHT:
        next_angle = 1
    if key == GLUT_KEY_PAGE_DOWN:
        light_simple *= -1
    if key == GLUT_KEY_PAGE_UP:
        light_car *= -1
    if key == GLUT_KEY_F1:
        xrot += 0.5
    if key == GLUT_KEY_F2:
        xrot -= 0.5
    if key == GLUT_KEY_F3:
        yrot += 0.5
    if key == GLUT_KEY_F4:
        yrot -= 0.5
    if key == GLUT_KEY_F5:
        zrot += 0.5
    if key == GLUT_KEY_F6:
        zrot -= 0.5
    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(width_, height_)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Task 3")
    glutDisplayFunc(draw_forest)
    glutReshapeFunc(reshape)
    glutSpecialFunc(special_keys)
    init()
    glEnable(GL_DEPTH_TEST)
    glutMainLoop()


if __name__ == "__main__":
    main()

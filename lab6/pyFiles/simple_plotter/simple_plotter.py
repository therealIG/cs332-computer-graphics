import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import os
os.sys.path.append("./../../../")
from lab6.pyFiles.shapes.geometric_primitives import Polyhedron, Point
from lab6.pyFiles.shapes.shapes import Hexahedron
from lab6.pyFiles.affine_transform import rotate_around_str_centered_parallel_y
from lab6.pyFiles.projection import perspective_projection, orto_projection, isometric_projection

edge = 3
cube = np.array([[[0, 0, 0], [0, 0, edge], [edge, 0, edge], [edge, 0, 0]],
                   [[0, 0, 0], [0, edge, 0], [edge, edge, 0], [edge, 0, 0]],
                   [[0, edge, 0], [0, edge, edge], [edge, edge, edge], [edge, edge, 0]],
                   [[0, 0, edge], [0, edge, edge], [edge, edge, edge], [edge, 0, edge]],
                   [[0, 0, edge], [0, edge, edge], [0, edge, 0], [0, 0, 0]],
                   [[edge, 0, 0], [edge, edge, 0], [edge, edge, edge], [edge, 0, edge]]
                 ])


def simple_plot_figure(polyhedron: Polyhedron) -> plt.figure:
    """
    plots custom figure on 3D - Axes
    :param polyhedron:
    :return: matplotlib.pyplot.figure
    """
    shape = polyhedron.vertexes()
    fig = plt.figure(figsize=plt.figaspect(0.5)*1.5)
    ax = fig.add_subplot(111, projection='3d')
    X = shape[:, :, 0]
    Y = shape[:, :, 1]
    Z = shape[:, :, 2]
    ax.scatter3D(X, Y, Z, color='red')
    ax.add_collection3d(Poly3DCollection(shape, facecolors='cyan', linewidths=1, edgecolors='r', alpha=.25))
    # ax.set_xlim3d([X.mean() - 2*X.max(), X.mean() + 2*X.max()])
    # ax.set_ylim3d([Y.mean() - 2*Y.max(), Y.mean() + 2*Y.max()])
    # ax.set_zlim3d([Z.mean() - 2*Z.max(), Z.mean() + 2*Z.max()])
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    return fig


def simple_plot_points(points: np.ndarray) -> plt.figure:
    """
    plots custom figure by it's points
    :param points:
    :return:
    """
    fig = plt.figure(figsize=plt.figaspect(0.5) * 1.5)
    ax = fig.add_subplot(111, projection='3d')
    X = points[:, :, 0]
    Y = points[:, :, 1]
    Z = points[:, :, 2]
    ax.scatter3D(X, Y, Z, color='red')
    # ax.plot_surface(X, Y, Z)
    ax.add_collection3d(Poly3DCollection(points, linewidths=1, edgecolors='r', alpha=1))
    # ax.set_xlim3d([X.mean() - 2*X.max(), X.mean() + 2*X.max()])
    # ax.set_ylim3d([Y.mean() - 2*Y.max(), Y.mean() + 2*Y.max()])
    # ax.set_zlim3d([Z.mean() - 2*Z.max(), Z.mean() + 2*Z.max()])
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    return fig


if __name__ == "__main__":
    shape = Hexahedron(sphere_center=np.array([5, -1, 4]), sphere_radius=2.)
    shape.vertexes(perspective_projection(shape.vertexes(), np.radians(45), 3/2., 100, 10))
    # shape.vertexes(isometric_projection(shape.vertexes(), 45, 45))
    print(shape)
    simple_plot_figure(shape)
    plt.show()
import numpy as np
from lab6.pyFiles.utils import _preparing_points
import math


def perspective_projection(vertexes: np.ndarray, fovy: float, aspect: float, f: float, n: float):
    """

    :param vertexes:
    :param fovy:
    :param aspect:
    :param f:
    :param n:
    :return:
    """
    mat = np.array([
        [(1./math.tan(fovy/2.))*(1./aspect), 0, 0, 0],
        [0, (1./math.tan(fovy/2.)), 0, 0],
        [0, 0, (f+n)/(f-n), 1],
        [0, 0, (-2*f*n)/(f-n), 0]
    ], dtype=np.float64)
    # mat = np.array([
    #     [1, 0, 0, 0],
    #     [0, 1, 0, 0],
    #     [0, 0, 1, 1],
    #     [0, 0, 1, 0]
    # ], dtype=np.float)
    result = np.matmul(_preparing_points(vertexes), mat)
    w = np.expand_dims(result[:, :, 3], axis=-1)
    return np.divide(result[:, :, :3], w)


def orto_projection(vertexes: np.ndarray, axis: int):
    """

    :param vertexes:
    :param axis:
    :return:
    """
    mat = np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ], dtype=np.float64)
    mat[axis, axis] = 0
    result = np.matmul(_preparing_points(vertexes), mat)
    return result


def isometric_projection(vertexes: np.ndarray, f: float, p: float):
    """

    :param vertexes:
    :return:
    """
    f = math.radians(f)
    p = math.radians(p)
    csf = math.cos(f)
    csp = math.cos(p)
    snf = math.sin(f)
    snp = math.sin(p)
    mat = np.array([
        [csf, snf*snp, 0, 0],
        [0, csp, 0, 0],
        [snf, -1*csf*snf, 0, 0],
        [0, 0, 0, 1]
    ], dtype=np.float)
    result = np.matmul(_preparing_points(vertexes), mat)
    return result

import numpy as np


def _get_normal(face: np.ndarray) -> np.ndarray:
    """
    Возвращает нормаль к грани
    :param face: Грань, np.ndarray (n, 3)
    :return: np.ndarray (3,)
    """
    vec1 = face[0] - face[1]
    vec2 = face[2] - face[1]
    return np.cross(vec1, vec2)


class Point:
    def __init__(self, x: float, y: float, z: float):
        """
        Точка: ndarray: shape = [1, 3]
        :param x: float
        :param y: float
        :param z: float
        """
        self._position: np.ndarray = np.array([[x, y, z]], dtype=np.float)

    def position(self, new_values: np.ndarray = None):
        if new_values is None:
            return self._position
        else:
            self._position = new_values

    def x(self, new_x: float = None):
        if new_x is None:
            return self._position[0, 0]
        else:
            self._position[0, 0] = new_x

    def y(self, new_y: float = None):
        if new_y is None:
            return self._position[0, 1]
        else:
            self._position[0, 1] = new_y

    def z(self, new_z: float = None):
        if new_z is None:
            return self._position[0, 2]
        else:
            self._position[0, 2] = new_z


class Line:
    def __init__(self, point1: np.ndarray, point2: np.ndarray):
        """
        Прямая: ndarray: shape = [1, 2, 3]
        :param point1: np.ndarray with shape [1, 3]
        :param point2: np.ndarray with shape [1, 3]
        """
        self._points: np.ndarray = np.array([[point1, point2]])

    def points(self, new_points: np.ndarray = None):
        if new_points is None:
            return self._points
        else:
            self._points = new_points

    def first_point(self, new_point: np.ndarray = None):
        if new_point is None:
            return self._points[0, 0]
        else:
            self._points[0, 0] = new_point

    def second_point(self, new_point: np.ndarray = None):
        if new_point is None:
            return self._points[0, 1]
        else:
            self._points[0, 1] = new_point


class Verge:
    def __init__(self, vertexes: np.ndarray):
        """
        Грань: ndarray: shape = [1, m, 3], m - количество вершин грани {вершины задаются по часовой стрелке}
        :param vertexes: np.ndarray
        """
        self._vertexes: np.ndarray = vertexes
        self._vertexes_count: int = self._vertexes.shape[1]

    def vertexes(self, new_vertexes: np.ndarray = None):
        if new_vertexes is None:
            return self._vertexes
        else:
            self._vertexes = new_vertexes

    def vertex(self, vertex_number: int, new_vertex: np.ndarray = None):
        if new_vertex is None:
            return self._vertexes[vertex_number]
        else:
            self._vertexes[vertex_number] = new_vertex

    def size(self):
        return self._vertexes_count


class Polyhedron:
    def __init__(self, sphere_center: np.ndarray, sphere_radius: float, polyhedron_type: str = None,
                 center: np.ndarray = None):
        """
        Многогранник: ndarray: shape = [n, m, 3], n - количество граней; m - количество вершин граней
        в каждой грани вершины задаются по часовой стрелке
        :param sphere_center: np.ndarray with shape (3, ). the center of the described sphere
        :param sphere_radius: float. the radius of the described sphere
        """
        if sphere_center is None:
            sphere_center = np.array([0, 0, 0], dtype=np.float)

        if sphere_radius is None:
            sphere_radius = 10.0

        self._vertexes: np.ndarray = self._construction(sphere_center.astype(np.float64), sphere_radius).astype(
            np.float64)
        self._type: str = polyhedron_type
        self._verges_count: int = self._vertexes.shape[0]
        self._vertexes_in_one_verge: int = self._vertexes.shape[1]
        self._common_vertexes_count: int = self._verges_count * self._vertexes_in_one_verge
        self._center = center
        self._visibility = [True] * self._vertexes.shape[0]

    def _construction(self, sphere_center: np.ndarray, sphere_radius: float) -> np.ndarray:
        raise NotImplementedError

    def common_vertexes_count(self) -> int:
        return self._common_vertexes_count

    def size(self) -> (int, int):
        return self._verges_count, self._vertexes_in_one_verge

    def vertexes(self, new_vertexes: np.ndarray = None):
        if new_vertexes is None:
            return self._vertexes
        else:
            self._vertexes = new_vertexes
            self._verges_count = new_vertexes.shape[0]

    def triangulation(self) -> (np.ndarray, int):
        return self._vertexes, 1

    @staticmethod
    def texture_coordinates(target_width, target_height) -> np.ndarray:
        return np.array([[[target_width / 2., target_height * 0.95], [0.05 * target_width, 0.05 * target_height],
                          [0.95 * target_width, 0.05 * target_height]]], dtype=np.float64)

    def get_normals(self) -> (list, list):
        """
        Возвращает вершины многогранника и нормали к ним
        :return: list, list
        """
        vs = []
        vs_faces = {}
        vn = []
        face_normal = []
        for face in self._vertexes:
            face_normal.append(_get_normal(face))

        for i in range(self._vertexes.shape[0]):
            for vertex in self._vertexes[i]:
                if tuple(vertex) not in vs:
                    vs.append(tuple(vertex))
                list_faces = vs_faces.get(tuple(vertex), list([]))
                list_faces.append(i)
                vs_faces[tuple(vertex)] = list_faces

        for vertex in vs:
            normals = [face_normal[i] for i in vs_faces[tuple(vertex)]]
            vn.append(np.mean(normals, axis=0))

        return vs, vn

    def clip_non_faces(self, overview: np.ndarray):
        """
        Отсечение нелицевых граней
        :param overview: Вектор обзора: shape = (3, )!
        :return: list(bool) статус видимости для каждой грани
        """
        if overview is not None:
            visibility = []
            for face in self._vertexes:
                normal = _get_normal(face)
                visible = np.dot(normal, overview) < 0
                if visible:
                    visibility.append(True)
                else:
                    visibility.append(False)
            self._visibility = visibility
        else:
            self._visibility = [True] * self._vertexes.shape[0]

    def visibility(self):
        return self._visibility

    @staticmethod
    def colors() -> list:
        return [np.array([255, 0, 25]), np.array([25, 0, 125]),
                np.array([0, 255, 0]), np.array([0, 0, 255])]

    def __str__(self):
        result: str = self._type + "\n"
        for n in range(self._verges_count):
            for m in range(self._vertexes_in_one_verge):
                for p in range(3):
                    result += "{:>5} ".format(round(self._vertexes[n, m, p], ndigits=2))
                result += "   "
            result += "\n"
        return result



from lab6.pyFiles.shapes.geometric_primitives import Polyhedron
import numpy as np
import math
from lab4.pyFiles.affine_transfromation import rotation

X = 0
Y = 1
Z = 2


class Tetrahedron(Polyhedron):
    def __init__(self, sphere_center: np.ndarray = None, sphere_radius: float = None):
        """
        Тетраэдр. Задается центром и радиусом описанной сферы.
        :param sphere_center: np.ndarray с размерностью (3, ). Если не передано,
                              то центр устанавливается в начало координат
        :param sphere_radius: float. Если не передано, то радиус устанавливается равным 10.0
        """
        Polyhedron.__init__(self, sphere_center, sphere_radius, "Tetrahedron", sphere_center)

    def _construction(self, sphere_center: np.ndarray, sphere_radius: float) -> np.ndarray:
        # Вершниа
        top = sphere_center.copy()
        top[Y] = top[Y] + sphere_radius

        # Центр основания
        basis_center = top.copy()
        r = math.sqrt(2/3) * ((4 * sphere_radius) / math.sqrt(6))
        basis_center[Y] = basis_center[Y] - r

        # Вершины основания
        basis_first_point = basis_center.copy()
        basis_second_point = basis_center.copy()
        basis_third_point = basis_center.copy()

        # Радиус вписанной в основание окружности
        r = (math.sqrt(2) * sphere_radius) / 3
        # Сторона тэтраедра
        a = (sphere_radius / math.sqrt(6)) * 2

        basis_first_point[Z] = basis_first_point[Z] - r
        basis_first_point[X] = basis_first_point[X] - a

        basis_second_point[Z] = basis_second_point[Z] - r
        basis_second_point[X] = basis_second_point[X] + a

        # Радиус описанной окружности
        r = (2 * sphere_radius * math.sqrt(2)) / 3
        basis_third_point[Z] = basis_third_point[Z] + r

        return np.array([
            [top, basis_third_point, basis_first_point],
            [top, basis_second_point, basis_third_point],
            [top, basis_first_point, basis_second_point],
            [basis_first_point, basis_third_point,  basis_second_point]
        ], dtype=np.float)


class Hexahedron(Polyhedron):
    """
    Куб. Задается центром и радиусом описанной сферы.
    :param sphere_center: np.ndarray с размерностью (3, ). Если не передано,
                          то центр устанавливается в начало координат
    :param sphere_radius: float. Если не передано, то радиус устанавливается равным 10.0
    """
    def __init__(self, sphere_center: np.ndarray = None, sphere_radius: float = None):
        Polyhedron.__init__(self, sphere_center, sphere_radius, "Hexahedron")

    def triangulation(self) -> (np.ndarray, int):
        result_of_triangulation: np.ndarray = np.ones(shape=(12, 3, 3), dtype=float)
        for i in range(0, self._vertexes.shape[0]):
            current_vertexes = self._vertexes[i]
            result_of_triangulation[2 * i, 0] = current_vertexes[0]
            result_of_triangulation[2 * i, 1] = current_vertexes[1]
            result_of_triangulation[2 * i, 2] = current_vertexes[2]

            result_of_triangulation[2 * i + 1, 0] = current_vertexes[0]
            result_of_triangulation[2 * i + 1, 1] = current_vertexes[2]
            result_of_triangulation[2 * i + 1, 2] = current_vertexes[3]

        return result_of_triangulation, 2

    @staticmethod
    def texture_coordinates(target_width, target_height) -> np.ndarray:
        left: float = 0.05
        right: float = 0.95

        first: list = [target_width * left, target_height * right]
        second: list = [target_width * right, target_height * right]
        third: list = [target_width * right, target_height * left]
        fourth: list = [target_width * left, target_height * left]

        return np.array([
            [
                first,
                second,
                third
            ],
            [
                first,
                third,
                fourth
            ]
        ], dtype=np.float64)

    @staticmethod
    def colors() -> list:
        return [np.array([255, 255, 255]), np.array([0, 150, 150]),
                np.array([0, 70, 0]), np.array([20, 125, 0]),
                np.array([255, 220, 0]), np.array([220, 0, 255])]

    def _construction(self, sphere_center: np.ndarray, sphere_radius: float) -> np.ndarray:
        # Сторона куба
        a = (2 * sphere_radius) / math.sqrt(3)
        # Радиус вписанной сферы
        r_inner = a / 2.
        # Радиус верхней грани
        r = math.sqrt(math.pow(sphere_radius, 2) - math.pow(r_inner, 2))

        # Поиск центра верхней грани
        top_verge_center = sphere_center.copy()
        top_verge_center[Y] = top_verge_center[Y] + r_inner

        # Поиск вершин верхней грани
        first_top = top_verge_center.copy()
        second_top = top_verge_center.copy()
        third_top = top_verge_center.copy()
        fourth_top = top_verge_center.copy()
        first_top[X] = first_top[X] - r
        third_top[X] = third_top[X] + r
        second_top[Z] = second_top[Z] - r
        fourth_top[Z] = fourth_top[Z] + r

        # Поиск вершин нижней грани
        first_bottom = first_top.copy()
        second_bottom = second_top.copy()
        third_bottom = third_top.copy()
        fourth_bottom = fourth_top.copy()
        first_bottom[Y] = first_bottom[Y] - a
        second_bottom[Y] = second_bottom[Y] - a
        third_bottom[Y] = third_bottom[Y] - a
        fourth_bottom[Y] = fourth_bottom[Y] - a

        return np.array([
            [first_top, second_top, third_top, fourth_top],
            [first_top, fourth_top, fourth_bottom, first_bottom],
            [second_top, first_top, first_bottom, second_bottom],
            [third_top, second_top, second_bottom, third_bottom],
            [fourth_top, third_top, third_bottom, fourth_bottom],
            [fourth_bottom, third_bottom, second_bottom, first_bottom]
        ], dtype=np.float)


class Octahedron(Polyhedron):
    """
    Октаэдр. Задается центром и радиусом описанной сферы.
    :param sphere_center: np.ndarray с размерностью (3, ). Если не передано,
                          то центр устанавливается в начало координат
    :param sphere_radius: float. Если не передано, то радиус устанавливается равным 10.0
    """
    def __init__(self, sphere_center: np.ndarray = None, sphere_radius: float = None):
        Polyhedron.__init__(self, sphere_center, sphere_radius, "Octahedron")

    def _construction(self, sphere_center: np.ndarray, sphere_radius: float) -> np.ndarray:
        # Верхняя вершина
        top = sphere_center.copy()
        top[Y] = top[Y] + sphere_radius

        # Нижняя вершина
        bottom = sphere_center.copy()
        bottom[Y] = bottom[Y] - sphere_radius

        # Точки одной высоты с центром сферы
        first = sphere_center.copy()
        second = sphere_center.copy()
        third = sphere_center.copy()
        fourth = sphere_center.copy()

        first[X] = first[X] - sphere_radius
        second[Z] = second[Z] - sphere_radius
        third[X] = third[X] + sphere_radius
        fourth[Z] = fourth[Z] + sphere_radius

        return np.array([
            [top, second, first],
            [top, third, second],
            [top, fourth, third],
            [top, first, fourth],
            [bottom, first, second],
            [bottom, second, third],
            [bottom, third, fourth],
            [bottom, fourth, first]
        ], dtype=np.float)

    @staticmethod
    def colors() -> list:
        return [np.array([0, 0, 128]), np.array([114, 128, 250]),
                np.array([0, 140, 255]), np.array([50, 205, 154]),
                np.array([144, 238, 144]), np.array([143, 188, 143]),
                np.array([127, 255, 0]), np.array([204, 209, 72])]


class Icosahedron(Polyhedron):
    """
    Икосаэдр. Задается центром и радиусом описанной сферы.
    :param sphere_center: np.ndarray с размерностью (3, ). Если не передано,
                          то центр устанавливается в начало координат
    :param sphere_radius: float. Если не передано, то радиус устанавливается равным 10.0
    """
    def __init__(self, sphere_center: np.ndarray = None, sphere_radius: float = None):
        Polyhedron.__init__(self, sphere_center, sphere_radius, "Icosahedron")

    @staticmethod
    def colors() -> list:
        return [np.array([120, 45, 142]), np.array([0, 52, 144]),
                np.array([0, 70, 8]), np.array([12, 21, 98]),
                np.array([2, 12, 13]), np.array([202, 0, 45]),
                np.array([15, 255, 55]), np.array([0, 0, 150]),
                np.array([0, 13, 0]), np.array([2, 125, 10]),
                np.array([255, 20, 0]), np.array([22, 0, 55]),
                np.array([78, 29, 44]), np.array([100, 15, 50]),
                np.array([0, 70, 0]), np.array([20, 1, 12]),
                np.array([13, 13, 13]), np.array([6, 6, 6]),
                np.array([67, 12, 82]), np.array([0, 0, 77])]

    def _construction(self, sphere_center: np.ndarray, sphere_radius: float) -> np.ndarray:
        # Верхняя вершина
        top = sphere_center.copy()
        top[Y] = top[Y] + sphere_radius

        # Нижняя вершина
        bottom = sphere_center.copy()
        bottom[Y] = bottom[Y] - sphere_radius

        # Нахождение стороны, радиуса сечений и смещения от вершины
        a = (4 * sphere_radius) / math.sqrt(2 * (5 + math.sqrt(5)))
        r = math.sqrt(math.pow(a, 2) - math.pow(a, 4)/(4 * math.pow(sphere_radius, 2)))
        y = math.pow(a, 2) / (2 * sphere_radius)

        # Угол поворота вершин сечений в плоскости, параллельной OXZ
        angle = math.radians(72.)

        # Центр верхнего сечения
        center_top = top.copy()
        center_top[Y] = center_top[Y] - y
        rotation_pivot = (center_top[X], center_top[Z])

        # Нахождение точек верхнего сечения
        first_top = center_top.copy()
        first_top[X] = first_top[X] - r

        top_points = [first_top]
        prev_point = first_top
        for i in range(4):
            current_point = prev_point.copy()
            rotation_point = np.array([[current_point[X], current_point[Z]]], dtype=np.float)
            rotation_point = rotation(rotation_point, angle, rotation_pivot)[0]
            current_point[X] = rotation_point[0]
            current_point[Z] = rotation_point[1]
            top_points.append(current_point)
            prev_point = current_point

        # Центр нижнего сечения
        bottom_center = bottom.copy()
        bottom_center[Y] = bottom_center[Y] + y
        rotation_pivot = (bottom_center[X], bottom_center[Z])

        # Нахождение точек нижнего сечения
        first_bottom = bottom_center.copy()
        first_bottom[X] = first_bottom[X] - r
        t_angle = math.radians(36.)
        t_rotation_point = np.array([[first_bottom[X], first_bottom[Z]]], dtype=np.float)
        t_rotation_point = rotation(t_rotation_point, t_angle, rotation_pivot)[0]
        first_bottom[X] = t_rotation_point[0]
        first_bottom[Z] = t_rotation_point[1]

        bottom_points = [first_bottom]
        prev_point = first_bottom
        for i in range(4):
            current_point = prev_point.copy()
            rotation_point = np.array([[current_point[X], current_point[Z]]], dtype=np.float)
            rotation_point = rotation(rotation_point, angle, rotation_pivot)[0]
            current_point[X] = rotation_point[0]
            current_point[Z] = rotation_point[1]
            bottom_points.append(current_point)
            prev_point = current_point

        return np.array([
            [top, top_points[0], top_points[1]],
            [top, top_points[1], top_points[2]],
            [top, top_points[2], top_points[3]],
            [top, top_points[3], top_points[4]],
            [top, top_points[4], top_points[0]],
            [bottom, bottom_points[1], bottom_points[0]],
            [bottom, bottom_points[0], bottom_points[4]],
            [bottom, bottom_points[4], bottom_points[3]],
            [bottom, bottom_points[3], bottom_points[2]],
            [bottom, bottom_points[2], bottom_points[1]],
            [top_points[0], bottom_points[4], bottom_points[0]],
            [top_points[1], bottom_points[0], bottom_points[1]],
            [top_points[2], bottom_points[1], bottom_points[2]],
            [top_points[3], bottom_points[2], bottom_points[3]],
            [top_points[4], bottom_points[3], bottom_points[4]],
            [bottom_points[4], top_points[0], top_points[4]],
            [bottom_points[3], top_points[4], top_points[3]],
            [bottom_points[2], top_points[3], top_points[2]],
            [bottom_points[1], top_points[2], top_points[1]],
            [bottom_points[0], top_points[1], top_points[0]]
        ], dtype=np.float)


class Dodecahedron(Polyhedron):
    """
    Додекаэдр. Задается центром и радиусом описанной сферы.
    :param sphere_center: np.ndarray с размерностью (3, ). Если не передано,
                          то центр устанавливается в начало координат
    :param sphere_radius: float. Если не передано, то радиус устанавливается равным 10.0
    """
    def __init__(self, sphere_center: np.ndarray = None, sphere_radius: float = None):
        Polyhedron.__init__(self, sphere_center, sphere_radius, "Dodecahedron")

    def _construction(self, sphere_center: np.ndarray, sphere_radius: float) -> np.ndarray:
        # Нахождение стороны
        a = (4 * sphere_radius) / ((1 + math.sqrt(5)) * math.sqrt(3))
        # Радиус вписанной сферы
        r = (a / 4.) * math.sqrt(10 + (22 / math.sqrt(5)))
        # Смещение в верхней плоскости сечения от центра
        x = math.sqrt(math.pow(sphere_radius, 2) - math.pow(r, 2))
        # Диагональ грани = сторона пятиугольника сечения
        d = (a * (math.sqrt(5) + 1)) / 2.
        # Радиус описанной окло сечения окружности
        r_section = (math.sqrt(10) * math.sqrt(5 + 2 * math.sqrt(5)) * d) / 10.

        # Нахождение смещения для точки сечения
        z = r_section - x
        b = math.sqrt(math.pow(a, 2) - math.pow(z, 2))

        # Углы повортов
        angle = math.radians(72.)
        t_angle = math.radians(36.)

        # Центр верхнего сечения
        top_center = sphere_center.copy()
        top_center[Y] = top_center[Y] + r
        rotation_pivot = (top_center[X], top_center[Z])

        # Нахождение точек верхнего сечения
        first_top = top_center.copy()
        first_top[X] = first_top[X] - x

        top_points = [first_top]
        prev_point = first_top
        for i in range(4):
            current_point = prev_point.copy()
            rotation_point = np.array([[
                current_point[X], current_point[Z]
            ]], dtype=np.float)
            rotation_point = rotation(rotation_point, angle, rotation_pivot)[0]
            current_point[X] = rotation_point[0]
            current_point[Z] = rotation_point[1]
            top_points.append(current_point)
            prev_point = current_point

        # Центр верхнего из средних сечений
        top_m_center = top_center.copy()
        top_m_center[Y] = top_m_center[Y] - b
        rotation_pivot = (top_m_center[X], top_m_center[Z])

        # Нахождения точек верхнего из средних сечений
        first_m_top = top_m_center.copy()
        first_m_top[X] = first_m_top[X] - r_section

        top_m_points = [first_m_top]
        prev_point = first_m_top
        for i in range(4):
            current_point = prev_point.copy()
            rotation_point = np.array([[
                current_point[X], current_point[Z]
            ]], dtype=np.float)
            rotation_point = rotation(rotation_point, angle, rotation_pivot)[0]
            current_point[X] = rotation_point[0]
            current_point[Z] = rotation_point[1]
            top_m_points.append(current_point)
            prev_point = current_point

        # Нахождения центра нижнего сечения
        bottom_center = sphere_center.copy()
        bottom_center[Y] = bottom_center[Y] - r
        rotation_pivot = (bottom_center[X], bottom_center[Z])

        # Нахождени точек нижнего сечения
        first_bottom = bottom_center.copy()
        first_bottom[X] = first_bottom[X] - x
        t_rotation_point = np.array([[first_bottom[X], first_bottom[Z]]], dtype=np.float)
        t_rotation_point = rotation(t_rotation_point, t_angle, rotation_pivot)[0]
        first_bottom[X] = t_rotation_point[0]
        first_bottom[Z] = t_rotation_point[1]

        bottom_points = [first_bottom]
        prev_point = first_bottom
        for i in range(4):
            current_point = prev_point.copy()
            rotation_point = np.array([[
                current_point[X], current_point[Z]
            ]], dtype=np.float)
            rotation_point = rotation(rotation_point, angle, rotation_pivot)[0]
            current_point[X] = rotation_point[0]
            current_point[Z] = rotation_point[1]
            bottom_points.append(current_point)
            prev_point = current_point

        # Нахождени центра нижнего из средних сечений
        bottom_m_center = bottom_center.copy()
        bottom_m_center[Y] = bottom_m_center[Y] + b
        rotation_pivot = (bottom_m_center[X], bottom_m_center[Z])

        # Нахождение точек нижнего из средних сечений
        first_m_bottom = bottom_m_center.copy()
        first_m_bottom[X] = first_m_bottom[X] - r_section
        t_rotation_point = np.array([[
            first_m_bottom[X], first_m_bottom[Z]
        ]], dtype=np.float)
        t_rotation_point = rotation(t_rotation_point, t_angle, rotation_pivot)[0]
        first_m_bottom[X] = t_rotation_point[0]
        first_m_bottom[Z] = t_rotation_point[1]

        bottom_m_points = [first_m_bottom]
        prev_point = first_m_bottom
        for i in range(4):
            current_point = prev_point.copy()
            rotation_point = np.array([[
                current_point[X], current_point[Z]
            ]], dtype=np.float)
            rotation_point = rotation(rotation_point, angle, rotation_pivot)[0]
            current_point[X] = rotation_point[0]
            current_point[Z] = rotation_point[1]
            bottom_m_points.append(current_point)
            prev_point = current_point

        return np.array([
            [top_points[0], top_points[1], top_points[2], top_points[3], top_points[4]],
            [top_points[0], top_m_points[0], bottom_m_points[0], top_m_points[1], top_points[1]],
            [top_points[1], top_m_points[1], bottom_m_points[1], top_m_points[2], top_points[2]],
            [top_points[2], top_m_points[2], bottom_m_points[2], top_m_points[3], top_points[3]],
            [top_points[3], top_m_points[3], bottom_m_points[3], top_m_points[4], top_points[4]],
            [top_points[4], top_m_points[4], bottom_m_points[4], top_m_points[0], top_points[0]],
            [bottom_points[0], bottom_points[4], bottom_points[3], bottom_points[2], bottom_points[1]],
            [bottom_points[0], bottom_m_points[0], top_m_points[0], bottom_m_points[4], bottom_points[4]],
            [bottom_points[4], bottom_m_points[4], top_m_points[4], bottom_m_points[3], bottom_points[3]],
            [bottom_points[3], bottom_m_points[3], top_m_points[3], bottom_m_points[2], bottom_points[2]],
            [bottom_points[2], bottom_m_points[2], top_m_points[2], bottom_m_points[1], bottom_points[1]],
            [bottom_points[1], bottom_m_points[1], top_m_points[1], bottom_m_points[0], bottom_points[0]]
        ], dtype=np.float)

    @staticmethod
    def colors() -> list:
        return [np.array([34, 78, 2]), np.array([77, 54, 32]),
                np.array([125, 38, 64]), np.array([4, 32, 128]),
                np.array([128, 32, 4]), np.array([198, 244, 218]),
                np.array([48, 166, 244]), np.array([251, 187, 96]),
                np.array([4, 127, 15]), np.array([3, 99, 12]),
                np.array([12, 98, 3]), np.array([99, 98, 12])]

    def triangulation(self) -> (np.ndarray, int):
        result_of_triangulation: np.ndarray = np.ones(shape=(36, 3, 3), dtype=float)
        for i in range(0, self._vertexes.shape[0]):
            current_vertexes = self._vertexes[i]
            result_of_triangulation[3 * i, 0] = current_vertexes[0]
            result_of_triangulation[3 * i, 1] = current_vertexes[1]
            result_of_triangulation[3 * i, 2] = current_vertexes[2]

            result_of_triangulation[3 * i + 1, 0] = current_vertexes[0]
            result_of_triangulation[3 * i + 1, 1] = current_vertexes[2]
            result_of_triangulation[3 * i + 1, 2] = current_vertexes[3]

            result_of_triangulation[3 * i + 2, 0] = current_vertexes[0]
            result_of_triangulation[3 * i + 2, 1] = current_vertexes[3]
            result_of_triangulation[3 * i + 2, 2] = current_vertexes[4]

        return result_of_triangulation, 3

    @staticmethod
    def texture_coordinates(target_width, target_height) -> np.ndarray:
        right: float = 0.95
        middle: float = 0.5

        rotation_angle: float = math.radians(72.)
        center: tuple = (target_width * middle, target_height * middle)
        first: list = [target_width * middle, target_height * right]
        second: list = rotation(np.array([first]), rotation_angle, center).tolist()[0]
        third: list = rotation(np.array([first]), 2 * rotation_angle, center).tolist()[0]
        fourth: list = rotation(np.array([first]), 3 * rotation_angle, center).tolist()[0]
        fifth: list = rotation(np.array([first]), -1 * rotation_angle, center).tolist()[0]

        return np.array([
            [
                first,
                second,
                third
            ],
            [
                first,
                third,
                fourth
            ],
            [
                first,
                fourth,
                fifth
            ],
        ], dtype=np.float64)

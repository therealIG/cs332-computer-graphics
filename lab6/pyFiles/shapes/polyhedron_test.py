from lab6.pyFiles.shapes.shapes import Tetrahedron, Hexahedron, Octahedron, Icosahedron, Dodecahedron
import numpy as np
import matplotlib.pyplot as plt
import h5py


if __name__ == "__main__":
    shape = Tetrahedron(np.array([1, 2, 5]), sphere_radius=4.0)
    with h5py.File('/home/konstantin/PycharmProjects/cs332-computer-graphics/lab7/data/test.h5', 'w') as hf:
        hf.create_dataset("test", data=shape.vertexes())

import numpy as np
from lab6.pyFiles.shapes.geometric_primitives import Polyhedron
from lab6.pyFiles.shapes.geometric_primitives import Point


def _prepare_polyhedron(polyhedron: Polyhedron) -> np.ndarray:
    return np.dstack((polyhedron.vertexes(), np.ones(polyhedron.vertexes().shape[:2], dtype=np.float64)))


def _prepare_points(points: np.ndarray) -> np.ndarray:
    return np.dstack((points, np.ones(points.shape[:2], dtype=np.float64)))


def shift_x(points: np.ndarray, shift) -> np.ndarray:
    """
    Смещение по оси X
    :param points: Координаты вершин фигуры
    :param shift: Смещение
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[1, 0, 0, shift],
                    [0, 1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def shift_y(points: np.ndarray, shift) -> np.ndarray:
    """
    Смещение по оси Y
    :param points: Координаты вершин фигуры
    :param shift: Смещение
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[1, 0, 0, 0],
                    [0, 1, 0, shift],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def shift_z(points: np.ndarray, shift) -> np.ndarray:
    """
    Смещение по оси Z
    :param points: Координаты вершин фигуры
    :param shift: Смещение
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 1, shift],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def rotate_around_y(points: np.ndarray, angle) -> np.ndarray:
    """
    Поворот вокруг оси Y
    :param points: Координаты вершин фигуры
    :param angle: Угол
    :return: Координаты вершин фигуры
    """
    angle = np.radians(angle)
    cos = np.cos(-angle)
    sin = np.sin(-angle)
    mtx = np.array([[cos, 0, -sin, 0],
                    [0, 1, 0, 0],
                    [sin, 0, cos, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def rotate_around_x(points: np.ndarray, angle) -> np.ndarray:
    """
    Поворот вокруг оси X
    :param points: Координаты вершин фигуры
    :param angle: Угол
    :return: Координаты вершин фигуры
    """
    angle = np.radians(angle)
    cos = np.cos(angle)
    sin = np.sin(angle)
    mtx = np.array([[1, 0, 0, 0],
                    [0, cos, -sin, 0],
                    [0, sin, cos, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def rotate_around_z(points: np.ndarray, angle) -> np.ndarray:
    """
    Поворот вокруг оси Z
    :param points: Координаты вершин фигуры
    :param angle: Угол
    :return: Координаты вершин фигуры
    """
    angle = np.radians(angle)
    cos = np.cos(angle)
    sin = np.sin(angle)
    mtx = np.array([[cos, -sin, 0, 0],
                    [sin, cos, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def scale_x(points: np.ndarray, scale_factor) -> np.ndarray:
    """
    Масштабирование по оси X
    :param points: Координаты вершин фигуры
    :param scale_factor: Коэффициент масштабирования
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[scale_factor, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def scale_y(points: np.ndarray, scale_factor) -> np.ndarray:
    """
    Масштабирование по оси Y
    :param points: Координаты вершин фигуры
    :param scale_factor: Коэффициент масштабирования
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[1, 0, 0, 0],
                    [0, scale_factor, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def scale_z(points: np.ndarray, scale_factor) -> np.ndarray:
    """
    Масштабирование по оси Y
    :param points: Координаты вершин фигуры
    :param scale_factor: Коэффициент масштабирования
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, scale_factor, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def reflect_relative_x(points: np.ndarray) -> np.ndarray:
    """
    Отражение относительно оси X
    :param points: Координаты вершин фигуры
    :return:
    """
    mtx = np.array([[-1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def reflect_relative_y(points: np.ndarray) -> np.ndarray:
    """
    Отражение относительно оси Y
    :param points: Координаты вершин фигуры
    :return:
    """
    mtx = np.array([[1, 0, 0, 0],
                    [0, -1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def reflect_relative_z(points: np.ndarray) -> np.ndarray:
    """
    Отражение относительно оси Z
    :param points: Координаты вершин фигуры
    :return:
    """
    mtx = np.array([[1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, -1, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def scale_center(points: np.ndarray, scale_factor) -> np.ndarray:
    """
    Масштабирование отсносительно своего центра
    :param points: Координаты вершин фигуры
    :param scale_factor: Коэффициент масштабирования
    :return: Координаты вершин фигуры
    """
    mtx = np.array([[scale_factor, 0, 0, 0],
                    [0, scale_factor, 0, 0],
                    [0, 0, scale_factor, 0],
                    [0, 0, 0, 1]
                    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def rotate_around_straight(points: np.ndarray, pt1: Point, pt2: Point, angle: np.float64) -> np.ndarray:
    """
    Поворот вокруг произвольной прямой на заданный угол
    :param points: Координаты вершин фигуры
    :param pt1: Первая точка, задающая прямую
    :param pt2: Вторая точка, задающая прямую
    :param angle: Угол поворота
    :return: Координаты вершин фигуры
    """
    a, b, c = pt1.position()[0]
    l, m, n = (pt2.position()[0] - pt1.position()[0]) / np.linalg.norm((pt2.position()[0] - pt1.position()[0]))
    cos = np.cos
    sin = np.sin
    angle = np.radians(angle)
    mtx = np.array([
        [l ** 2 + cos(angle) * (1 - l ** 2), l * (1 - cos(angle)) * m + n * sin(angle),
         l * (1 - cos(angle)) * n - m * sin(angle), 0],
        [l * (1 - cos(angle)) * m - n * sin(angle), m ** 2 + cos(angle) * (1 - m ** 2),
         m * (1 - cos(angle)) * n + l * sin(angle), 0],
        [l * (1 - cos(angle)) * n + m * sin(angle), m * (1 - cos(angle)) * n - l * sin(angle),
         n ** 2 + cos(angle) * (1 - n ** 2), 0],
        [0, 0, 0, 1]
    ])
    return np.matmul(_prepare_points(points), mtx.T)[:, :, :3]


def rotate_around_str_centered_parallel_x(points: np.ndarray, angle: np.float64) -> np.ndarray:
    """
    Поворот многогранника вокруг прямой проходящей через центр многогранника, параллельно оси X.
    :param points: Координаты вершин фигуры
    :param angle: Угол поворота
    :return: Координаты вершин фигуры
    """
    pt1 = Point(points[:, :, 0].mean(), points[:, :, 1].mean(), points[:, :, 2].mean())
    pt2 = Point(points[:, :, 0].mean() + 10, points[:, :, 1].mean(), points[:, :, 2].mean())
    return rotate_around_straight(points, pt1, pt2, angle)


def rotate_around_str_centered_parallel_y(points: np.ndarray, angle: np.float64) -> np.ndarray:
    """
    Поворот многогранника вокруг прямой проходящей через центр многогранника, параллельно оси Y.
    :param points: Координаты вершин фигуры
    :param angle: Угол поворота
    :return: Координаты вершин фигуры
    """
    pt1 = Point(points[:, :, 0].mean(), points[:, :, 1].mean(), points[:, :, 2].mean())
    pt2 = Point(points[:, :, 0].mean(), points[:, :, 1].mean() + 10, points[:, :, 2].mean())
    return rotate_around_straight(points, pt1, pt2, angle)


def rotate_around_str_centered_parallel_z(points: np.ndarray, angle: np.float64) -> np.ndarray:
    """
    Поворот многогранника вокруг прямой проходящей через центр многогранника, параллельно оси Z.
    :param points: Координаты вершин фигуры
    :param angle: Угол поворота
    :return: Координаты вершин фигуры
    """
    pt1 = Point(points[:, :, 0].mean(), points[:, :, 1].mean(), points[:, :, 2].mean())
    pt2 = Point(points[:, :, 0].mean(), points[:, :, 1].mean(), points[:, :, 2].mean() + 10)
    return rotate_around_straight(points, pt1, pt2, angle)

import numpy as np


def _preparing_points(points: np.ndarray) -> np.ndarray:
    return np.concatenate((
        points,
        np.ones((points.shape[0], points.shape[1], 1), dtype=np.float64)
    ), -1)

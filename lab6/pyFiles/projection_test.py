from lab6.pyFiles.projection import perspective_projection, orto_projection, isometric_projection
from lab6.pyFiles.shapes.shapes import *
import matplotlib.pyplot as plt
from matplotlib import collections


if __name__ == "__main__":
    shape = Hexahedron(np.array([5, -3, 1]), sphere_radius=2)
    print(shape)
    # shape_on_perspective = perspective_projection(shape.vertexes(), np.radians(30), 3/2., 100, 1)
    # shape_on_perspective = orto_projection(shape_on_perspective, 2)
    shape_on_perspective = isometric_projection(shape.vertexes(), 45, 45)
    shape.vertexes(shape_on_perspective)
    print(shape)
    fig = plt.figure(figsize=(20, 20))
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    ax.add_collection(
        collections.PolyCollection(shape.vertexes()[:, :, :2], facecolors='cyan', linewidths=1, edgecolors='r'))
    X = shape.vertexes()[:, :, 0]
    Y = shape.vertexes()[:, :, 1]
    ax.set_xticks(np.arange(-10, 10, 1))
    ax.set_yticks(np.arange(-10, 10, 1))
    ax.set_xlabel('Y')
    ax.set_ylabel('Z')
    plt.show()
import h5py
import matplotlib.pyplot as plt
import numpy as np
import math

from PyQt5.QtWidgets import QInputDialog
from matplotlib.collections import PolyCollection
from matplotlib.widgets import Button, Slider, TextBox, RadioButtons
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import cv2
import os

os.sys.path.append("./../../")
from lab6.pyFiles.projection import perspective_projection, isometric_projection, orto_projection
from lab6.pyFiles.shapes.shapes import Hexahedron, Tetrahedron, Dodecahedron, Icosahedron, Octahedron
from lab6.pyFiles.affine_transform import *
import re

fig = plt.figure()

current_figure = "Tetrahedron"
current_sphere_radius = 5
current_sphere_center = np.array([0, 0, 0])
current_state = Tetrahedron(sphere_center=current_sphere_center, sphere_radius=current_sphere_radius).vertexes()
straight_for_rotation = "X"
point1 = Point(0, 0, 0)
point2 = Point(0, 0, 0)
value_scale = 1
value_scale_x = 1
value_scale_y = 1
value_scale_z = 1
value_offset_x = 0
value_offset_y = 0
value_offset_z = 0
value_turn_x = 0
value_turn_y = 0
value_turn_z = 0
value_rotation_line = 0
value_rotation_point = 0


def update():
    global current_sphere_center
    global current_figure
    global current_sphere_radius
    get_new_state(current_figure, current_sphere_radius, current_sphere_center)
    plot()


ax = fig.add_subplot(111)


def plot():
    global current_state
    # X = current_state[:, :, 0]
    # Y = current_state[:, :, 1]
    # Z = current_state[:, :, 2]
    ax.cla()
    # ax.scatter3D(X, Y, Z)
    # ax.add_collection3d(Poly3DCollection(current_state, facecolors='cyan', linewidths=1, edgecolors='r', alpha=.25))
    # ax.set_xlim3d(-50, 50)
    # ax.set_ylim3d(-50, 50)
    # ax.set_zlim3d(-50, 50)
    # ax.set_zticks(np.arange(-50, 50, 10))
    # ax.set_yticks(np.arange(-50, 50, 10))
    # ax.set_xticks(np.arange(-50, 50, 10))
    # ax.set_xlabel('X')
    # ax.set_ylabel('Y')
    # ax.set_zlabel('Z')
    shape_on_perspective = isometric_projection(current_state, 90, 0)
    # ax.set_aspect('equal')
    ax.add_collection(PolyCollection(shape_on_perspective[:, :, :2], facecolors='cyan', linewidths=1, edgecolors='r'))
    X = shape_on_perspective[:, :, 0]
    Y = shape_on_perspective[:, :, 1]
    ax.set_xticks(np.arange(-50, 50, 10))
    ax.set_yticks(np.arange(-50, 50, 10))
    ax.set_xlabel('Z')
    ax.set_ylabel('Y')

    # изометрическая проекция
    val = isometric_projection(current_state, 45, 45)[:, :, :2]
    mask = np.ones((512, 512, 3), np.uint8) * 255
    contours = [[0] * len(val[0]) for i in range(len(val))]
    for i in range(len(val)):
        for j in range(len(val[i])):
            contours[i][j] = (val[i][j][0] * 5.12 + 256, val[i][j][1] * 5.12 + 256)
    for i in range(len(val)):
        list_contours = []
        for j in range(len(val[i])):
            list_contours.append(contours[i][j])
        triangle_cnt = np.array(list_contours, dtype=np.int64)
        cv2.drawContours(mask, [triangle_cnt], 0, (0, 200, 255), -1)
        cv2.drawContours(mask, [triangle_cnt], 0, (255, 0, 33), 1)
    cv2.imwrite("isometric_projection.png", mask)
    # -----------------------------------------------------------------------------------

    # ортографическая проекция
    val = orto_projection(current_state, 2)[:, :, :2]
    mask = np.ones((512, 512, 3), np.uint8) * 255
    contours = [[0] * len(val[0]) for i in range(len(val))]
    for i in range(len(val)):
        for j in range(len(val[i])):
            contours[i][j] = (val[i][j][0] * 5.12 + 256, val[i][j][1] * 5.12 + 256)
    for i in range(len(val)):
        list_contours = []
        for j in range(len(val[i])):
            list_contours.append(contours[i][j])
        triangle_cnt = np.array(list_contours, dtype=np.int64)
        cv2.drawContours(mask, [triangle_cnt], 0, (0, 200, 255), -1)
        cv2.drawContours(mask, [triangle_cnt], 0, (255, 0, 33), 1)
    cv2.imwrite("orto_projection.png", mask)


plt.subplots_adjust(0.4, 0.1, 0.9, 0.9, 0.1, 0.1)
plot()


# Выбор фигуры для отображения
def get_new_state(figure, radius, center):
    global current_state
    if figure == "Tetrahedron":
        current_state = Tetrahedron(sphere_center=center, sphere_radius=radius).vertexes()
    if figure == "Hexahedron":
        current_state = Hexahedron(sphere_center=center, sphere_radius=radius).vertexes()
    if figure == "Octahedron":
        current_state = Octahedron(sphere_center=center, sphere_radius=radius).vertexes()
    if figure == "Icosahedron":
        current_state = Icosahedron(sphere_center=center, sphere_radius=radius).vertexes()
    if figure == "Dodecahedron":
        current_state = Dodecahedron(sphere_center=center, sphere_radius=radius).vertexes()
    plot()


rax = plt.axes([0.005, 0, 0.2, 0.135])
labels = ["Tetrahedron", "Hexahedron", "Octahedron", "Icosahedron", "Dodecahedron"]
check = RadioButtons(rax, labels)
for circle in check.labels:
    circle.set_fontsize(7)
    circle.set_fontweight("bold")
for circle in check.circles:
    circle.set_radius(0.04)


def set_figur(label):
    global current_state
    global current_sphere_center
    global current_sphere_radius
    global current_figure
    if label == "Tetrahedron":
        current_state = Tetrahedron(sphere_center=current_sphere_center, sphere_radius=current_sphere_radius).vertexes()
    if label == "Hexahedron":
        current_state = Hexahedron(sphere_center=current_sphere_center, sphere_radius=current_sphere_radius).vertexes()
    if label == "Octahedron":
        current_state = Octahedron(sphere_center=current_sphere_center, sphere_radius=current_sphere_radius).vertexes()
    if label == "Icosahedron":
        current_state = Icosahedron(sphere_center=current_sphere_center, sphere_radius=current_sphere_radius).vertexes()
    if label == "Dodecahedron":
        current_state = Dodecahedron(sphere_center=current_sphere_center,
                                     sphere_radius=current_sphere_radius).vertexes()
    current_figure = label
    # Обнуление всех значений при выборе новой фигуры
    global point1, point2, value_scale, value_scale_x, value_scale_y, value_scale_z, value_offset_x, value_offset_y, \
        value_offset_z, value_turn_x, value_turn_y, value_turn_z, value_rotation_line, value_rotation_point
    point1 = Point(np.float64(0), np.float64(0), np.float64(0))
    point2 = Point(np.float64(0), np.float64(0), np.float64(0))
    value_scale = 1
    value_scale_x = 1
    value_scale_y = 1
    value_scale_z = 1
    value_offset_x = 0
    value_offset_y = 0
    value_offset_z = 0
    value_turn_x = 0
    value_turn_y = 0
    value_turn_z = 0
    value_rotation_line = 0
    value_rotation_point = 0

    # Обнудение всех значений для слайдеров
    global slider_offset_X, slider_offset_Y, slider_offset_Z, slider_scale_X, slider_scale_Y, slider_scale_Z, \
        slider_scale_center, slider_turn_X, slider_turn_Y, slider_turn_Z, slider_rotation_point, slider_rotation, \
        text_box_coordinate_point, text_box_coordinate_point1
    slider_offset_X.reset()
    slider_offset_Y.reset()
    slider_offset_Z.reset()
    slider_scale_X.reset()
    slider_scale_Y.reset()
    slider_scale_Z.reset()
    slider_scale_center.reset()
    slider_turn_X.reset()
    slider_turn_Y.reset()
    slider_turn_Z.reset()
    slider_rotation.reset()
    slider_rotation_point.reset()
    text_box_coordinate_point.set_val("0,0,0")
    text_box_coordinate_point1.set_val("0,0,0")
    plot()


check.on_clicked(set_figur)


# -----------------------------------------------------------------------------------

# Смещение
def update_offset_X(val):
    global current_state, value_offset_x
    current_state = shift_x(current_state, slider_offset_X.val - value_offset_x)
    value_offset_x = slider_offset_X.val
    plot()


def update_offset_Y(val):
    global current_state, value_offset_y
    current_state = shift_y(current_state, slider_offset_Y.val - value_offset_y)
    value_offset_y = slider_offset_Y.val
    plot()


def update_offset_Z(val):
    global current_state, value_offset_z
    current_state = shift_z(current_state, slider_offset_Z.val - value_offset_z)
    value_offset_z = slider_offset_Z.val
    plot()


fig.text(0.005, 0.30, "Shift (X,Y,Z)", fontsize=7, fontweight="bold")
rax_slider_offset_X = fig.add_axes([0.005, 0.275, 0.25, 0.02])
slider_offset_X = Slider(rax_slider_offset_X, " ", -50.0, 50.0, valinit=0.0, valstep=1.0)
slider_offset_X.on_changed(update_offset_X)

rax_slider_offset_Y = fig.add_axes([0.005, 0.245, 0.25, 0.02])
slider_offset_Y = Slider(rax_slider_offset_Y, " ", -50.0, 50.0, valinit=0.0, valstep=1.0)
slider_offset_Y.on_changed(update_offset_Y)

rax_slider_offset_Z = fig.add_axes([0.005, 0.215, 0.25, 0.02])
slider_offset_Z = Slider(rax_slider_offset_Z, " ", -50.0, 50.0, valinit=0.0, valstep=1.0)
slider_offset_Z.on_changed(update_offset_Z)


# -----------------------------------------------------------------------------------

# Поворот
def update_turn_X(val):
    global current_state, value_turn_x
    current_state = rotate_around_x(current_state, slider_turn_X.val - value_turn_x)
    value_turn_x = slider_turn_X.val
    plot()


def update_turn_Y(val):
    global current_state, value_turn_y
    current_state = rotate_around_y(current_state, slider_turn_Y.val - value_turn_y)
    value_turn_y = slider_turn_Y.val
    plot()


def update_turn_Z(val):
    global current_state, value_turn_z
    current_state = rotate_around_z(current_state, slider_turn_Z.val - value_turn_z)
    value_turn_z = slider_turn_Z.val
    plot()


fig.text(0.005, 0.42, "Turn (X,Y,Z)", fontsize=7, fontweight="bold")
rax_slider_turn_X = fig.add_axes([0.005, 0.395, 0.25, 0.02])
slider_turn_X = Slider(rax_slider_turn_X, " Turn X", -180, 180, valinit=0)
slider_turn_X.on_changed(update_turn_X)

rax_slider_turn_Y = fig.add_axes([0.005, 0.365, 0.25, 0.02])
slider_turn_Y = Slider(rax_slider_turn_Y, " Offset Y", -180, 180, valinit=0)
slider_turn_Y.on_changed(update_turn_Y)

rax_slider_turn_Z = fig.add_axes([0.005, 0.335, 0.25, 0.02])
slider_turn_Z = Slider(rax_slider_turn_Z, " Offset Z", -180, 180, valinit=0)
slider_turn_Z.on_changed(update_turn_Z)
# -----------------------------------------------------------------------------------

# Задание фигуры
fig.text(0.01, 0.19, "Center", fontsize=7, fontweight="bold")
rax_center_circle = fig.add_axes([0.005, 0.14, 0.1, 0.04])
text_box_center_circle = TextBox(rax_center_circle, "", initial="0,0,0")

fig.text(0.115, 0.19, "Radius", fontsize=7, fontweight="bold")
rax_radius_circle = fig.add_axes([0.11, 0.14, 0.1, 0.04])
text_box_radius_circle = TextBox(rax_radius_circle, "", initial="5")


def set_text_radius(label):
    global text_box_radius_circle
    if re.fullmatch(r'\d+', label):
        import ast
        val = ast.literal_eval(label)
        global current_sphere_radius
        current_sphere_radius = val
        get_new_state(current_figure, current_sphere_radius, current_sphere_center)
    else:
        text_box_radius_circle.set_val("5")
        current_sphere_radius = 5
        get_new_state(current_figure, current_sphere_radius, current_sphere_center)


def set_text_center(label):
    global text_box_center_circle
    if re.fullmatch(r'[-]?\d+,[-]?\d+,[-]?\d+', label):
        val = label.replace(",", " ")
        lst = val.split()
        val = np.array([lst[0], lst[1], lst[2]])
        global current_sphere_center
        current_sphere_center = val
        get_new_state(current_figure, current_sphere_radius, current_sphere_center)
    else:
        text_box_center_circle.set_val("0,0,0")
        current_sphere_center = np.array([0, 0, 0])
        get_new_state(current_figure, current_sphere_radius, current_sphere_center)


text_box_radius_circle.on_submit(set_text_radius)
text_box_center_circle.on_submit(set_text_center)


# -----------------------------------------------------------------------------------
def update_scale_X(val):
    global current_state, value_scale_x
    current_state = scale_x(current_state, slider_scale_X.val / value_scale_x)
    value_scale_x = slider_scale_X.val
    plot()


def update_scale_Y(val):
    global current_state, value_scale_y
    current_state = scale_y(current_state, slider_scale_Y.val / value_scale_y)
    value_scale_y = slider_scale_Y.val
    plot()


def update_scale_Z(val):
    global current_state, value_scale_z
    current_state = scale_z(current_state, slider_scale_Z.val / value_scale_z)
    value_scale_z = slider_scale_Z.val
    plot()


# Масштаб
fig.text(0.005, 0.54, "Scale (X,Y,Z)", fontsize=7, fontweight="bold")
rax_slider_scale_X = fig.add_axes([0.005, 0.515, 0.25, 0.02])
slider_scale_X = Slider(rax_slider_scale_X, "", 0.0, 10.0, valinit=1.0, valstep=0.2)
slider_scale_X.on_changed(update_scale_X)

rax_slider_scale_Y = fig.add_axes([0.005, 0.485, 0.25, 0.02])
slider_scale_Y = Slider(rax_slider_scale_Y, "", 0.0, 10.0, valinit=1.0, valstep=0.2)
slider_scale_Y.on_changed(update_scale_Y)

rax_slider_scale_Z = fig.add_axes([0.005, 0.455, 0.25, 0.02])
slider_scale_Z = Slider(rax_slider_scale_Z, "", 0.0, 10.0, valinit=1.0, valstep=0.2)
slider_scale_Z.on_changed(update_scale_Z)
# -----------------------------------------------------------------------------------

# Отражение относительно выбранной координатной плоскости
fig.text(0.005, 0.648, "Reflect-relative", fontsize=7, fontweight="bold")
rax_surface = plt.axes([0.005, 0.568, 0.07, 0.07])
labels_surface = ["X", "Y", "Z"]
check_surface = RadioButtons(rax_surface, labels_surface)
for circle in check_surface.labels:
    circle.set_fontsize(7)
    circle.set_fontweight("bold")


def update_surface(label):
    global current_state
    if label == "X":
        current_state = reflect_relative_x(current_state)
    if label == "Y":
        current_state = reflect_relative_y(current_state)
    if label == "Z":
        current_state = reflect_relative_z(current_state)
    plot()


check_surface.on_clicked(update_surface)


# -----------------------------------------------------------------------------------

# Масштаб относительно своего центра
def update_scale_center(val):
    global current_state
    global value_scale
    current_state = scale_center(current_state, slider_scale_center.val / value_scale)
    value_scale = slider_scale_center.val
    plot()


fig.text(0.005, 0.715, "Scaling relative to center", fontsize=7, fontweight="bold")
rax_slider_scale_center = fig.add_axes([0.005, 0.685, 0.25, 0.02])
slider_scale_center = Slider(rax_slider_scale_center, "", 0.0, 10.0, valinit=1.0, valstep=0.2)
slider_scale_center.on_changed(update_scale_center)
# -----------------------------------------------------------------------------------

# Кнопка сохранения

def save(event):
    """
    Создание Диалогового окна для задания имени сохраняемого файла
    :return: None
    """
    with h5py.File('../interface/Dodecahedron.h5', 'w') as hf:
        hf.create_dataset("test", data=current_state)


rax_button_save = fig.add_axes([0.35, 0.006, 0.05, 0.04])
button_save = Button(rax_button_save, "Save")
button_save.label.set_fontsize(7)
button_save.on_clicked(save)
# -----------------------------------------------------------------------------------

# Кнопка загрузки
rax_button_load = fig.add_axes([0.55, 0.006, 0.05, 0.04])
button_load = Button(rax_button_load, "Load")
button_load.label.set_fontsize(7)


# -----------------------------------------------------------------------------------


# Вращение[Slider] многогранника вокруг прямой проходящей через центр многогранника,
# параллельно выбранной координатной оси[CheckButtons]
def update_rotation_around_straight(val):
    global current_state, value_rotation_line
    global straight_for_rotation
    if straight_for_rotation == "X":
        current_state = rotate_around_str_centered_parallel_x(current_state, np.float64(
            float(slider_rotation.val - value_rotation_line)))
    if straight_for_rotation == "Y":
        current_state = rotate_around_str_centered_parallel_y(current_state, np.float64(
            float(slider_rotation.val - value_rotation_line)))
    if straight_for_rotation == "Z":
        current_state = rotate_around_str_centered_parallel_z(current_state, np.float64(
            float(slider_rotation.val - value_rotation_line)))
    value_rotation_line = slider_rotation.val
    plot()


def change_rotation_around_straight(label):
    global straight_for_rotation
    straight_for_rotation = label
    update_rotation_around_straight(label)


fig.text(0.005, 0.86, "Rotation through a straight line", fontsize=7, fontweight="bold")
rax_slider_rotation = fig.add_axes([0.005, 0.83, 0.25, 0.02])
slider_rotation = Slider(rax_slider_rotation, "", -180.0, 180.0, valinit=0.0, valstep=1.0)
slider_rotation.on_changed(update_rotation_around_straight)

rax_rotation = plt.axes([0.005, 0.75, 0.07, 0.07])
labels_rotation = ["X", "Y", "Z"]
check_rotation = RadioButtons(rax_rotation, labels_rotation)
for circle in check_rotation.labels:
    circle.set_fontsize(7)
    circle.set_fontweight("bold")
check_rotation.on_clicked(change_rotation_around_straight)


# -----------------------------------------------------------------------------------

# Поворот[Slider] вокруг произвольной (заданной координатами двух точек)[TextBox] прямой на заданный угол[Slider]
def update_rotation_point1(label):
    global current_state
    global point1, text_box_coordinate_point
    val1 = str(label)
    if re.fullmatch(r'[-]?\d+,[-]?\d+,[-]?\d+', val1):
        lst1 = val1.replace(",", " ").split()
        point1 = Point(np.float64(lst1[0]), np.float64(lst1[1]), np.float64(lst1[2]))
    else:
        text_box_coordinate_point.set_val("0,0,0")
        point1 = Point(0, 0, 0)


def update_rotation_point2(label):
    global current_state
    global point2, text_box_coordinate_point1
    val1 = str(label)
    if re.fullmatch(r'[-]?\d+,[-]?\d+,[-]?\d+', val1):
        print("True")
        lst1 = val1.replace(",", " ").split()
        point2 = Point(np.float64(lst1[0]), np.float64(lst1[1]), np.float64(lst1[2]))
    else:
        text_box_coordinate_point1.set_val("0,0,0")
        point2 = Point(0, 0, 0)


def update_rotation_around_straight1(val):
    global current_state, value_rotation_point, slider_rotation_point
    global point1, point2
    if point1.z() == 0 and point1.x() == 0 and point1.y() == 0 and point2.z() == 0 and point2.x() == 0 and point2.y() == 0:
        slider_rotation_point.reset()
    else:
        current_state = rotate_around_straight(current_state, point1, point2, np.float64(float(
            slider_rotation_point.val - value_rotation_point)))
        value_rotation_point = slider_rotation_point.val
        plot()


fig.text(0.005, 0.98, "Rotate around an arbitrary point straight line at a given angle", fontsize=7, fontweight="bold")
rax_slider_rotation_point = fig.add_axes([0.005, 0.90, 0.25, 0.015])
slider_rotation_point = Slider(rax_slider_rotation_point, "", -180.0, 180.0, valinit=0.0, valstep=1.0)
slider_rotation_point.on_changed(update_rotation_around_straight1)

rax_coordinate_point = fig.add_axes([0.005, 0.925, 0.1, 0.04])
text_box_coordinate_point = TextBox(rax_coordinate_point, "", initial="0,0,0")
text_box_coordinate_point.on_submit(update_rotation_point1)

rax_coordinate_point1 = fig.add_axes([0.125, 0.925, 0.1, 0.04])
text_box_coordinate_point1 = TextBox(rax_coordinate_point1, "", initial="0,0,0")
text_box_coordinate_point1.on_submit(update_rotation_point2)


# -----------------------------------------------------------------------------------



plt.show()

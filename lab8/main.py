import sys
import cv2
import time
from lab8.pyFiles.simpleRender.graphic_utils import *
from lab6.pyFiles.shapes.shapes import Tetrahedron, Hexahedron, Icosahedron, Dodecahedron
from lab6.pyFiles.affine_transform import rotate_around_x, rotate_around_z
from lab8.pyFiles.camera import Camera
from lab8.pyFiles.utils import (orthographic_projection, to4d, to3d,
                                default_orthographic_projection, default_perspective_projection)
from lab8.pyFiles.simpleRender.graphic_utils import line
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def two_var_func():
    X = np.arange(-200, 200, 3)
    Y = np.arange(-200, 200, 3)
    X, Y = np.meshgrid(X, Y)
    Z = np.sqrt(X ** 2 + Y ** 2)
    points = np.empty((X.reshape(-1).shape[0], 3))
    points[:, 0] = X.reshape(-1)
    points[:, 1] = Y.reshape(-1)
    points[:, 2] = Z.reshape(-1)
    return points


def draw(points: np.ndarray, scene: np.ndarray):
    tmp = points[:, 0].copy()
    points[:, 0] = points[:, 2]
    points[:, 2] = tmp
    points = points[np.argsort(points, axis=0)[:, 0]]
    # tmp = points[:, 0].copy()
    # points[:, 0] = points[:, 2]
    # points[:, 2] = tmp
    sorted = {}  # key: x, value: (max, min)
    step = 10
    current_gr_number = -1
    gr_count = np.unique(points[:, 0]).shape[0]
    while current_gr_number < gr_count:
        current_gr_number += 1
        if current_gr_number % step != 0:
            continue

        current_z = points[0, 0]
        ind_start_gr = 0
        try:
            ind_end_gr = list(points[:, 0] == current_z).index(False)
        except ValueError as e:
            print(e)
            ind_end_gr = points.shape[0] + 1
        # ind_end_gr = list(np.where(points != current_z)[:, 0]).index(False)
        current_yxs = points[ind_start_gr:ind_end_gr, 1:]

        tmp = current_yxs[:, 1].copy()
        current_yxs[:, 1] = current_yxs[:, 0]
        current_yxs[:, 0] = tmp
        current_yxs = current_yxs[np.argsort(current_yxs, axis=0)[:, 0]]

        for i in range(current_yxs.shape[0] - 1):
            current_xy = current_yxs[i]
            next_xy = current_yxs[i + 1]

            max_y, min_y = sorted.get(current_xy[0], (-sys.maxsize, sys.maxsize))

            if current_xy[1] > max_y:
                current_xy = np.hstack((current_xy, np.array([0])))
                next_xy = np.hstack((next_xy, np.array([0])))

                line(start=current_xy, end=next_xy, scene=scene, color=np.array([255, 189, 234]))
                # try:
                #     scene[current_xy[1], current_xy[0]] = np.array([255, 189, 234])
                # except Exception as e:
                #     pass

                sorted[current_xy[0]] = (current_xy[1], min_y)

            if current_xy[1] < min_y:
                current_xy = np.hstack((current_xy, np.array([0])))
                next_xy = np.hstack((next_xy, np.array([0])))

                line(start=current_xy, end=next_xy, scene=scene, color=np.array([255, 189, 234]))
                # try:
                #     scene[current_xy[1], current_xy[0]] = np.array([255, 189, 234])
                # except Exception as e:
                #     pass
                sorted[current_xy[0]] = (max_y, current_xy[1])

        points = points[ind_end_gr:, :]

    return scene
    # to_draw = []
    # for i in range(points.shape[0]):
    #     x = points[i, 0]
    #     y = points[i, 1]
    #     # z = points[i, 2]
    #     if x not in sorted:
    #         sorted.update({x: (y, None)})
    #         to_draw.append(points[i])
    #     else:
    #         max_val, min_val = sorted.get(x)
    #         if y > max_val:
    #             sorted.update({x: (y, min_val)})
    #             to_draw.append(points[i])
    #         elif y < max_val and min_val is None:
    #             sorted.update({x: (max_val, y)})
    #             to_draw.append(points[i])
    #         elif min_val is not None and y < min_val:
    #             sorted.update({x: (max_val, y)})
    #             to_draw.append(points[i])
    #
    # for pt in to_draw:
    #     try:
    #         x = pt[0]
    #         y = pt[1]
    #         scene[x, y] = [255, 255, 255]
    #     except Exception as e:
    #         pass
    #
    # return scene


def floating_horizon(scene, matrix_shift_xy, lookat_matrix) -> np.ndarray:
    points = two_var_func()
    ones: np.ndarray = np.ones(shape=(points.shape[0], 1), dtype=float)
    new_points = np.concatenate([points, ones], axis=-1)
    new_points = np.dot(new_points, lookat_matrix.T)
    new_points = np.dot(new_points, matrix_shift_xy.T)
    points3d = new_points[:, :3].astype(np.int)
    return draw(points3d, scene)



if __name__ == "__main__":
    floating_horizon()
    # scene, z_buffer = create_empty_scene(height=500, width=1000)
    # eye = np.array([500, 250, 1000])
    # target = np.array([250, 50, 0])
    # camera = Camera(eye, target, default_orthographic_projection())
    # lookat_matrix = camera.lookat()
    # # shape2 = Hexahedron(sphere_center=np.array([500, 250, -1]), sphere_radius=100.0)
    # shape2 = Hexahedron(sphere_center=np.array([500, 250, 0]), sphere_radius=110.0)
    # # new_vertexes = rotate_around_x(shape1.vertexes(), -45)
    # # new_vertexes = to3d(np.dot(to4d(new_vertexes), lookat_matrix.T))
    # # shape1.vertexes(new_vertexes)
    # # print(shape1)
    # new_vertexes = rotate_around_z(shape2.vertexes(), 15)
    # # new_vertexes = rotate_around_x(new_vertexes, -30)
    # # new_vertexes = to3d(np.dot(to4d(new_vertexes), lookat_matrix.T))
    # shape2.vertexes(new_vertexes)
    # texture = cv2.imread("./test_texture.jpg")
    # print(shape2)
    # # shape_render(shape1, scene, z_buffer, shape1.colors(), np.array([-1, -1, -1]))
    # start_time = time.time()
    # print(shape_render(shape2, scene, z_buffer, shape2.colors(), np.array([0, 1, 0]), None, texture))
    # print("Elapsed time:", time.time() - start_time)
    # cv2.imshow("Graphic scene", scene.astype(np.uint8))
    # cv2.waitKey(0)

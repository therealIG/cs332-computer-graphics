import numpy as np
from typing import NoReturn


class Camera:
    def __init__(self, eye_position: np.ndarray,
                 target_point: np.ndarray,
                 projection_mat: np.ndarray,
                 up_vector: np.ndarray = None):
        """
        It is used to construct a viewing matrix
        :param eye_position: a camera is located at the eye position; numpy.ndarray[float[3]]
        :param target_point: looking at (or rotating to) the target point; numpy.ndarray[float[3]]
        :param projection_mat:
        :param up_vector: up vector in global coordinates. Default value is [0, 1, 0]; numpy.ndarray[float[3]]
        """
        self._eye: np.ndarray = eye_position
        self._target: np.ndarray = target_point
        self._projection: np.ndarray = projection_mat
        self._world_up = up_vector
        if self._world_up is None:
            self._world_up = np.array([0, 1, 0], dtype=float)
        self._mt: np.ndarray = np.empty((1, 1))
        self._mt_update()
        self._mr: np.ndarray = np.empty((1, 1))
        self._mr_update()

    def eye(self) -> np.ndarray:
        return self._eye

    def target(self) ->np.ndarray:
        return self._target

    def _mt_update(self) -> NoReturn:
        self._mt = np.array([
            [1, 0, 0, -1 * self._eye[0]],
            [0, 1, 0, -1 * self._eye[1]],
            [0, 0, 1, -1 * self._eye[2]],
            [0, 0, 0, 1]
        ], dtype=float)

    def mt(self) -> np.ndarray:
        """
        The translation part of lookAt
        :return: numpy.ndarray[float[4x4]]
        """
        return self._mt

    @staticmethod
    def _normalize_vector(vector: np.ndarray) -> np.ndarray:
        """
        Calculate normalize vector based on input vector
        :param vector: np.ndarray[float[3]]
        :return: np.ndarray[float[3]]
        """
        vector_norm: float = np.linalg.norm(vector)
        return vector / vector_norm

    def _mr_update(self) -> NoReturn:
        forward_vector: np.ndarray = self._normalize_vector(self._eye - self._target)
        left_vector: np.ndarray = self._normalize_vector(np.cross(self._world_up, forward_vector))
        up_vector: np.ndarray = np.cross(forward_vector, left_vector)

        self._mr = np.array([
            [left_vector[0], left_vector[1], left_vector[2], 0],
            [up_vector[0], up_vector[1], up_vector[2], 0],
            [forward_vector[0], forward_vector[1], forward_vector[2], 0],
            [0, 0, 0, 1]
        ], dtype=float)

    def mr(self) -> np.ndarray:
        """
        The rotation part of lookAt
        :return: numpy.ndarray[float[4x4]]
        """
        return self._mr

    def eye_update(self, new_eye_position: np.ndarray) -> NoReturn:
        """
        Update position of camera
        :param new_eye_position: numpy.ndarray[float[3]]
        """
        self._eye = new_eye_position
        self._mt_update()
        self._mr_update()

    def target_update(self, new_target_position: np.ndarray) -> NoReturn:
        """
        Update position of target
        :param new_target_position: numpy.ndarray[float[3]]
        """
        self._target = new_target_position
        self._mr_update()

    def mview(self) -> np.ndarray:
        """
        Calculate matrix of view for camera
        :return: numpy.ndarray[float[4x4]]
        """
        return np.dot(self._mr, self._mt)

    def lookat(self) -> np.ndarray:
        """
        Calculate matrix for get projection image through camera
        :return: numpy.ndarray[float[4x4]]
        """
        mview: np.ndarray = self.mview()
        return np.dot(self._projection, mview)

    @staticmethod
    def _matrix_pretty_output(mat: np.ndarray) -> str:
        result = ""
        for i in range(0, mat.shape[0]):
            for j in range(0, mat.shape[1]):
                result += "{:>4} ".format(round(mat[i][j], ndigits=1))
            result += "\n"
        return result

    def __str__(self):
        result = "\nCamera object\n\n"
        result += "Eye position: {}\n".format(self._eye)
        result += "Target poition: {}\n\n".format(self._target)
        result += "Projection matrix: \n"
        result += self._matrix_pretty_output(self._projection)
        result += "\n"
        result += "Rotation matrix: \n"
        result += self._matrix_pretty_output(self._mr)
        result += "\n"
        result += "Translation matrix: \n"
        result += self._matrix_pretty_output(self._mt)
        result += "\n"

        return result

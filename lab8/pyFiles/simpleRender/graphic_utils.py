from typing import NoReturn, Union
from numpy import abs
from numba import jit
from lab6.pyFiles.shapes.geometric_primitives import *
import sys


def norm(v: np.ndarray) -> np.ndarray:
    v_norm = np.linalg.norm(v)
    return v / v_norm


def create_empty_scene(height: int = 600, width: int = 800):
    scene = np.zeros(shape=(height, width, 3), dtype=np.float)
    z_buffer = np.full(shape=(scene.shape[0], scene.shape[1]), fill_value=-1 * sys.maxsize)
    return scene, z_buffer


def line(start: np.ndarray, end: np.ndarray, scene: np.ndarray, color: np.ndarray) -> np.ndarray:
    x0, y0, z0 = start[1], start[0], start[2]
    x1, y1, z1 = end[1], end[0], end[2]
    steep: bool = False
    if abs(x0 - x1) < abs(y0 - y1):
        steep = True
        y0, x0 = x0, y0
        y1, x1 = x1, y1

    if x0 > x1:
        x1, x0 = x0, x1
        y1, y0 = y0, y1
        z1, z0 = z0, z1

    dx: int = x1 - x0
    dy: int = y1 - y0
    dz: int = z1 - z0

    derror2: int = abs(dy) * 2
    error2: int = 0
    y: int = y0

    for x in range(x0, x1 + 1):
        try:
            if steep:
                scene[int(y), int(x)] = color
            else:
                scene[int(x), int(y)] = color
        except Exception as e:
            pass

        error2 += derror2

        if error2 > dx:
            y += 1 if y1 > y0 else -1
            error2 -= dx * 2

    return scene


def triangle(first: np.ndarray, second: np.ndarray, third: np.ndarray,
             ity0: float, ity1: float, ity2: float,
             scene: np.ndarray, inner_color: np.ndarray,
             z_buffer: np.ndarray,
             texture: np.ndarray, uv_map_mat):
    vertexes_ity = [(first, ity0), (second, ity1), (third, ity2)]
    vertexes_ity = sorted(vertexes_ity, key=lambda v: v[0][1])
    vertexes = [vertex[0] for vertex in vertexes_ity]
    ity0, ity1, ity2 = tuple([ity[1] for ity in vertexes_ity])

    total_height: int = vertexes[2][1] - vertexes[0][1]
    texture_height: int = texture.shape[0]
    texture_width: int = texture.shape[1]
    backlog = ""
    for i in range(0, total_height):
        backlog_open = True
        second_half: bool = (i > vertexes[1][1] - vertexes[0][1]) or (vertexes[1][1] == vertexes[0][1])
        segment_height: int = vertexes[2][1] - vertexes[1][1] if second_half else vertexes[1][1] - vertexes[0][1]

        alpha: np.float64 = np.float64(i / total_height)
        s: int = vertexes[1][1] - vertexes[0][1] if second_half else 0
        beta: np.float64 = np.float64((i - s) / segment_height)

        a: np.ndarray = (vertexes[0] + (vertexes[2] - vertexes[0]) * alpha).astype(np.int)
        b: np.ndarray = vertexes[1] + (vertexes[2] - vertexes[1]) * beta if second_half else vertexes[0] + (
                vertexes[1] - vertexes[0]) * beta
        b = b.astype(np.int)

        ity_a: float = ity0 + (ity2 - ity0) * alpha
        ity_b: float = ity1 + (ity2 - ity1) * beta if second_half else ity0 + (ity1 - ity0) * beta

        if a[0] > b[0]:
            a, b = b, a

        for j in range(a[0], b[0] + 1):
            phi: np.float64 = np.float64(1) if b[0] == a[0] else np.float64(j - a[0]) / np.float64(b[0] - a[0])
            p: np.ndarray = (a.astype(float) + (b - a).astype(float) * phi).astype(int)
            ity_p: np.float64 = np.float64((ity_a + (ity_b - ity_a) * phi))

            try:
                if z_buffer[vertexes[0][1] + i, j] < p[2]:
                    z_buffer[vertexes[0][1] + i, j] = p[2]
                    y_cur = float(vertexes[0][1] + i)
                    x_cur = float(j)
                    uv_cords = np.clip(np.dot(np.array([x_cur, y_cur, 1]), uv_map_mat), [0, 0, 0],
                                       [texture_width - 1, texture_height - 1, 1.])
                    v = int(uv_cords[1])
                    u = int(uv_cords[0])
                    texel = texture[v, u]

                    scene[vertexes[0][1] + i, j] = np.clip(texel + texel * ity_p, 0, 255)
            except Exception as e:
                if backlog_open:
                    backlog += str(e) + "\n" + "Y = {0:>5}; X = {1:>5}".format(j, vertexes[0][1] + i) + "\n"
                    backlog_open = False
    return backlog


@jit(nopython=True, parallel=True)
def _parallel_inner_circle_phi_compute(a: np.ndarray, b: np.ndarray, current_y: int, ity_a: float,
                                       ity_b: float, texture: np.ndarray, scene: np.ndarray,
                                       z_buffer: np.ndarray, uv_map_mat: np.ndarray):
    for j in range(int(a[1]), int(b[1]) + 1):
        phi: float = float(j - a[1]) / float(b[1] - a[1])
        p: np.ndarray = (a + (b - a) * phi)
        ity_p: float = ity_a + (ity_b - ity_a) * phi

        if z_buffer[current_y, j] < int(p[2]):
            z_buffer[current_y, j] = int(p[2])
            uv_cords = np.dot(np.array([j, current_y, 1], dtype=np.float64), uv_map_mat)
            color = texture[int(uv_cords[1]), int(uv_cords[0])] + texture[int(uv_cords[1]), int(uv_cords[0])] * ity_p
            scene[current_y, j] = color


@jit(nopython=True, parallel=True)
def _parallel_inner_circle_const_one(a: np.ndarray, b: np.ndarray, current_y: int, ity_a: float,
                                     ity_b: float, texture: np.ndarray, scene: np.ndarray,
                                     z_buffer: np.ndarray, uv_map_mat: np.ndarray):
    for j in range(int(a[1]), int(b[1]) + 1):
        phi: float = 1.
        p: np.ndarray = (a + (b - a) * phi)
        ity_p: float = ity_a + (ity_b - ity_a) * phi

        if z_buffer[current_y, j] < int(p[2]):
            z_buffer[current_y, j] = int(p[2])
            uv_cords = np.dot(np.array([j, current_y, 1], dtype=np.float64), uv_map_mat)
            color = texture[int(uv_cords[1]), int(uv_cords[0])] + texture[int(uv_cords[1]), int(uv_cords[0])] * ity_p

            scene[current_y, j] = color


def triangle_speedup(vertexes: np.ndarray, scene: np.ndarray, z_buffer: np.ndarray, texture: np.ndarray,
                     uv_map_mat: np.ndarray, inner_color: np.ndarray) -> str:

    vertexes = vertexes[np.argsort(vertexes, axis=0)[:, 0]]
    total_height: int = int(vertexes[2, 0] - vertexes[0, 0])
    backlog = ""

    for i in range(0, total_height):
        backlog_open = True

        alpha: float = float(i) / total_height
        a: np.ndarray = (vertexes[0] + (vertexes[2] - vertexes[0]) * alpha)
        ity_a: float = vertexes[0, 3] + (vertexes[2, 3] - vertexes[0, 3]) * alpha

        if (i > vertexes[1, 0] - vertexes[0, 0]) or (vertexes[1, 0] == vertexes[0, 0]):
            segment_height: int = vertexes[2, 0] - vertexes[1, 0]
            beta: float = float(i - (vertexes[1, 0] - vertexes[0, 0])) / segment_height
            b: np.ndarray = (vertexes[1] + (vertexes[2] - vertexes[1]) * beta)
            ity_b: float = vertexes[1, 3] + (vertexes[2, 3] - vertexes[1, 3]) * beta

        else:
            segment_height: int = vertexes[1, 0] - vertexes[0, 0]
            beta: float = float(i) / segment_height
            b: np.ndarray = (vertexes[0] + (vertexes[1] - vertexes[0]) * beta)
            ity_b: float = vertexes[0, 3] + (vertexes[1, 3] - vertexes[0, 3]) * beta

        if a[1] > b[1]:
            a, b = b, a

        if np.abs(a[1] - b[1]) < 0.000001:
            phi_computing = _parallel_inner_circle_const_one
        else:
            phi_computing = _parallel_inner_circle_phi_compute

        current_y: int = int(vertexes[0, 0] + i)
        phi_computing(a, b, current_y, ity_a, ity_b, texture, scene, z_buffer, uv_map_mat)
        # for j in range(int(a[1]), int(b[1]) + 1):
        #
        #     phi: float = phi_computing(j, a, b)
        #     p: np.ndarray = (a + (b - a) * phi).astype(int)
        #     ity_p: float = ity_a + (ity_b - ity_a) * phi
        #
        #     try:
        #         if z_buffer[current_y, j] < p[2]:
        #             z_buffer[current_y, j] = p[2]
        #             uv_cords = np.dot(np.array([j, current_y, 1]), uv_map_mat).astype(int)
        #             scene[current_y, j] = np.clip(
        #                 texture[uv_cords[1], uv_cords[0]] + texture[uv_cords[1], uv_cords[0]] * ity_p, 0, 255)
        #     except Exception as e:
        #         if backlog_open:
        #             backlog += str(e) + "\n" + "Y = {0:>5}; X = {1:>5}".format(current_y, j) + "\n"
        #             backlog_open = False
    scene.clip(0, 255, scene)
    return backlog


def triangle_wo_buffer(vertexes: np.ndarray, scene: np.ndarray, inner_color: np.ndarray,
                       **kwargs):
    vertexes = vertexes[np.argsort(vertexes, axis=0)[:, 0]]
    total_height: int = int(vertexes[2, 0] - vertexes[0, 0])
    backlog = ""

    for i in range(0, total_height):
        backlog_open = True

        alpha: float = float(i) / total_height
        a: np.ndarray = (vertexes[0] + (vertexes[2] - vertexes[0]) * alpha)

        if (i > vertexes[1, 0] - vertexes[0, 0]) or (vertexes[1, 0] == vertexes[0, 0]):
            segment_height: int = vertexes[2, 0] - vertexes[1, 0]
            beta: float = float(i - (vertexes[1, 0] - vertexes[0, 0])) / segment_height
            b: np.ndarray = (vertexes[1] + (vertexes[2] - vertexes[1]) * beta)

        else:
            segment_height: int = vertexes[1, 0] - vertexes[0, 0]
            beta: float = float(i) / segment_height
            b: np.ndarray = (vertexes[0] + (vertexes[1] - vertexes[0]) * beta)

        if a[1] > b[1]:
            a, b = b, a

        current_y: int = int(vertexes[0, 0] + i)
        for j in range(int(a[1]), int(b[1]) + 1):
            try:
                scene[current_y, j] = inner_color
            except Exception as e:
                if backlog_open:
                    backlog += str(e) + "\n" + "Y = {0:>5}; X = {1:>5}".format(current_y, j) + "\n"
                    backlog_open = False
    return backlog


def triangle_wo_buffer(vertexes: np.ndarray, scene: np.ndarray, inner_color: np.ndarray,
                       **kwargs):
    vertexes = vertexes[np.argsort(vertexes, axis=0)[:, 0]]
    total_height: int = int(vertexes[2, 0] - vertexes[0, 0])
    backlog = ""

    for i in range(0, total_height):
        backlog_open = True

        alpha: float = float(i) / total_height
        a: np.ndarray = (vertexes[0] + (vertexes[2] - vertexes[0]) * alpha)

        if (i > vertexes[1, 0] - vertexes[0, 0]) or (vertexes[1, 0] == vertexes[0, 0]):
            segment_height: int = vertexes[2, 0] - vertexes[1, 0]
            beta: float = float(i - (vertexes[1, 0] - vertexes[0, 0])) / segment_height
            b: np.ndarray = (vertexes[1] + (vertexes[2] - vertexes[1]) * beta)

        else:
            segment_height: int = vertexes[1, 0] - vertexes[0, 0]
            beta: float = float(i) / segment_height
            b: np.ndarray = (vertexes[0] + (vertexes[1] - vertexes[0]) * beta)

        if a[1] > b[1]:
            a, b = b, a

        if np.abs(a[1] - b[1]) < 0.000001:
            phi_computing = _const_one
        else:
            phi_computing = _phi_compute

        current_y: int = int(vertexes[0, 0] + i)
        for j in range(int(a[1]), int(b[1]) + 1):
            phi: float = phi_computing(j, a, b)
            try:
                scene[current_y, j] = inner_color
            except Exception as e:
                if backlog_open:
                    backlog += str(e) + "\n" + "Y = {0:>5}; X = {1:>5}".format(current_y, j) + "\n"
                    backlog_open = False
    return backlog


def shape_render(shape: Polyhedron, scene: np.ndarray, z_buffer, colors: list, light_dir: np.ndarray, overview,
                 texture):
    vertexes, triangles_number = shape.triangulation()
    vs, vn = shape.get_normals()
    n = vertexes.shape[0]
    face_number: int = -1

    if z_buffer is None:
        triangle_method = triangle_wo_buffer
    else:
        overview = None
        triangle_method = triangle_speedup

    shape.clip_non_faces(overview)
    visibility = shape.visibility()
    current_uv_map_mat = np.zeros(shape=(3, 3), dtype=np.float64)
    uv_cords = shape.texture_coordinates(texture.shape[1], texture.shape[0])
    backlog = ""
    for i in range(0, n):
        first = vertexes[i, 0, :3]
        second = vertexes[i, 1, :3]
        third = vertexes[i, 2, :3]

        polygon_number = i % triangles_number
        if polygon_number == 0:
            face_number += 1

        if not visibility[face_number]:
            backlog += "Skip non-visibility face; face number: {}".format(face_number)
            continue

        det = np.linalg.det(np.array([[first[0], first[1], 1], [second[0], second[1], 1], [third[0], third[1], 1]]))
        backlog += "Det: {}".format(det)
        if np.abs(det) < 0.0001:
            continue

        current_uv_map_mat[0, 0] = np.linalg.det(np.array(
            [[uv_cords[polygon_number, 0, 0], first[1], 1], [uv_cords[polygon_number, 1, 0], second[1], 1],
             [uv_cords[polygon_number, 2, 0], third[1], 1]])) / det
        current_uv_map_mat[1, 0] = np.linalg.det(np.array(
            [[first[0], uv_cords[polygon_number, 0, 0], 1], [second[0], uv_cords[polygon_number, 1, 0], 1],
             [third[0], uv_cords[polygon_number, 2, 0], 1]])) / det
        current_uv_map_mat[2, 0] = np.linalg.det(np.array([[first[0], first[1], uv_cords[polygon_number, 0, 0]],
                                                           [second[0], second[1], uv_cords[polygon_number, 1, 0]],
                                                           [third[0], third[1], uv_cords[polygon_number, 2, 0]]])) / det

        current_uv_map_mat[0, 1] = np.linalg.det(np.array(
            [[uv_cords[polygon_number, 0, 1], first[1], 1], [uv_cords[polygon_number, 1, 1], second[1], 1],
             [uv_cords[polygon_number, 2, 1], third[1], 1]])) / det
        current_uv_map_mat[1, 1] = np.linalg.det(np.array(
            [[first[0], uv_cords[polygon_number, 0, 1], 1], [second[0], uv_cords[polygon_number, 1, 1], 1],
             [third[0], uv_cords[polygon_number, 2, 1], 1]])) / det
        current_uv_map_mat[2, 1] = np.linalg.det(np.array([[first[0], first[1], uv_cords[polygon_number, 0, 1]],
                                                           [second[0], second[1], uv_cords[polygon_number, 1, 1]],
                                                           [third[0], third[1], uv_cords[polygon_number, 2, 1]]])) / det

        current_uv_map_mat[2, 2] = 1

        # uv_cords_computing = np.dot(np.array([third[0], third[1], 1]), current_uv_map_mat)
        # uv_cords_ground_true = uv_cords[polygon_number, 2]
        # print("OK")
        inner_color: np.ndarray = colors[face_number]

        if light_dir is None:
            first_ity, second_ity, third_ity = 0., 0., 0.
        else:
            first_ity = np.dot(norm(np.array(vn[vs.index(tuple(first))])), norm(light_dir))
            second_ity = np.dot(norm(np.array(vn[vs.index(tuple(second))])), norm(light_dir))
            third_ity = np.dot(norm(np.array(vn[vs.index(tuple(third))])), norm(light_dir))

        first[0], first[1] = first[1], first[0]
        second[0], second[1] = second[1], second[0]
        third[0], third[1] = third[1], third[0]
        backlog += triangle_method(np.vstack((np.hstack((first, np.array([first_ity]))),
                                              np.hstack((second, np.array([second_ity]))),
                                              np.hstack((third, np.array([third_ity]))))),
                                   scene=scene, inner_color=inner_color , z_buffer=z_buffer,
                                   texture=texture, uv_map_mat=current_uv_map_mat)
        # backlog += triangle_method(first.astype(int), second.astype(int), third.astype(int), first_ity, second_ity, third_ity,
        #                            scene=scene, inner_color=inner_color , z_buffer=z_buffer,
        #                            texture=texture, uv_map_mat=current_uv_map_mat)
    return backlog

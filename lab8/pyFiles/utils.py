import numpy as np


def to3d(shape_as_array: np.ndarray) -> np.ndarray:
    """
    Conversion from shape[MxNx4] to shape[MxNx3] by 4s dimension clipping
    :param shape_as_array: numpy.ndarray[T[MxNx4]]
    :return: numpy.ndarray[T[MxNx3]]
    """
    shape_as_array_norm: np.ndarray = shape_as_array / shape_as_array[:, :, 3:]
    return shape_as_array_norm[:, :, :3]


def to4d(shape_as_array: np.ndarray) -> np.ndarray:
    """
    Conversion from shape[MxNx3] to shape[MxNx4] by extending to target size with ones
    :param shape_as_array: numpy.ndarray[T[MxNx3]]
    :return: numpy.ndarray[T[MxNx4]]
    """
    ones: np.ndarray = np.ones(shape=(shape_as_array.shape[0], shape_as_array.shape[1], 1), dtype=float)
    return np.concatenate([shape_as_array, ones], axis=-1)


def perspective_projection(width: float, height: float, near: float, far: float) -> np.ndarray:
    """
    Calculate matrix of Perspective projection
    :param width: width of top of Frustum; float
    :param height: height of top of Frustum; float
    :param near: distance to top of Frustum; float
    :param far: distance to bottom of Frustum; float
    :return: perspective projection matrix; numpy.ndarray[float[4x4]]
    """
    r: float = width / 2.
    t: float = height / 2.
    print("Alpha 1 right / near: {:>4}".format(np.round(np.rad2deg(np.arctan(r / near)), decimals=1)))
    print("Alpha 2 top   / near: {:>4}".format(np.round(np.rad2deg(np.arctan(t / near)), decimals=1)))
    t1: float = -1 * (far + near) / (far - near)
    t2: float = -2 * far * near / (far - near)
    return np.array([
        [near / r, 0, 0, 0],
        [0, near / t, 0, 0],
        [0, 0, t1, t2],
        [0, 0, -1, 0]
    ], dtype=float)


def orthographic_projection(width: float = 1000., height: float = 1000., near: float = 0.1, far: float = 1000.) -> np.ndarray:
    """
    Calculate matrix of orthographic projection
    :param width: width of top of rectangular; float
    :param height: height of top of rectangular; float
    :param near: distance to top of rectangular; float
    :param far: distance to bottom of rectangular; float
    :return: orthographic projection matrix; numpy.ndarray[float[4x4]]
    """
    r: float = width / 2.
    t: float = height / 2.
    t1: float = -2. / (far - near)
    t2: float = -1. * (far + near) / (far - near)
    return np.array([
        [1./r, 0, 0, 0],
        [0, 1./t, 0, 0],
        [0, 0, t1, t2],
        [0, 0, 0, 1]
    ], dtype=float)


def default_orthographic_projection() -> np.ndarray:
    return np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ], dtype=float)


def default_perspective_projection(z_distance: np.float) -> np.ndarray:
    return np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, -1/z_distance],
        [0 ,0, 0, 1]
    ], dtype=float)

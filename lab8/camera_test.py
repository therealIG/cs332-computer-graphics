import sys
sys.path.append("./../")
from lab8.pyFiles.camera import Camera
from lab6.pyFiles.shapes.shapes import Hexahedron, Tetrahedron
from lab8.pyFiles.utils import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import collections
from lab8.pyFiles.utils import clip_non_faces


def show_polyhedron(polyhedron):
    print(polyhedron)
    fig = plt.figure(figsize=(20, 20))
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    verticies = polyhedron.vertexes()[:, :, :2]
    print(verticies.shape)
    ax.add_collection(
        collections.PolyCollection(verticies, facecolors='cyan', linewidths=1, edgecolors='r'))
    X = polyhedron.vertexes()[:, :, 0]
    Y = polyhedron.vertexes()[:, :, 1]
    ax.set_xticks(np.arange(-10, 10, 1))
    ax.set_yticks(np.arange(-10, 10, 1))
    ax.set_xlabel('Y')
    ax.set_ylabel('Z')
    plt.show()


if __name__ == "__main__":
    print("Camera connected")

    eye_position = np.array([100, 100, 100], dtype=float)
    target_position = np.array([0, 0, 0], dtype=float)

    projection_width = 0.1
    projection_height = 0.1
    projection_far = 100.
    projection_near = 0.1
    projection_mat = perspective_projection(width=projection_width,
                                            height=projection_height,
                                            near=projection_near,
                                            far=projection_far)

    camera = Camera(eye_position, target_position, projection_mat)
    print(camera)

    sphere_center = np.array([0, 0, 0], dtype=float)
    radius = 10.0
    cube = Hexahedron(sphere_center, radius)
    overview = np.array([eye_position, target_position])

    lookat_matrix = camera.lookat()
    print("LookAt matrix shapes:", lookat_matrix.shape)
    vertexes = cube.vertexes()
    prepared_vertexes = to4d(cube.vertexes())
    print("Vertexes shape:", vertexes.shape)
    print("Prepared vertexes shape", prepared_vertexes.shape)
    vertexes_after_view_translation = np.dot(prepared_vertexes, lookat_matrix.T)
    vertexes_after_view_translation = to3d(vertexes_after_view_translation) * 10
    vertexes_without_back_faces = clip_non_faces(vertexes_after_view_translation,
                                                 np.concatenate([np.expand_dims(eye_position, 0),
                                                                 np.expand_dims(target_position, 0)],
                                                                axis=0))
    print("New vertexes shape:", vertexes_without_back_faces.shape)
    print()

    cube.vertexes(vertexes_without_back_faces)
    show_polyhedron(cube)

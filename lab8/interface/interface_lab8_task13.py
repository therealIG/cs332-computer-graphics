import sys

import cv2
import matplotlib
import datetime
import h5py

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from lab6.pyFiles.affine_transform import *
from lab6.pyFiles.shapes.shapes import *
from lab6.pyFiles.shapes.shapes import Hexahedron

from lab8.pyFiles.camera import *
from lab8.pyFiles.utils import *
from lab8.pyFiles.simpleRender.graphic_utils import shape_render, create_empty_scene

import re
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.collections import PolyCollection
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from lab6.pyFiles.projection import isometric_projection, orto_projection

matplotlib.use('Qt5Agg')


class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        QWidget.__init__(self)
        layout = QGridLayout()
        self.setLayout(layout)
        self.data = None
        self.setStyleSheet("background-color: gray;")
        layout.setSpacing(10)

        # Значения слайдеров
        self.value_shift_x = []
        self.value_shift_y = []
        self.value_shift_z = []

        self.value_scale = []
        self.value_scale_x = []
        self.value_scale_y = []
        self.value_scale_z = []

        self.value_turn_x = []
        self.value_turn_y = []
        self.value_turn_z = []

        self.flag_for_display = 2

        self.with_cnf = False
        self.with_z_buffer = False
        self.with_light = False
        self.coordinate_non_face = None

        """
        ----------------------------------------------------------------------
        --------- Размеры сцены; положение камеры ----------------------------
        ----------------------------------------------------------------------
        """

        self.scene_center_x = 950
        self.scene_center_y = 500
        self.scene_center_z = 0

        camera_x = self.scene_center_x
        camera_y = self.scene_center_y
        camera_z = self.scene_center_z + 500

        self.coordinate_camera = np.array([camera_x, camera_y, camera_z], dtype=float)
        self.coordinate_target = np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z], dtype=float)

        self.matrix_shift_xy = np.array([
            [1, 0, 0, self.scene_center_x],
            [0, 1, 0, self.scene_center_y],
            [0, 0, 1, self.scene_center_z],
            [0, 0, 0, 1]
        ])

        self.window_width = self.scene_center_x * 2
        self.window_height = self.scene_center_y * 2

        self.camera = Camera(self.coordinate_camera, self.coordinate_target, default_orthographic_projection())

        self.type_figure = None
        self.value_for_scale = [1 / 4, 1 / 3, 1 / 2, 1, 2, 3, 4, 5]

        self.type_projection = "orto_projection"
        self.set_figures = []  # список имен типов всех фигур на сцене
        self.set_dates = []  # список всех фигур на сцене
        self.default_light_dir = np.array([0, 1, 0])

        self.log_file = "logs"
        self.log_line_number = 1
        with open(self.log_file, "a") as log_file:
            log_file.write("\n------------------------------\n------------------------------\nNew session:\n{0}".format(
                datetime.datetime.now()))

        self._handling_type_info = "INFO"
        self._handling_type_warning = "WARNING"
        self._handling_type_error = "ERROR"
        self._open_handling_type = [self._handling_type_info,
                                    self._handling_type_warning]  # Выводит в консоль только эти категории, печатает в файл все
        self.logger(
            log_info="Scene size: Width = {0:>5}; Height = {1:>5}".format(self.window_width, self.window_height),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(camera_x, camera_y, camera_z),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Target position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(self.scene_center_x,
                                                                                  self.scene_center_y,
                                                                                  self.scene_center_z),
            handling_type=self._handling_type_info
        )
        # -------------------------------------------------------------------

        # --------------------------------------------------------------------------

        # Слайдеры смещения
        self.label_offset_x = QLabel(" Shift X")
        font = self.label_offset_x.font()
        font.setPointSize(10)
        self.label_offset_x.setFont(font)
        layout.addWidget(self.label_offset_x, 0, 0)
        self.slider_shift_x = QSlider(Qt.Horizontal)
        self.slider_shift_x.setFixedSize(300, 20)
        self.slider_shift_x.setRange(-200, 200)
        self.slider_shift_x.setValue(0)
        self.slider_shift_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_x.setTickInterval(20)
        self.slider_shift_x.setSingleStep(1)
        layout.addWidget(self.slider_shift_x, 1, 0)
        self.label_shift_x_value = QLabel(str(self.slider_shift_x.value()))
        layout.addWidget(self.label_shift_x_value, 1, 1)
        self.slider_shift_x.valueChanged.connect(self.update_slider_shift_x)
        self.label_shift_x_value.setFixedSize(27, 10)

        self.label_offset_y = QLabel(" Shift Y")
        self.label_offset_y.setFont(font)
        layout.addWidget(self.label_offset_y, 2, 0)
        self.slider_shift_y = QSlider(Qt.Horizontal)
        self.slider_shift_y.setFixedSize(300, 20)
        self.slider_shift_y.setRange(-200, 200)
        self.slider_shift_y.setValue(0)
        self.slider_shift_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_y.setTickInterval(20)
        self.slider_shift_y.setSingleStep(1)
        layout.addWidget(self.slider_shift_y, 3, 0)
        self.label_shift_y_value = QLabel(str(self.slider_shift_y.value()))
        layout.addWidget(self.label_shift_y_value, 3, 1)
        self.slider_shift_y.valueChanged.connect(self.update_slider_shift_y)
        self.label_shift_y_value.setFixedSize(27, 10)

        self.label_offset_z = QLabel(" Shift Z")
        self.label_offset_z.setFont(font)
        layout.addWidget(self.label_offset_z, 4, 0)
        self.slider_shift_z = QSlider(Qt.Horizontal)
        self.slider_shift_z.setFixedSize(300, 20)
        self.slider_shift_z.setRange(-200, 200)
        self.slider_shift_z.setValue(0)
        self.slider_shift_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_z.setTickInterval(20)
        self.slider_shift_z.setSingleStep(1)
        layout.addWidget(self.slider_shift_z, 5, 0)
        self.label_shift_z_value = QLabel(str(self.slider_shift_z.value()))
        layout.addWidget(self.label_shift_z_value, 5, 1)
        self.slider_shift_z.valueChanged.connect(self.update_slider_shift_z)
        self.label_shift_z_value.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Слайдеры поворота
        self.label_turn_x = QLabel(" Turn X")
        self.label_turn_x.setFont(font)
        layout.addWidget(self.label_turn_x, 6, 0)
        self.slider_turn_x = QSlider(Qt.Horizontal)
        self.slider_turn_x.setFixedSize(300, 20)
        self.slider_turn_x.setRange(-180, 180)
        self.slider_turn_x.setValue(0)
        self.slider_turn_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_x.setTickInterval(10)
        self.slider_turn_x.setSingleStep(1)
        layout.addWidget(self.slider_turn_x, 7, 0)
        self.label_turn_x_value = QLabel(str(self.slider_turn_x.value()))
        layout.addWidget(self.label_turn_x_value, 7, 1)
        self.slider_turn_x.valueChanged.connect(self.update_slider_turn_x)
        self.label_turn_x_value.setFixedSize(27, 10)

        self.label_turn_y = QLabel(" Turn Y")
        self.label_turn_y.setFont(font)
        layout.addWidget(self.label_turn_y, 8, 0)
        self.slider_turn_y = QSlider(Qt.Horizontal)
        self.slider_turn_y.setFixedSize(300, 20)
        self.slider_turn_y.setRange(-180, 180)
        self.slider_turn_y.setValue(0)
        self.slider_turn_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_y.setTickInterval(10)
        self.slider_turn_y.setSingleStep(1)
        layout.addWidget(self.slider_turn_y, 9, 0)
        self.label_turn_y_value = QLabel(str(self.slider_turn_y.value()))
        layout.addWidget(self.label_turn_y_value, 9, 1)
        self.slider_turn_y.valueChanged.connect(self.update_slider_turn_y)
        self.label_turn_y_value.setFixedSize(27, 10)

        self.label_turn_z = QLabel(" Turn Z")
        self.label_turn_z.setFont(font)
        layout.addWidget(self.label_turn_z, 10, 0)
        self.slider_turn_z = QSlider(Qt.Horizontal)
        self.slider_turn_z.setFixedSize(300, 20)
        self.slider_turn_z.setRange(-180, 180)
        self.slider_turn_z.setValue(0)
        self.slider_turn_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_z.setTickInterval(10)
        self.slider_turn_z.setSingleStep(1)
        layout.addWidget(self.slider_turn_z, 11, 0)
        self.label_turn_z_value = QLabel(str(self.slider_turn_z.value()))
        layout.addWidget(self.label_turn_z_value, 11, 1)
        self.slider_turn_z.valueChanged.connect(self.update_slider_turn_z)
        self.label_turn_z_value.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Слайдеры масшатибрования
        self.label_scale = QLabel(" Scale ")
        self.label_scale.setFont(font)
        layout.addWidget(self.label_scale, 12, 0)
        self.slider_scale = QSlider(Qt.Horizontal)
        self.slider_scale.setFixedSize(300, 20)
        self.slider_scale.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale.setRange(0, 7)
        self.slider_scale.setValue(3)
        self.slider_scale.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale.setTickInterval(1)
        self.slider_scale.setSingleStep(1)
        layout.addWidget(self.slider_scale, 13, 0)
        self.label_scale_value = QLabel(str(self.value_for_scale[self.slider_scale.value()]))
        layout.addWidget(self.label_scale_value, 13, 1)
        self.slider_scale.valueChanged.connect(self.update_slider_scale)
        self.label_scale_value.setFixedSize(27, 10)

        self.label_scale_x = QLabel(" Scale X")
        self.label_scale_x.setFont(font)
        layout.addWidget(self.label_scale_x, 14, 0)
        self.slider_scale_x = QSlider(Qt.Horizontal)
        self.slider_scale_x.setFixedSize(300, 20)
        self.slider_scale_x.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_x.setRange(0, 7)
        self.slider_scale_x.setValue(3)
        self.slider_scale_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_x.setTickInterval(1)
        self.slider_scale_x.setSingleStep(1)
        layout.addWidget(self.slider_scale_x, 15, 0)
        self.label_scale_value_x = QLabel(str(self.value_for_scale[self.slider_scale_x.value()]))
        layout.addWidget(self.label_scale_value_x, 15, 1)
        self.slider_scale_x.valueChanged.connect(self.update_slider_scale_x)
        self.label_scale_value_x.setFixedSize(27, 10)

        self.label_scale_y = QLabel(" Scale Y")
        self.label_scale_y.setFont(font)
        layout.addWidget(self.label_scale_y, 16, 0)
        self.slider_scale_y = QSlider(Qt.Horizontal)
        self.slider_scale_y.setFixedSize(300, 20)
        self.slider_scale_y.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_y.setRange(0, 7)
        self.slider_scale_y.setValue(3)
        self.slider_scale_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_y.setTickInterval(1)
        self.slider_scale_y.setSingleStep(1)
        layout.addWidget(self.slider_scale_y, 17, 0)
        self.label_scale_value_y = QLabel(str(self.value_for_scale[self.slider_scale_y.value()]))
        layout.addWidget(self.label_scale_value_y, 17, 1)
        self.slider_scale_y.valueChanged.connect(self.update_slider_scale_y)
        self.label_scale_value_y.setFixedSize(27, 10)

        self.label_scale_z = QLabel(" Scale Z")
        self.label_scale_z.setFont(font)
        layout.addWidget(self.label_scale_z, 18, 0)
        self.slider_scale_z = QSlider(Qt.Horizontal)
        self.slider_scale_z.setFixedSize(300, 20)
        self.slider_scale_z.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_z.setRange(0, 7)
        self.slider_scale_z.setValue(3)
        self.slider_scale_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_z.setTickInterval(1)
        self.slider_scale_z.setSingleStep(1)
        layout.addWidget(self.slider_scale_z, 19, 0)
        self.label_scale_value_z = QLabel(str(self.value_for_scale[self.slider_scale_z.value()]))
        layout.addWidget(self.label_scale_value_z, 19, 1)
        self.slider_scale_z.valueChanged.connect(self.update_slider_scale_z)
        self.label_scale_value_z.setFixedSize(27, 10)
        # -------------------------------------------------------------------------

        # Canvas
        self.figure = QGraphicsView()
        self.figure.setMinimumSize(900, 900)
        layout.addWidget(self.figure, 0, 2, 34, 9)
        # --------------------------------------------------------------------------
        self.type_projection = None
        self.corner_first = 0
        self.corner_second = 0
        self.axis = 0

        # Определение слайдеров положения камеры
        self.label_camera_position = QLabel(" Camera Position X,Y,Z ")
        self.label_camera_position.setFont(font)
        layout.addWidget(self.label_camera_position, 20, 0)
        # X
        self.slider_position_x = QSlider(Qt.Horizontal)
        self.slider_position_x.setFixedSize(300, 20)
        self.slider_position_x.setRange(-1000, 2000)
        self.slider_position_x.setValue(camera_x)
        self.slider_position_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_x.setTickInterval(100)
        self.slider_position_x.setSingleStep(1)
        layout.addWidget(self.slider_position_x, 21, 0)
        self.label_position_value_x = QLabel(str(self.slider_position_x.value()))
        layout.addWidget(self.label_position_value_x, 21, 1)
        self.slider_position_x.valueChanged.connect(self.update_slider_position_x)
        self.label_position_value_x.setFixedSize(27, 10)

        # Y
        self.slider_position_y = QSlider(Qt.Horizontal)
        self.slider_position_y.setFixedSize(300, 20)
        self.slider_position_y.setRange(-1000, 2000)
        self.slider_position_y.setValue(camera_y)
        self.slider_position_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_y.setTickInterval(100)
        self.slider_position_y.setSingleStep(1)
        layout.addWidget(self.slider_position_y, 22, 0)
        self.label_position_value_y = QLabel(str(self.slider_position_y.value()))
        layout.addWidget(self.label_position_value_y, 22, 1)
        self.slider_position_y.valueChanged.connect(self.update_slider_position_y)
        self.label_position_value_y.setFixedSize(27, 10)

        # Z
        self.slider_position_z = QSlider(Qt.Horizontal)
        self.slider_position_z.setFixedSize(300, 20)
        self.slider_position_z.setRange(-1000, 2000)
        self.slider_position_z.setValue(camera_z)
        self.slider_position_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_z.setTickInterval(100)
        self.slider_position_z.setSingleStep(1)
        layout.addWidget(self.slider_position_z, 23, 0)
        self.label_position_value_z = QLabel(str(self.slider_position_z.value()))
        layout.addWidget(self.label_position_value_z, 23, 1)
        self.slider_position_z.valueChanged.connect(self.update_slider_position_z)
        self.label_position_value_z.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Определение слайдеров положения цели
        self.label_camera_target = QLabel(" Camera Target X,Y,Z ")
        self.label_camera_target.setFont(font)
        layout.addWidget(self.label_camera_target, 24, 0)
        # X
        self.slider_position_target_x = QSlider(Qt.Horizontal)
        self.slider_position_target_x.setFixedSize(300, 20)
        self.slider_position_target_x.setRange(-1000, 2000)
        self.slider_position_target_x.setValue(self.scene_center_x)
        self.slider_position_target_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_x.setTickInterval(100)
        self.slider_position_target_x.setSingleStep(1)
        layout.addWidget(self.slider_position_target_x, 25, 0)
        self.label_position_target_value_x = QLabel(str(self.slider_position_target_x.value()))
        layout.addWidget(self.label_position_target_value_x, 25, 1)
        self.slider_position_target_x.valueChanged.connect(self.update_slider_position_target_x)
        self.label_position_target_value_x.setFixedSize(27, 10)

        # Y
        self.slider_position_target_y = QSlider(Qt.Horizontal)
        self.slider_position_target_y.setFixedSize(300, 20)
        self.slider_position_target_y.setRange(-1000, 2000)
        self.slider_position_target_y.setValue(self.scene_center_y)
        self.slider_position_target_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_y.setTickInterval(100)
        self.slider_position_target_y.setSingleStep(1)
        layout.addWidget(self.slider_position_target_y, 26, 0)
        self.label_position_target_value_y = QLabel(str(self.slider_position_target_y.value()))
        layout.addWidget(self.label_position_target_value_y, 26, 1)
        self.slider_position_target_y.valueChanged.connect(self.update_slider_position_target_y)
        self.label_position_target_value_y.setFixedSize(27, 10)

        # Z
        self.slider_position_target_z = QSlider(Qt.Horizontal)
        self.slider_position_target_z.setFixedSize(300, 20)
        self.slider_position_target_z.setRange(-1000, 2000)
        self.slider_position_target_z.setValue(self.scene_center_z)
        self.slider_position_target_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_z.setTickInterval(100)
        self.slider_position_target_z.setSingleStep(10)
        layout.addWidget(self.slider_position_target_z, 27, 0)
        self.label_position_target_value_z = QLabel(str(self.slider_position_target_z.value()))
        layout.addWidget(self.label_position_target_value_z, 27, 1)
        self.slider_position_target_z.valueChanged.connect(self.update_slider_position_target_z)
        self.label_position_target_value_z.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # CheckBox отсечения нелицевых граней
        self.label_checkbox = QLabel(" Clipping non-face faces ")
        self.label_checkbox.setFont(font)
        layout.addWidget(self.label_checkbox, 28, 0)

        self.check_box_cnf = QCheckBox("With CNF")
        self.check_box_cnf.setFont(font)
        self.check_box_cnf.clicked.connect(self.update_check_box)
        layout.addWidget(self.check_box_cnf, 28, 1)

        self.check_box_z = QCheckBox("With Z-Buffer")
        self.check_box_z.setFont(font)
        self.check_box_z.clicked.connect(self.update_check_box)
        layout.addWidget(self.check_box_z, 29, 1)

        self.check_box_light_on = QCheckBox("Light ON")
        self.check_box_light_on.setFont(font)
        self.check_box_light_on.clicked.connect(self.update_light)
        layout.addWidget(self.check_box_light_on, 30, 1)

        # --------------------------------------------------------------------------

        # Лист всех фигур + кнопки добавления и удаления фигур
        self.label_list_figures = QLabel(" List figures on Scene ")
        self.label_list_figures.setFont(font)
        layout.addWidget(self.label_list_figures, 31, 0)

        self.list_figures = QListWidget()
        self.list_figures.setFont(font)
        self.list_figures.setMaximumWidth(200)
        layout.addWidget(self.list_figures, 32, 0)

        self.button_add_figure = QPushButton(" Add figure")
        self.button_add_figure.setFont(font)
        self.button_add_figure.setFixedSize(300, 40)
        self.button_add_figure.clicked.connect(self.update_add_figure)
        layout.addWidget(self.button_add_figure, 29, 0)

        self.button_del_figure = QPushButton(" Delete figure")
        self.button_del_figure.setFont(font)
        self.button_del_figure.setFixedSize(300, 40)
        self.button_del_figure.clicked.connect(self.update_del_figure)
        layout.addWidget(self.button_del_figure, 30, 0)
        # --------------------------------------------------------------------------

    def plot(self):
        """
        Отрисовка изображения в 2D
        :return: None
        """
        scene, z_buffer = create_empty_scene(height=self.window_height, width=self.window_width)
        if not self.with_z_buffer:
            z_buffer = None
        current_light_dir = self.default_light_dir if self.with_light else None
        lookat_matrix = self.camera.lookat()

        self.logger(
            log_info="Render start. Shapes count: {}".format(len(self.set_figures)),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera state:\n{0}".format(self.camera),
            handling_type=self._handling_type_info
        )
        for i in range(len(self.set_figures)):
            data = self.set_dates[i]
            shape_type = self.set_figures[i]
            shape_for_render = self._shape_choosing(shape_type)(np.array([250, 250, 0]), 50.0)

            prepared_vertexes = to4d(data.vertexes())
            vertexes_after_view_translation = np.dot(prepared_vertexes, lookat_matrix.T)
            vertexes_after_view_translation = np.dot(vertexes_after_view_translation,
                                                     np.transpose(self.matrix_shift_xy))
            vertexes_after_view_translation = to3d(vertexes_after_view_translation)
            shape_for_render.vertexes(vertexes_after_view_translation)

            self.coordinate_non_face = self.camera.target() - self.camera.eye()
            self.coordinate_non_face = np.expand_dims(self.coordinate_non_face, axis=0)
            self.coordinate_non_face = np.expand_dims(self.coordinate_non_face, axis=0)
            self.coordinate_non_face = to4d(self.coordinate_non_face)
            self.coordinate_non_face = np.dot(self.coordinate_non_face, lookat_matrix.T)
            self.coordinate_non_face = np.dot(self.coordinate_non_face, np.transpose(self.matrix_shift_xy))[0, 0, :3]
            texture = cv2.resize(cv2.imread("./tt.jpg"), (self.window_height, self.window_width))
            backlog = shape_render(shape_for_render, scene, z_buffer, shape_for_render.colors(), current_light_dir,
                                   self.coordinate_non_face, texture)

            handling_type = self._handling_type_info
            if backlog != "":
                handling_type = self._handling_type_warning
                self.logger(
                    log_info="Backlog: {}".format(backlog),
                    handling_type=self._handling_type_error
                )
            self.logger(
                log_info="Shape in global coordinates:\n{0}Shape prepared for render:\n{1}".format(data,
                                                                                                   shape_for_render),
                handling_type=handling_type
            )

        graphic_scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(cv2.cvtColor(scene.astype(np.uint8), cv2.COLOR_BGR2RGB), self.window_width, self.window_height,
                   QImage.Format_RGB888))
        graphic_scene.addPixmap(pixmap1)
        self.figure.setScene(graphic_scene)

        self.logger(
            log_info="Render end.",
            handling_type=self._handling_type_info
        )
        # ---------------------------------------------------

    def logger(self, log_info: str, handling_type: str = "UNKNOWN"):
        # ------------------------------------------------------------------------
        # ------------------------- LOGGER ---------------------------------------

        for line in log_info.split("\n"):
            current_line = "[{0}]::[{1}]:: {2}".format(self.log_line_number, handling_type, line)

            if handling_type in self._open_handling_type:
                print(current_line)

            with open(self.log_file, "a") as log_file:
                log_file.write(current_line + "\n")
            self.log_line_number += 1

        # ------------------------------------------------------------------------

    def update_slider_turn_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_turn_x_value.setText(str(self.slider_turn_x.value()))
                current_data = rotate_around_x(current_data,
                                               self.value_turn_x[current_index] - self.slider_turn_x.value())
                self.value_turn_x[current_index] = self.slider_turn_x.value()
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider turn X. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                    current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider turn X. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_turn_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_turn_y_value.setText(str(self.slider_turn_y.value()))
                current_data = rotate_around_y(current_data,
                                               self.value_turn_y[current_index] - self.slider_turn_y.value())
                self.value_turn_y[current_index] = self.slider_turn_y.value()
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider turn Y. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                    current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider turn Y. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_turn_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_turn_z_value.setText(str(self.slider_turn_z.value()))
                current_data = rotate_around_z(current_data,
                                               self.value_turn_z[current_index] - self.slider_turn_z.value())
                self.value_turn_z[current_index] = self.slider_turn_z.value()
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider turn Z. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                    current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider turn Z. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_shift_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_shift_x_value.setText(str(self.slider_shift_x.value()))
                current_data = shift_x(current_data, self.slider_shift_x.value() - self.value_shift_x[current_index])
                self.value_shift_x[current_index] = self.slider_shift_x.value()
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider shift X. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                     current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider shift X. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_shift_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_shift_y_value.setText(str(self.slider_shift_y.value()))
                current_data = shift_y(current_data, self.slider_shift_y.value() - self.value_shift_y[current_index])
                self.value_shift_y[current_index] = self.slider_shift_y.value()
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider shift Y. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                     current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider shift Y. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_shift_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_shift_z_value.setText(str(self.slider_shift_z.value()))
                current_data = shift_z(current_data, self.slider_shift_z.value() - self.value_shift_z[current_index])
                self.value_shift_z[current_index] = self.slider_shift_z.value()
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider shift Z. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                     current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider shift Z. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_scale(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_scale_value.setText(str(self.value_for_scale[self.slider_scale.value()]))
                self.label_scale_value_x.setText(str(self.value_for_scale[self.slider_scale.value()]))
                self.label_scale_value_y.setText(str(self.value_for_scale[self.slider_scale.value()]))
                self.label_scale_value_z.setText(str(self.value_for_scale[self.slider_scale.value()]))
                current_data = scale_center(current_data,
                                            self.value_for_scale[self.slider_scale.value()] / self.value_scale[
                                                current_index])
                self.value_scale[current_index] = self.value_for_scale[self.slider_scale.value()]
                self.value_scale_x[current_index] = self.value_for_scale[self.slider_scale.value()]
                self.value_scale_y[current_index] = self.value_for_scale[self.slider_scale.value()]
                self.value_scale_z[current_index] = self.value_for_scale[self.slider_scale.value()]
                self.slider_scale_x.setValue(self.slider_scale.value())
                self.slider_scale_y.setValue(self.slider_scale.value())
                self.slider_scale_z.setValue(self.slider_scale.value())
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider scale. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                   current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider scale. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_scale_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_scale_value_x.setText(str(self.value_for_scale[self.slider_scale_x.value()]))
                current_data = scale_x(current_data,
                                       self.value_for_scale[self.slider_scale_x.value()] / self.value_scale_x[
                                           current_index])
                self.value_scale_x[current_index] = self.value_for_scale[self.slider_scale_x.value()]
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider scale X. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                     current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider scale X. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_scale_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_scale_value_y.setText(str(self.value_for_scale[self.slider_scale_y.value()]))
                current_data = scale_y(current_data,
                                       self.value_for_scale[self.slider_scale_y.value()] / self.value_scale_y[
                                           current_index])
                self.value_scale_y[current_index] = self.value_for_scale[self.slider_scale_y.value()]
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider scale Y. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                     current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider scale Y. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_scale_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            current_data = self.set_dates[current_index].vertexes()
            prev_state = self.set_dates[current_index]
            if current_data is not None:
                self.label_scale_value_z.setText(str(self.value_for_scale[self.slider_scale_z.value()]))
                current_data = scale_z(current_data,
                                       self.value_for_scale[self.slider_scale_z.value()] / self.value_scale_z[
                                           current_index])
                self.value_scale_z[current_index] = self.value_for_scale[self.slider_scale_z.value()]
                self.set_dates[current_index].vertexes(current_data)
                self.plot()
                current_state = self.set_dates[current_index]
                self.logger(
                    log_info="Update slider scale Z. Current state:\n{0}\nCurrent index: {1}".format(str(current_state),
                                                                                                     current_index),
                    handling_type=self._handling_type_info
                )
            else:
                self.logger(
                    log_info="Update slider scale Z. \nCurrent index: {0}\n Shape not found".format(current_index),
                    handling_type=self._handling_type_warning
                )

    def update_slider_position_x(self):
        current_index = self.list_figures.currentRow()
        self.label_position_value_x.setText(str(self.slider_position_x.value()))
        previous_coordinate = self.coordinate_camera
        self.coordinate_camera[0] = self.slider_position_x.value()
        self.logger(
            log_info="Update slider position X, update camera position.\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_camera[0], self.coordinate_camera[1], self.coordinate_camera[2]),
            handling_type=self._handling_type_info
        )
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_y(self):
        current_index = self.list_figures.currentRow()
        self.label_position_value_y.setText(str(self.slider_position_y.value()))
        previous_coordinate = self.coordinate_camera
        self.coordinate_camera[1] = self.slider_position_y.value()
        self.logger(
            log_info="Update slider position Y, update camera position.\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_camera[0], self.coordinate_camera[1], self.coordinate_camera[2]),
            handling_type=self._handling_type_info
        )
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_z(self):
        current_index = self.list_figures.currentRow()
        self.label_position_value_z.setText(str(self.slider_position_z.value()))
        previous_coordinate = self.coordinate_camera
        self.coordinate_camera[2] = self.slider_position_z.value()
        self.logger(
            log_info="Update slider position Z, update camera position.\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_camera[0], self.coordinate_camera[1], self.coordinate_camera[2]),
            handling_type=self._handling_type_info
        )
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_target_x(self):
        current_index = self.list_figures.currentRow()
        self.label_position_target_value_x.setText(str(self.slider_position_target_x.value()))
        previous_coordinate = self.coordinate_target
        self.coordinate_target[0] = self.slider_position_target_x.value()
        self.logger(
            log_info="Update slider position target X\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_target[0], self.coordinate_target[1], self.coordinate_target[2]),
            handling_type=self._handling_type_info
        )
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_slider_position_target_y(self):
        current_index = self.list_figures.currentRow()
        self.label_position_target_value_y.setText(str(self.slider_position_target_y.value()))
        previous_coordinate = self.coordinate_target
        self.coordinate_target[1] = self.slider_position_target_x.value()
        self.logger(
            log_info="Update slider position target Y\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_target[0], self.coordinate_target[1], self.coordinate_target[2]),
            handling_type=self._handling_type_info
        )
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_slider_position_target_z(self):
        current_index = self.list_figures.currentRow()
        self.label_position_target_value_z.setText(str(self.slider_position_target_z.value()))
        previous_coordinate = self.coordinate_target
        self.coordinate_target[2] = self.slider_position_target_x.value()
        self.logger(
            log_info="Update slider position target Z\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_target[0], self.coordinate_target[1], self.coordinate_target[2]),
            handling_type=self._handling_type_info
        )
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def _shape_choosing(self, shape_type):
        if shape_type == "Dodecahedron":
            return Dodecahedron
        if shape_type == "Hexahedron":
            return Hexahedron
        if shape_type == "Icocahedron":
            return Icosahedron
        if shape_type == "Octahedron":
            return Octahedron
        if shape_type == "Tetrahedron":
            return Tetrahedron

    def update_add_figure(self):
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', '../data/task3/')[0]
            with h5py.File(fname, 'r') as hf:
                fname = fname.split("/")[-1].split(".")[0]
                if fname == "Dodecahedron":
                    self.type_figure = "Dodecahedron"
                    cube = Dodecahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]),
                                        100.0)
                if fname == "Hexahedron":
                    self.type_figure = "Hexahedron"
                    cube = Hexahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                if fname == "Icocahedron":
                    self.type_figure = "Icocahedron"
                    cube = Icosahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                if fname == "Octahedron":
                    self.type_figure = "Octahedron"
                    cube = Octahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                if fname == "Tetrahedron":
                    self.type_figure = "Tetrahedron"
                    cube = Tetrahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                self.logger(
                    log_info="Update add figure. Adding new shape to scene.\nShape:{0}".format(cube),
                    handling_type=self._handling_type_info
                )
                self.list_figures.addItem(fname)
                self.set_figures.append(fname)
                self.set_dates.append(cube)

                self.value_turn_x.append(0)
                self.value_turn_y.append(0)
                self.value_turn_z.append(0)

                self.value_shift_x.append(0)
                self.value_shift_y.append(0)
                self.value_shift_z.append(0)

                self.value_scale.append(1)
                self.value_scale_x.append(1)
                self.value_scale_y.append(1)
                self.value_scale_z.append(1)

                self.slider_shift_x.setValue(0)
                self.slider_shift_y.setValue(0)
                self.slider_shift_z.setValue(0)

                self.slider_turn_x.setValue(0)
                self.slider_turn_y.setValue(0)
                self.slider_turn_z.setValue(0)

                self.slider_scale.setValue(3)
                self.slider_scale_x.setValue(3)
                self.slider_scale_y.setValue(3)
                self.slider_scale_z.setValue(3)

                self.plot()
        except FileNotFoundError:
            pass
        except ValueError:
            pass

    def update_del_figure(self):
        current_index = self.list_figures.currentRow()
        if current_index != -1:
            del self.set_dates[current_index]
            del self.set_figures[current_index]

            del self.value_turn_x[current_index]
            del self.value_turn_y[current_index]
            del self.value_turn_z[current_index]

            del self.value_shift_x[current_index]
            del self.value_shift_y[current_index]
            del self.value_shift_z[current_index]

            del self.value_scale[current_index]
            del self.value_scale_x[current_index]
            del self.value_scale_y[current_index]
            del self.value_scale_z[current_index]

            self.list_figures.takeItem(current_index)
            self.plot()

    def update_check_box(self, state):
        with_cnf_prev_state = self.with_cnf
        with_z_buffer_prev_state = self.with_z_buffer

        self.with_cnf = self.check_box_cnf.isChecked()
        self.with_z_buffer = self.check_box_z.isChecked()

        self.logger(
            log_info="Render method changed\nWith CNF: {0}\nWith Z-Buffer: {1}".format(self.with_cnf,
                                                                                       self.with_z_buffer),
            handling_type=self._handling_type_info
        )

        if not with_cnf_prev_state and self.with_cnf:
            self.coordinate_non_face = self.camera.eye()
            # self.coordinate_non_face = np.expand_dims(self.coordinate_non_face, axis=0)
            # self.coordinate_non_face = np.expand_dims(self.coordinate_non_face, axis=0)
            # self.coordinate_non_face = to4d(self.coordinate_non_face)
            # self.coordinate_non_face = np.dot(self.coordinate_non_face, np.transpose(self.matrix_shift_xy))[0, 0, :3]

        elif with_cnf_prev_state and not self.with_cnf:
            self.coordinate_non_face = None
        self.logger(
            log_info="Current coordinate of overview for CNF: {}".format(self.coordinate_non_face),
            handling_type=self._handling_type_info
        )
        # ^ is XOR
        render_restart = (with_z_buffer_prev_state ^ self.with_z_buffer) or (
                with_cnf_prev_state ^ self.with_cnf and not self.with_z_buffer)
        if render_restart:
            self.plot()

    def update_light(self, state):
        self.with_light = self.check_box_light_on.isChecked()

        if self.with_light:
            self.logger(
                log_info="Light ON",
                handling_type=self._handling_type_info
            )

        if not self.with_light:
            self.logger(
                log_info="Light OFF",
                handling_type=self._handling_type_info
            )

        if self.with_z_buffer:
            self.plot()


app = QApplication(sys.argv)
screen = Window()
screen.setGeometry(0, 0, 900, 900)
screen.show()
sys.exit(app.exec_())

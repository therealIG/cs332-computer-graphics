import sys
import matplotlib
import re

from lab6.pyFiles.shapes.shapes import Hexahedron
from lab8.pyFiles.simpleRender.graphic_utils import shape_render, create_empty_scene

matplotlib.use('Qt5Agg')
import h5py
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from lab6.pyFiles.affine_transform import *
from lab6.pyFiles.shapes.shapes import *
from lab6.pyFiles.projection import isometric_projection, orto_projection
from lab8.pyFiles.camera import *
from lab8.pyFiles.utils import *


class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        QWidget.__init__(self)
        layout = QGridLayout()
        self.setLayout(layout)
        self.data = None
        self.setStyleSheet("background-color: gray;")
        layout.setSpacing(10)

        # Значения слайдеров
        self.value_shift_x = []
        self.value_shift_y = []
        self.value_shift_z = []

        self.value_scale = []
        self.value_scale_x = []
        self.value_scale_y = []
        self.value_scale_z = []

        self.value_turn_x = []
        self.value_turn_y = []
        self.value_turn_z = []

        self.flag_for_display = 2

        self.coordinate_camera = np.array([100, 100, 1000], dtype=float)
        self.coordinate_target = np.array([100, 100, 0], dtype=float)

        # projection_width = 0.1
        # projection_height = 0.1
        # projection_far = 100.
        # projection_near = 0.1
        # projection_mat = perspective_projection(width=projection_width,
        #                                         height=projection_height,
        #                                         near=projection_near,
        #                                         far=projection_far)

        projection_mat = np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ], dtype=float)

        self.camera = Camera(self.coordinate_camera, self.coordinate_target, projection_mat)

        self.type_figure = None
        self.value_for_scale = [1 / 4, 1 / 3, 1 / 2, 1, 2, 3, 4, 5]

        self.type_projection = "orto_projection"
        self.set_figures = [] # множество всех фигур на сцене
        self.set_dates = []  # множество всех фигур на сцене
        # -------------------------------------------------------------------

        # --------------------------------------------------------------------------

        # Слайдеры смещения
        self.label_offset_x = QLabel(" Shift X")
        font = self.label_offset_x.font()
        font.setPointSize(10)
        self.label_offset_x.setFont(font)
        layout.addWidget(self.label_offset_x, 0, 0)
        self.slider_shift_x = QSlider(Qt.Horizontal)
        self.slider_shift_x.setFixedSize(300, 20)
        self.slider_shift_x.setRange(-200, 200)
        self.slider_shift_x.setValue(0)
        self.slider_shift_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_x.setTickInterval(20)
        self.slider_shift_x.setSingleStep(1)
        layout.addWidget(self.slider_shift_x, 1, 0)
        self.label_shift_x_value = QLabel(str(self.slider_shift_x.value()))
        layout.addWidget(self.label_shift_x_value, 1, 1)
        self.slider_shift_x.valueChanged.connect(self.update_slider_shift_x)
        self.label_shift_x_value.setFixedSize(27, 10)

        self.label_offset_y = QLabel(" Shift Y")
        self.label_offset_y.setFont(font)
        layout.addWidget(self.label_offset_y, 2, 0)
        self.slider_shift_y = QSlider(Qt.Horizontal)
        self.slider_shift_y.setFixedSize(300, 20)
        self.slider_shift_y.setRange(-200, 200)
        self.slider_shift_y.setValue(0)
        self.slider_shift_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_y.setTickInterval(20)
        self.slider_shift_y.setSingleStep(1)
        layout.addWidget(self.slider_shift_y, 3, 0)
        self.label_shift_y_value = QLabel(str(self.slider_shift_y.value()))
        layout.addWidget(self.label_shift_y_value, 3, 1)
        self.slider_shift_y.valueChanged.connect(self.update_slider_shift_y)
        self.label_shift_y_value.setFixedSize(27, 10)

        self.label_offset_z = QLabel(" Shift Z")
        self.label_offset_z.setFont(font)
        layout.addWidget(self.label_offset_z, 4, 0)
        self.slider_shift_z = QSlider(Qt.Horizontal)
        self.slider_shift_z.setFixedSize(300, 20)
        self.slider_shift_z.setRange(-200, 200)
        self.slider_shift_z.setValue(0)
        self.slider_shift_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_z.setTickInterval(20)
        self.slider_shift_z.setSingleStep(1)
        layout.addWidget(self.slider_shift_z, 5, 0)
        self.label_shift_z_value = QLabel(str(self.slider_shift_z.value()))
        layout.addWidget(self.label_shift_z_value, 5, 1)
        self.slider_shift_z.valueChanged.connect(self.update_slider_shift_z)
        self.label_shift_z_value.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Слайдеры поворота
        self.label_turn_x = QLabel(" Turn X")
        self.label_turn_x.setFont(font)
        layout.addWidget(self.label_turn_x, 6, 0)
        self.slider_turn_x = QSlider(Qt.Horizontal)
        self.slider_turn_x.setFixedSize(300, 20)
        self.slider_turn_x.setRange(-180, 180)
        self.slider_turn_x.setValue(0)
        self.slider_turn_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_x.setTickInterval(10)
        self.slider_turn_x.setSingleStep(1)
        layout.addWidget(self.slider_turn_x, 7, 0)
        self.label_turn_x_value = QLabel(str(self.slider_turn_x.value()))
        layout.addWidget(self.label_turn_x_value, 7, 1)
        self.slider_turn_x.valueChanged.connect(self.update_slider_turn_x)
        self.label_turn_x_value.setFixedSize(27, 10)

        self.label_turn_y = QLabel(" Turn Y")
        self.label_turn_y.setFont(font)
        layout.addWidget(self.label_turn_y, 8, 0)
        self.slider_turn_y = QSlider(Qt.Horizontal)
        self.slider_turn_y.setFixedSize(300, 20)
        self.slider_turn_y.setRange(-180, 180)
        self.slider_turn_y.setValue(0)
        self.slider_turn_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_y.setTickInterval(10)
        self.slider_turn_y.setSingleStep(1)
        layout.addWidget(self.slider_turn_y, 9, 0)
        self.label_turn_y_value = QLabel(str(self.slider_turn_y.value()))
        layout.addWidget(self.label_turn_y_value, 9, 1)
        self.slider_turn_y.valueChanged.connect(self.update_slider_turn_y)
        self.label_turn_y_value.setFixedSize(27, 10)

        self.label_turn_z = QLabel(" Turn Z")
        self.label_turn_z.setFont(font)
        layout.addWidget(self.label_turn_z, 10, 0)
        self.slider_turn_z = QSlider(Qt.Horizontal)
        self.slider_turn_z.setFixedSize(300, 20)
        self.slider_turn_z.setRange(-180, 180)
        self.slider_turn_z.setValue(0)
        self.slider_turn_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_z.setTickInterval(10)
        self.slider_turn_z.setSingleStep(1)
        layout.addWidget(self.slider_turn_z, 11, 0)
        self.label_turn_z_value = QLabel(str(self.slider_turn_z.value()))
        layout.addWidget(self.label_turn_z_value, 11, 1)
        self.slider_turn_z.valueChanged.connect(self.update_slider_turn_z)
        self.label_turn_z_value.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Слайдеры масшатибрования
        self.label_scale = QLabel(" Scale ")
        self.label_scale.setFont(font)
        layout.addWidget(self.label_scale, 12, 0)
        self.slider_scale = QSlider(Qt.Horizontal)
        self.slider_scale.setFixedSize(300, 20)
        self.slider_scale.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale.setRange(0, 7)
        self.slider_scale.setValue(3)
        self.slider_scale.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale.setTickInterval(1)
        self.slider_scale.setSingleStep(1)
        layout.addWidget(self.slider_scale, 13, 0)
        self.label_scale_value = QLabel(str(self.value_for_scale[self.slider_scale.value()]))
        layout.addWidget(self.label_scale_value, 13, 1)
        self.slider_scale.valueChanged.connect(self.update_slider_scale)
        self.label_scale_value.setFixedSize(27, 10)

        self.label_scale_x = QLabel(" Scale X")
        self.label_scale_x.setFont(font)
        layout.addWidget(self.label_scale_x, 14, 0)
        self.slider_scale_x = QSlider(Qt.Horizontal)
        self.slider_scale_x.setFixedSize(300, 20)
        self.slider_scale_x.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_x.setRange(0, 7)
        self.slider_scale_x.setValue(3)
        self.slider_scale_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_x.setTickInterval(1)
        self.slider_scale_x.setSingleStep(1)
        layout.addWidget(self.slider_scale_x, 15, 0)
        self.label_scale_value_x = QLabel(str(self.value_for_scale[self.slider_scale_x.value()]))
        layout.addWidget(self.label_scale_value_x, 15, 1)
        self.slider_scale_x.valueChanged.connect(self.update_slider_scale_x)
        self.label_scale_value_x.setFixedSize(27, 10)

        self.label_scale_y = QLabel(" Scale Y")
        self.label_scale_y.setFont(font)
        layout.addWidget(self.label_scale_y, 16, 0)
        self.slider_scale_y = QSlider(Qt.Horizontal)
        self.slider_scale_y.setFixedSize(300, 20)
        self.slider_scale_y.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_y.setRange(0, 7)
        self.slider_scale_y.setValue(3)
        self.slider_scale_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_y.setTickInterval(1)
        self.slider_scale_y.setSingleStep(1)
        layout.addWidget(self.slider_scale_y, 17, 0)
        self.label_scale_value_y = QLabel(str(self.value_for_scale[self.slider_scale_y.value()]))
        layout.addWidget(self.label_scale_value_y, 17, 1)
        self.slider_scale_y.valueChanged.connect(self.update_slider_scale_y)
        self.label_scale_value_y.setFixedSize(27, 10)

        self.label_scale_z = QLabel(" Scale Z")
        self.label_scale_z.setFont(font)
        layout.addWidget(self.label_scale_z, 18, 0)
        self.slider_scale_z = QSlider(Qt.Horizontal)
        self.slider_scale_z.setFixedSize(300, 20)
        self.slider_scale_z.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_z.setRange(0, 7)
        self.slider_scale_z.setValue(3)
        self.slider_scale_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_z.setTickInterval(1)
        self.slider_scale_z.setSingleStep(1)
        layout.addWidget(self.slider_scale_z, 19, 0)
        self.label_scale_value_z = QLabel(str(self.value_for_scale[self.slider_scale_z.value()]))
        layout.addWidget(self.label_scale_value_z, 19, 1)
        self.slider_scale_z.valueChanged.connect(self.update_slider_scale_z)
        self.label_scale_value_z.setFixedSize(27, 10)
        # -------------------------------------------------------------------------

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        layout.addWidget(self.canvas, 0, 2, 32, 9)
        # --------------------------------------------------------------------------
        self.type_projection = None
        self.corner_first = 0
        self.corner_second = 0
        self.axis = 0

        # Определение слайдеров положения камеры
        self.label_camera_position = QLabel(" Camera Position X,Y,Z ")
        self.label_camera_position.setFont(font)
        layout.addWidget(self.label_camera_position, 20, 0)
        # X
        self.slider_position_x = QSlider(Qt.Horizontal)
        self.slider_position_x.setFixedSize(300, 20)
        self.slider_position_x.setRange(-1000, 1000)
        self.slider_position_x.setValue(100)
        self.slider_position_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_x.setTickInterval(100)
        self.slider_position_x.setSingleStep(1)
        layout.addWidget(self.slider_position_x, 21, 0)
        self.label_position_value_x = QLabel(str(self.slider_position_x.value()))
        layout.addWidget(self.label_position_value_x, 21, 1)
        self.slider_position_x.valueChanged.connect(self.update_slider_position_x)
        self.label_position_value_x.setFixedSize(27, 10)

        # Y
        self.slider_position_y = QSlider(Qt.Horizontal)
        self.slider_position_y.setFixedSize(300, 20)
        self.slider_position_y.setRange(-1000, 1000)
        self.slider_position_y.setValue(100)
        self.slider_position_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_y.setTickInterval(100)
        self.slider_position_y.setSingleStep(1)
        layout.addWidget(self.slider_position_y, 22, 0)
        self.label_position_value_y = QLabel(str(self.slider_position_y.value()))
        layout.addWidget(self.label_position_value_y, 22, 1)
        self.slider_position_y.valueChanged.connect(self.update_slider_position_y)
        self.label_position_value_y.setFixedSize(27, 10)

        # Z
        self.slider_position_z = QSlider(Qt.Horizontal)
        self.slider_position_z.setFixedSize(300, 20)
        self.slider_position_z.setRange(-1000, 1000)
        self.slider_position_z.setValue(1000)
        self.slider_position_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_z.setTickInterval(100)
        self.slider_position_z.setSingleStep(1)
        layout.addWidget(self.slider_position_z, 23, 0)
        self.label_position_value_z = QLabel(str(self.slider_position_z.value()))
        layout.addWidget(self.label_position_value_z, 23, 1)
        self.slider_position_z.valueChanged.connect(self.update_slider_position_z)
        self.label_position_value_z.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Определение слайдеров положения цели
        self.label_camera_target = QLabel(" Camera Target X,Y,Z ")
        self.label_camera_target.setFont(font)
        layout.addWidget(self.label_camera_target, 24, 0)
        # X
        self.slider_position_target_x = QSlider(Qt.Horizontal)
        self.slider_position_target_x.setFixedSize(300, 20)
        self.slider_position_target_x.setRange(-1000, 1000)
        self.slider_position_target_x.setValue(100)
        self.slider_position_target_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_x.setTickInterval(100)
        self.slider_position_target_x.setSingleStep(1)
        layout.addWidget(self.slider_position_target_x, 25, 0)
        self.label_position_target_value_x = QLabel(str(self.slider_position_target_x.value()))
        layout.addWidget(self.label_position_target_value_x, 25, 1)
        self.slider_position_target_x.valueChanged.connect(self.update_slider_position_target_x)
        self.label_position_target_value_x.setFixedSize(27, 10)

        # Y
        self.slider_position_target_y = QSlider(Qt.Horizontal)
        self.slider_position_target_y.setFixedSize(300, 20)
        self.slider_position_target_y.setRange(-1000, 1000)
        self.slider_position_target_y.setValue(100)
        self.slider_position_target_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_y.setTickInterval(100)
        self.slider_position_target_y.setSingleStep(1)
        layout.addWidget(self.slider_position_target_y, 26, 0)
        self.label_position_target_value_y = QLabel(str(self.slider_position_target_y.value()))
        layout.addWidget(self.label_position_target_value_y, 26, 1)
        self.slider_position_target_y.valueChanged.connect(self.update_slider_position_target_y)
        self.label_position_target_value_y.setFixedSize(27, 10)

        # Z
        self.slider_position_target_z = QSlider(Qt.Horizontal)
        self.slider_position_target_z.setFixedSize(300, 20)
        self.slider_position_target_z.setRange(-1000, 1000)
        self.slider_position_target_z.setValue(0)
        self.slider_position_target_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_z.setTickInterval(100)
        self.slider_position_target_z.setSingleStep(10)
        layout.addWidget(self.slider_position_target_z, 27, 0)
        self.label_position_target_value_z = QLabel(str(self.slider_position_target_z.value()))
        layout.addWidget(self.label_position_target_value_z, 27, 1)
        self.slider_position_target_z.valueChanged.connect(self.update_slider_position_target_z)
        self.label_position_target_value_z.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Лист всех фигур + кнопки добавления и удаления фигур
        self.label_list_figures = QLabel(" List figures on Scene ")
        self.label_list_figures.setFont(font)
        layout.addWidget(self.label_list_figures, 30, 0)

        self.list_figures = QListWidget()
        self.list_figures.setFont(font)
        self.list_figures.setMaximumWidth(200)
        layout.addWidget(self.list_figures, 31, 0)

        self.button_add_figure = QPushButton(" Add figure")
        self.button_add_figure.setFont(font)
        self.button_add_figure.setFixedSize(300, 40)
        self.button_add_figure.clicked.connect(self.update_add_figure)
        layout.addWidget(self.button_add_figure, 28, 0)

        self.button_del_figure = QPushButton(" Delete figure")
        self.button_del_figure.setFont(font)
        self.button_del_figure.setFixedSize(300, 40)
        self.button_del_figure.clicked.connect(self.update_del_figure)
        layout.addWidget(self.button_del_figure, 29, 0)
        # --------------------------------------------------------------------------

    def plot(self):
        """
        Отрисовка изображения в 2D
        :return: None
        """
        self.figure.clear()
        ax = self.figure.add_subplot(111)

        scene, z_buffer = create_empty_scene(height=1000, width=1900)
        lookat_matrix = self.camera.lookat()
        for i in range(len(self.set_figures)):
            data = self.set_dates[i]
            shape_type = self.set_figures[i]
            shape_for_render = self._shape_choosing(shape_type)(np.array([250, 250, 0]), 50.0)
            prepared_vertexes = to4d(data.vertexes())
            vertexes_after_view_translation = np.dot(prepared_vertexes, lookat_matrix.T)
            vertexes_after_view_translation = to3d(vertexes_after_view_translation)
            shape_for_render.vertexes(vertexes_after_view_translation)
            shape_render(shape_for_render, scene, z_buffer, shape_for_render.colors(), -i)
        # graphic_scene = QGraphicsScene()
        # pixmap1 = QPixmap.fromImage(
        #     QImage(scene.astype(np.uint8), 1900, 1000, QImage.Format_RGB888))
        # graphic_scene.addPixmap(pixmap1)
        # self.figure.setScene(graphic_scene)

        ax.add_collection(
            PolyCollection(scene[:, :, :2], facecolors='cyan', linewidths=1, edgecolors='r'))
        ax.set_xticks(np.arange(-50, 60, 10))
        ax.set_yticks(np.arange(-50, 60, 10))
        self.canvas.draw()
        # ---------------------------------------------------

    def update_slider_turn_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_turn_x_value.setText(str(self.slider_turn_x.value()))
            current_data = rotate_around_x(current_data, self.value_turn_x[current_index] - self.slider_turn_x.value())
            self.value_turn_x[current_index] = self.slider_turn_x.value()
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_turn_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_turn_y_value.setText(str(self.slider_turn_y.value()))
            current_data = rotate_around_y(current_data, self.value_turn_y[current_index] - self.slider_turn_y.value())
            self.value_turn_y[current_index] = self.slider_turn_y.value()
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_turn_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_turn_z_value.setText(str(self.slider_turn_z.value()))
            current_data = rotate_around_z(current_data, self.value_turn_z[current_index] - self.slider_turn_z.value())
            self.value_turn_z[current_index] = self.slider_turn_z.value()
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_shift_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_shift_x_value.setText(str(self.slider_shift_x.value()))
            current_data = shift_x(current_data, self.slider_shift_x.value() - self.value_shift_x[current_index])
            self.value_shift_x[current_index] = self.slider_shift_x.value()
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_shift_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_shift_y_value.setText(str(self.slider_shift_y.value()))
            current_data = shift_y(current_data, self.slider_shift_y.value() - self.value_shift_y[current_index])
            self.value_shift_y[current_index] = self.slider_shift_y.value()
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_shift_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_shift_z_value.setText(str(self.slider_shift_z.value()))
            current_data = shift_z(current_data, self.slider_shift_z.value() - self.value_shift_z[current_index])
            self.value_shift_z[current_index] = self.slider_shift_z.value()
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_scale(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_scale_value.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.label_scale_value_x.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.label_scale_value_y.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.label_scale_value_z.setText(str(self.value_for_scale[self.slider_scale.value()]))
            current_data = scale_center(current_data, self.value_for_scale[self.slider_scale.value()] / self.value_scale[current_index])
            self.value_scale[current_index] = self.value_for_scale[self.slider_scale.value()]
            self.value_scale_x[current_index] = self.value_for_scale[self.slider_scale.value()]
            self.value_scale_y[current_index] = self.value_for_scale[self.slider_scale.value()]
            self.value_scale_z[current_index] = self.value_for_scale[self.slider_scale.value()]
            self.slider_scale_x.setValue(self.slider_scale.value())
            self.slider_scale_y.setValue(self.slider_scale.value())
            self.slider_scale_z.setValue(self.slider_scale.value())
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_scale_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_scale_value_x.setText(str(self.value_for_scale[self.slider_scale_x.value()]))
            current_data = scale_x(current_data, self.value_for_scale[self.slider_scale_x.value()] / self.value_scale_x[current_index])
            self.value_scale_x[current_index] = self.value_for_scale[self.slider_scale_x.value()]
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_scale_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_scale_value_y.setText(str(self.value_for_scale[self.slider_scale_y.value()]))
            current_data = scale_y(current_data, self.value_for_scale[self.slider_scale_y.value()] / self.value_scale_y[current_index])
            self.value_scale_y[current_index] = self.value_for_scale[self.slider_scale_y.value()]
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_scale_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        current_index = self.list_figures.currentRow()
        current_data = self.set_dates[current_index].vertexes()
        if current_data is not None:
            self.label_scale_value_z.setText(str(self.value_for_scale[self.slider_scale_z.value()]))
            current_data = scale_z(current_data, self.value_for_scale[self.slider_scale_z.value()] / self.value_scale_z[current_index])
            self.value_scale_z[current_index] = self.value_for_scale[self.slider_scale_z.value()]
            self.set_dates[current_index].vertexes(current_data)
            self.plot()

    def update_slider_position_x(self):
        current_index = self.list_figures.currentRow()
        self.label_position_value_x.setText(str(self.slider_position_x.value()))
        self.coordinate_camera[0] = self.slider_position_x.value()
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_y(self):
        current_index = self.list_figures.currentRow()
        self.label_position_value_y.setText(str(self.slider_position_y.value()))
        self.coordinate_camera[1] = self.slider_position_y.value()
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_z(self):
        current_index = self.list_figures.currentRow()
        self.label_position_value_z.setText(str(self.slider_position_z.value()))
        self.coordinate_camera[2] = self.slider_position_z.value()
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_target_x(self):
        current_index = self.list_figures.currentRow()
        self.label_position_target_value_x.setText(str(self.slider_position_target_x.value()))
        self.coordinate_target[0] = self.slider_position_target_x.value()
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_slider_position_target_y(self):
        current_index = self.list_figures.currentRow()
        self.label_position_target_value_y.setText(str(self.slider_position_target_y.value()))
        self.coordinate_target[1] = self.slider_position_target_y.value()
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_slider_position_target_z(self):
        current_index = self.list_figures.currentRow()
        self.label_position_target_value_z.setText(str(self.slider_position_target_z.value()))
        self.coordinate_target[2] = self.slider_position_target_z.value()
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def _shape_choosing(self, shape_type):
        if shape_type == "Dodecahedron":
            return Dodecahedron
        if shape_type == "Hexahedron":
            return Hexahedron
        if shape_type == "Icocahedron":
            return Icosahedron
        if shape_type == "Octahedron":
            return Octahedron
        if shape_type == "Tetrahedron":
            return Tetrahedron

    def update_add_figure(self):
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', '../data/task3/')[0]
            with h5py.File(fname, 'r') as hf:
                fname = fname.split("/")[-1].split(".")[0]
                if fname == "Dodecahedron":
                    self.type_figure = "Dodecahedron"
                    cube = Dodecahedron(np.array([250, 250, 0]), 50.0)
                if fname == "Hexahedron":
                    self.type_figure = "Hexahedron"
                    cube = Hexahedron(np.array([250, 250, 0]), 50.0)
                if fname == "Icocahedron":
                    self.type_figure = "Icocahedron"
                    cube = Icosahedron(np.array([250, 250, 0]), 50.0)
                if fname == "Octahedron":
                    self.type_figure = "Octahedron"
                    cube = Octahedron(np.array([250, 250, 0]), 50.0)
                if fname == "Tetrahedron":
                    self.type_figure = "Tetrahedron"
                    cube = Tetrahedron(np.array([250, 250, 0]), 50.0)
                self.list_figures.addItem(fname)
                self.set_figures.append(fname)
                self.set_dates.append(cube)

                self.value_turn_x.append(0)
                self.value_turn_y.append(0)
                self.value_turn_z.append(0)

                self.value_shift_x.append(0)
                self.value_shift_y.append(0)
                self.value_shift_z.append(0)

                self.value_scale.append(1)
                self.value_scale_x.append(1)
                self.value_scale_y.append(1)
                self.value_scale_z.append(1)

                self.slider_shift_x.setValue(0)
                self.slider_shift_y.setValue(0)
                self.slider_shift_z.setValue(0)

                self.slider_turn_x.setValue(0)
                self.slider_turn_y.setValue(0)
                self.slider_turn_z.setValue(0)

                self.slider_scale.setValue(3)
                self.slider_scale_x.setValue(3)
                self.slider_scale_y.setValue(3)
                self.slider_scale_z.setValue(3)

                self.plot()
        except FileNotFoundError:
            pass
        except ValueError:
            pass

    def update_del_figure(self):
        current_index = self.list_figures.currentRow()
        del self.set_dates[current_index]
        del self.set_figures[current_index]

        del self.value_turn_x[current_index]
        del self.value_turn_y[current_index]
        del self.value_turn_z[current_index]

        del self.value_shift_x[current_index]
        del self.value_shift_y[current_index]
        del self.value_shift_z[current_index]

        del self.value_scale[current_index]
        del self.value_scale_x[current_index]
        del self.value_scale_y[current_index]
        del self.value_scale_z[current_index]

        self.list_figures.takeItem(current_index)
        self.plot()


app = QApplication(sys.argv)
screen = Window()
screen.setGeometry(0, 0, 900, 900)
screen.show()
sys.exit(app.exec_())

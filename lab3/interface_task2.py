from lab3.calculate import *
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene, QGraphicsLineItem, QWidget
from PyQt5 import QtCore, uic
import cv2

qtCreatorFile = "task2.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.clear)
        self.image = cv2.resize(cv2.imread("sample.png"), (800, 600))
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(self.image.data, self.image.shape[1], self.image.shape[0], self.image.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        self.graphicsView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.graphicsView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setMouseTracking(True)
        self.setMouseMoveEventDelegate(self.graphicsView)
        self.listPoint = []
        self.point_for_start_task1 = None

    def setMouseMoveEventDelegate(self, setQWidget):
        def subWidgetMouseMoveEvent(eventQMouseEvent):
            QWidget.mouseMoveEvent(setQWidget, eventQMouseEvent)

        setQWidget.setMouseTracking(True)
        setQWidget.mouseMoveEvent = subWidgetMouseMoveEvent

    def clear(self):
        self.image = cv2.resize(cv2.imread("sample.png"), (800, 600))
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(self.image.data, self.image.shape[1], self.image.shape[0], self.image.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        self.graphicsView.viewport().update()

    def mouseDoubleClickEvent(self, event):
        self.point_for_start_task1 = event.pos()
        bordes, self.image = find_contour(self.image, self.point_for_start_task1.x(), self.point_for_start_task1.y())
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(self.image.data, self.image.shape[1], self.image.shape[0], self.image.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        print(bordes)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.setFixedSize(800, 670)
    window.show()
    sys.exit(app.exec_())

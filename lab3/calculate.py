import numpy as np
import sys
import cv2

def find_contour(source, row, column):
    """
    finds contours
    :param source: ndarray (CVImage)
    :param row: initial y position
    :param column: initial x position
    :return: set of points
    """
    sys.setrecursionlimit(15000)

    def isborder(r, c):
        up = np.all(source[r-1, c] == color)
        down = np.all(source[r+1, c] == color)
        left = np.all(source[r, c-1] == color)
        right = np.all(source[r, c+1] == color)
        current = np.all(source[r, c] == color)
        values = [up, down, left, right]
        return np.any(values) and not current

    color = source[row, column]
    surveyed_color = color
    border_row = row
    while np.all(color == surveyed_color):
        border_row -= 1
        surveyed_color = source[border_row, column]
    finish_row = row
    surveyed_color = color
    while np.all(color == surveyed_color):
        finish_row -= 1
        surveyed_color = source[finish_row, column-1]
    start_contour = (border_row, column)
    finish_contour = (finish_row, column-1)
    borders = set()
    borders.add(start_contour)
    surveyed_contour = (0, 0)
    bypass_row = border_row + 1
    bypass_column = column

    def move_right():
        nonlocal bypass_column, bypass_row, surveyed_contour, finish_contour
        while surveyed_contour != finish_contour:
            if isborder(bypass_row-1, bypass_column):
                surveyed_contour = (bypass_row-1, bypass_column)
                borders.add(surveyed_contour)
                source[bypass_row-1, bypass_column] = [0, 0, 255]
            else:
                if isborder(bypass_row, bypass_column+1):
                    move_down()
                else:
                    bypass_row -= 1
                    move_up()
            if not isborder(bypass_row, bypass_column+1):
                bypass_column += 1
            else:
                if isborder(bypass_row, bypass_column + 1):
                    move_down()
                else:
                    move_up()

    def move_up():
        nonlocal bypass_column, bypass_row, surveyed_contour, finish_contour
        while surveyed_contour != finish_contour:
            if isborder(bypass_row, bypass_column-1):
                surveyed_contour = (bypass_row, bypass_column-1)
                borders.add(surveyed_contour)
                source[bypass_row, bypass_column-1] = [0, 0, 255]
            else:
                bypass_column -= 1
                move_left()
            if not isborder(bypass_row-1, bypass_column):
                bypass_row -= 1
            else:
                move_right()

    def move_down():
        nonlocal bypass_column, bypass_row, surveyed_contour, finish_contour
        while surveyed_contour != finish_contour:
            if isborder(bypass_row, bypass_column + 1):
                surveyed_contour = (bypass_row, bypass_column + 1)
                borders.add(surveyed_contour)
                source[bypass_row, bypass_column + 1] = [0, 0, 255]
            else:
                bypass_column += 1
                move_right()
            if not isborder(bypass_row + 1, bypass_column):
                bypass_row += 1
            else:
                move_left()

    def move_left():
        nonlocal bypass_column, bypass_row, surveyed_contour, finish_contour
        while surveyed_contour != finish_contour:
            if isborder(bypass_row + 1, bypass_column):
                surveyed_contour = (bypass_row + 1, bypass_column)
                borders.add(surveyed_contour)
                source[bypass_row + 1, bypass_column] = [0, 0, 255]
            else:
                bypass_row += 1
                move_down()
            if not isborder(bypass_row, bypass_column-1):
                bypass_column -= 1
            else:
                if isborder(bypass_row, bypass_column-1):
                    move_up()
    move_right()
    source = cv2.cvtColor(source, cv2.COLOR_BGR2RGB)
    return borders, source


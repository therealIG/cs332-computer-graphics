import numpy as np
import cv2
import queue


def draw_line(image, start_pos, q, target_image = None):
    y = start_pos[1]
    x = start_pos[0]

    if image[y, x] == 255:
        while (x > 0) and (image[y, x] == 255):
            x -= 1
        x += 1
        while x < (image.shape[1] - 1) and (image[y, x] == 255):
            image[y, x] = target_image[y, x]
            if y < (image.shape[0] - 1):
                q.put((x, y+1))
            if y > 0:
                q.put((x, y-1))
            x += 1


def fill_area(image: np.ndarray, paint_for_filling: np.ndarray, start):
    image = image[:, :, 0]

    x = start.x()
    y = start.y()

    paint_for_filling = cv2.cvtColor(paint_for_filling, cv2.COLOR_BGR2GRAY)
    paint_for_filling = cv2.resize(paint_for_filling, (image.shape[1], image.shape[0]))

    q = queue.Queue()
    q.put((x, y))

    while not q.empty():
        cur_pos = q.get()
        draw_line(image, cur_pos, q, paint_for_filling)

    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    return image

import sys

from lab3.task1 import *
from PyQt5.QtCore import QPoint
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene, QGraphicsLineItem, QWidget
from PyQt5 import QtCore, uic
import numpy as np
import cv2

qtCreatorFile = "design.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.clear)
        self.fill1.clicked.connect(self.fill_with_color)
        self.fill2.clicked.connect(self.fill_with_image)
        self.lastPoint = QPoint()
        self.drawing = False
        blank_image2 = 255 * np.ones(shape=[600, 800, 3], dtype=np.uint8)
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(blank_image2, blank_image2.shape[1], blank_image2.shape[0], blank_image2.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        self.graphicsView.setSceneRect(0, 0, 800, 600)
        self.graphicsView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.graphicsView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setMouseTracking(True)
        self.setMouseMoveEventDelegate(self.graphicsView)
        self.listPoint = []
        self.point_for_start_task1 = None
        self.image = None

    def setMouseMoveEventDelegate(self, setQWidget):
        def subWidgetMouseMoveEvent(eventQMouseEvent):
            QWidget.mouseMoveEvent(setQWidget, eventQMouseEvent)

        setQWidget.setMouseTracking(True)
        setQWidget.mouseMoveEvent = subWidgetMouseMoveEvent

    def clear(self):
        clear_img = 255 * np.ones(shape=[600, 800, 3], dtype=np.uint8)
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(clear_img, clear_img.shape[1], clear_img.shape[0], clear_img.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        self.graphicsView.viewport().update()
        self.lastPoint = QPoint()
        self.listPoint = []

    def mouseDoubleClickEvent(self, event):
        self.point_for_start_task1 = event.pos()

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.drawing = True
            self.lastPoint = event.pos()
            self.listPoint.append([event.x(), event.y()])

    def mouseMoveEvent(self, event):
        if event.buttons() and QtCore.Qt.LeftButton and self.drawing:
            scene = self.graphicsView.scene()
            scene.addItem(QGraphicsLineItem(QtCore.QLineF(self.lastPoint, event.pos())))
            self.graphicsView.viewport().update()
            self.lastPoint = event.pos()
            if 800 >= event.x() > 0 and 600 >= event.y() > 0:
                self.listPoint.append([event.x(), event.y()])
            self.update()

    def fill_with_color(self):
        channels_count = 4
        pixmap = self.graphicsView.grab()
        image = pixmap.toImage()
        b = image.bits()
        b.setsize(600 * 800 * channels_count)
        self.image = np.frombuffer(b, np.uint8).reshape((600, 800, channels_count))
        image_after_fill = fill_area(self.image, np.zeros_like(self.image), self.point_for_start_task1)
        scene_after_fill = QGraphicsScene()
        pixmap_after_fill = QPixmap.fromImage(
            QImage(image_after_fill.data, image_after_fill.shape[1], image_after_fill.shape[0],
                   QImage.Format_RGB888))
        scene_after_fill.addPixmap(pixmap_after_fill)
        self.graphicsView.setScene(scene_after_fill)
        self.graphicsView.viewport().update()

    def fill_with_image(self):
        image_for_fill = cv2.resize(cv2.imread("Lenna.png"), (800, 600))
        channels_count = 4
        pixmap = self.graphicsView.grab()
        image = pixmap.toImage()
        b = image.bits()
        b.setsize(600 * 800 * channels_count)
        self.image = np.frombuffer(b, np.uint8).reshape((600, 800, channels_count))
        image_after_fill = fill_area(self.image, image_for_fill, self.point_for_start_task1)
        scene_after_fill = QGraphicsScene()
        pixmap_after_fill = QPixmap.fromImage(
            QImage(image_after_fill.data, image_after_fill.shape[1], image_after_fill.shape[0],
                   QImage.Format_RGB888))
        scene_after_fill.addPixmap(pixmap_after_fill)
        self.graphicsView.setScene(scene_after_fill)
        self.graphicsView.viewport().update()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.setFixedSize(827, 689)
    window.show()
    sys.exit(app.exec_())

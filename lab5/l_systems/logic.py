import turtle
import canvasvg
import cairosvg
import tkinter

SYSTEM_RULES = {}  # generator system rules for l-system


def _derivation(axiom, steps):
    derived = [axiom]  # seed
    for _ in range(steps):
        next_seq = derived[-1]
        next_axiom = [_rule(char) for char in next_seq]
        derived.append(''.join(next_axiom))
    return derived


def _rule(sequence):
    if sequence in SYSTEM_RULES:
        return SYSTEM_RULES[sequence]
    return sequence


def _draw_l_system(turtle, SYSTEM_RULES, seg_length, angle):
    stack = []
    for command in SYSTEM_RULES:
        turtle.pd()
        if command == 'F':
            turtle.forward(seg_length)
        elif command == "f":
            turtle.pu()  # pen up - not drawing
            turtle.forward(seg_length)
        elif command == "+":
            turtle.right(angle)
        elif command == "-":
            turtle.left(angle)
        elif command == "[":
            stack.append((turtle.position(), turtle.heading()))
        elif command == "]":
            turtle.pu()  # pen up - not drawing
            position, heading = stack.pop()
            turtle.goto(position)
            turtle.setheading(heading)


def _set_turtle(alpha_zero, w):
    r_turtle = turtle.RawTurtle(w)  # recursive turtle
    r_turtle.speed(0)  # adjust as needed (0 = fastest)
    r_turtle.setheading(alpha_zero)  # initial heading
    return r_turtle


def l_system(iterations: int, step_size: int):
    input_file = open('l_system.txt', 'r')
    # getting all information from input file
    config = input_file.readline().split(' ')
    axiom = config[0]
    angle = float(config[1])
    alpha_zero = float(config[2])  # initial direction in degrees (0 - right, 90 - up, 180 - left, 270 - down
    for rule_line in input_file:
        key, value = rule_line.split('->')
        SYSTEM_RULES[key] = value

    model = _derivation(axiom, iterations)  # axiom (initial string), nth iterations
    root = tkinter.Tk()
    canvas = turtle.ScrolledCanvas(root)
    canvas.pack(side=tkinter.LEFT)

    screen = turtle.TurtleScreen(canvas)
    # Set turtle parameters and draw L-System
    t = turtle.RawTurtle(screen)  # recursive turtle
    t.screen.screensize(5000, 5000)
    t.hideturtle()
    t.speed(0)  # adjust as needed (0 = fastest)
    t.setheading(alpha_zero)  # initial heading
    screen.tracer(0)
    _draw_l_system(t, model[-1], step_size, angle)  # draw model
    screen.update()
    canvasvg.saveall('sample.svg', canvas)
    root.destroy()
    cairosvg.svg2png(url="sample.svg", write_to='output.png')
from lab5.l_systems import *
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from matplotlib.widgets import Slider, Button

from lab5.l_systems.logic import l_system


def update(val):
    global ax
    step_size = int(step_size_slider.val)
    iterations = int(iterations_slider.val)
    l_system(iterations, step_size)
    img = mpimg.imread('output.png')
    ax.clear()
    ax.imshow(img)
    plt.draw()


fig = plt.subplots(1, 1)
plt.subplots_adjust(left=0.1, bottom=0.2)
plt.subplot(1, 1, 1)
ax = fig[0].gca()

iterations_axe = plt.axes([0.15, 0.08, 0.7, 0.025])
step_size_axe = plt.axes([0.15, 0.12, 0.7, 0.025])
iterations_slider = Slider(iterations_axe, 'size', 0, 15, 0, valstep=1)
step_size_slider = Slider(step_size_axe, 'step_size', 0, 40, 5, valstep=1)

iterations_slider.on_changed(update)
step_size_slider.on_changed(update)
plt.show()

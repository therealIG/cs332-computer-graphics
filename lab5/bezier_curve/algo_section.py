import numpy as np
from lab5.bezier_curve.utils import *


class ref_point:
    def __init__(self, position: tuple, offset: int = 50, coef: list = None, size: int = 1000):
        self._coefficient = coef_init(size) if coef is None else coef
        self._position = position

        self._right_support = (position[0] + offset, position[1])
        self._left_support = (position[0] - offset, position[1])

        self._as_first_point = np.multiply(self._coefficient[0], self._position)
        self._right_block = np.sum(np.array([self._as_first_point,
                                             np.multiply(self._coefficient[1], self._right_support)]), axis=0)

        self._as_last_point = np.multiply(self._coefficient[3], self._position)
        self._left_block = np.sum(np.array([self._as_last_point,
                                            np.multiply(self._coefficient[2], self._left_support)]), axis=0)

    def _recalc_in_point_value(self):
        self._as_first_point = np.multiply(self._coefficient[0], self._position)
        self._as_last_point = np.multiply(self._coefficient[3], self._position)

    def _recalc_right(self):
        self._right_block = np.sum(np.array([self._as_first_point,
                                             np.multiply(self._coefficient[1], self._right_support)]), axis=0)

    def _recalc_left(self):
        self._left_block = np.sum(np.array([self._as_last_point,
                                            np.multiply(self._coefficient[2], self._left_support)]), axis=0)

    def position(self):
        return self._position

    def get_right_block(self):
        return self._right_block

    def get_left_block(self):
        return self._left_block

    def update_point(self, new_position):
        left_offsets = (self._position[0] - self._left_support[0], self._position[1] - self._left_support[1])
        right_offsets = (self._position[0] - self._right_support[0], self._position[1] - self._right_support[1])
        self._position = new_position
        self._left_support = (self._position[0] - left_offsets[0], self._position[1] - left_offsets[1])
        self._right_support = (self._position[0] - right_offsets[0], self._position[1] - right_offsets[1])

        self._recalc_in_point_value()
        self._recalc_left()
        self._recalc_right()

    def left_support_point(self, point=None):
        if point is None:
            return self._left_support
        else:
            sign = rotation_dir([self._position, self._left_support], point)
            prev_position = self._left_support
            self._left_support = point
            cos_value = line_cos(self._position, prev_position, self._position, self._left_support)
            self._right_support = tuple(
                rotate_around_point(self._right_support, cos_value, self._position, sign).astype(np.int32))
            print(self._right_support)
            self._recalc_left()
            self._recalc_right()
            print("done left")
            return None

    def right_support_point(self, point=None):
        if point is None:
            return self._right_support
        else:
            sign = rotation_dir([self._position, self._right_support], point)
            prev_position = self._right_support
            self._right_support = point
            cos_value = line_cos(self._position, prev_position, self._position, self._right_support)
            self._left_support = tuple(
                rotate_around_point(self._left_support, cos_value, self._position, sign).astype(np.int32))

            self._recalc_left()
            self._recalc_right()
            print("done right")
            return None


class BezierCurve:
    def __init__(self, size: int = 1000, coef: list = None, offset: int = 50):
        self._offset = offset
        self._coef = coef_init(size) if coef is None else coef
        self._points = []

    def points(self):
        return self._points

    @staticmethod
    def _comporator_seq(prev, next_p):
        r, l = prev.get_right_block(), next_p.get_left_block()
        return np.sum([r, l], axis=0)

    @staticmethod
    def _comporator(prev, next_p):
        if prev.position()[0] <= next_p.position()[0]:
            r, l = prev.get_right_block(), next_p.get_left_block()
            return np.sum([r, l], axis=0)
        else:
            r, l = next_p.get_right_block(), prev.get_left_block()
            return np.flip(np.sum([r, l], axis=0), axis=0)

    def get_line(self):
        result = []
        prev = self._points[0]
        for i in range(len(self._points) - 1):
            index = i + 1
            result.append(self._comporator_seq(prev, self._points[index]))
            prev = self._points[index]
        return np.concatenate(result, axis=0)

    def add_point(self, position):
        point = ref_point(position, self._offset, self._coef)
        self._points.append(point)

        return point.left_support_point(), point.right_support_point()

    def get_support_point(self, position):
        point = ref_point(position, self._offset, self._coef)

        return point.left_support_point(), point.right_support_point()

    def update_point(self, index, new_position):
        point = self._points[index]
        point.update_point(new_position)

        return point.left_support_point(), point.right_support_point()

    def delete_point(self, index):
        point = self._points[index]
        self._points.remove(point)

    def update_support_point(self, ref_point_index: int, is_left: bool, new_position: tuple):
        point = self._points[ref_point_index]
        if is_left:
            point.left_support_point(new_position)
            return point.right_support_point()
        else:
            point.right_support_point(new_position)
            return point.left_support_point()

import sys
import typing

sys.path.append("./../../")

from PyQt5.QtCore import QRectF, QLineF
from PyQt5.QtGui import QPainter, QFont, QPen, QColor
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene, QWidget, QGraphicsItem, QStyleOptionGraphicsItem, QGraphicsTextItem
from PyQt5 import QtCore, uic, QtGui
from lab5.bezier_curve.algo_section import *

qtCreatorFile = "design.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class Point_support(QGraphicsItem):
    def __init__(self, posx, posy):
        super().__init__()
        self.allowDrag = True
        self.posx = posx
        self.posy = posy

    def boundingRect(self):
        return QRectF(self.posx, self.posy, 5, 5)

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem,
              widget: typing.Optional[QWidget] = ...) -> None:
        painter.setBrush(QtGui.QColor(0, 0, 255))
        painter.drawEllipse(self.boundingRect())


class Point(QGraphicsItem):
    def __init__(self, posx, posy):
        super().__init__()
        self.allowDrag = True
        self.posx = posx
        self.posy = posy

    def boundingRect(self):
        return QRectF(self.posx, self.posy, 7, 7)

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem,
              widget: typing.Optional[QWidget] = ...) -> None:
        painter.drawEllipse(self.boundingRect())


def setMouseMoveEventDelegate(setQWidget):
    def subWidgetMouseMoveEvent(eventQMouseEvent):
        QWidget.mouseMoveEvent(setQWidget, eventQMouseEvent)

    setQWidget.setMouseTracking(True)
    setQWidget.mouseMoveEvent = subWidgetMouseMoveEvent


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.delete_button.clicked.connect(self.delete)
        self.change_button.clicked.connect(self.change)
        self.change_button_2.clicked.connect(self.change_support)
        self.clear_button.clicked.connect(self.clear)
        self.DELTA = 10
        self.graphicsView.setAcceptDrops(True)
        self.setAcceptDrops(True)
        self.change = False
        self.point_for_cahnge = None
        self.bool_change_support = False
        self.support_point_for_cahnge = False

        self.list_point = []
        self.list_support_point = []
        self.list_object = []
        self.flag_change_point = False
        scene = QGraphicsScene(0, 0, 1400, 1000)
        self.scene = scene
        self.graphicsView.setScene(scene)
        self.graphicsView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.graphicsView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setMouseTracking(True)
        setMouseMoveEventDelegate(self.graphicsView)
        self.Bezier = BezierCurve()

    def clear(self):
        scene = QGraphicsScene(0, 0, 1400, 1000)
        self.scene = scene
        self.graphicsView.setScene(scene)
        self.graphicsView.viewport().update()
        self.list_point = []
        self.list_support_point = []
        self.list_object = []
        self.listWidget.clear()
        self.listWidget_2.clear()
        self.Bezier = BezierCurve()
        self.change = False
        self.point_for_cahnge = None
        self.support_point_for_cahnge = None
        self.bool_change_support = False

    def removeSel(self):
        listItems = self.listWidget.selectedItems()
        if not listItems:
            return
        for item in listItems:
            self.listWidget.takeItem(self.listWidget.row(item))

    def changeSel_support(self, position, ind):
        sel_items = self.listWidget_2.selectedItems()
        for item in sel_items:
            item.setText(str(position[0])+","+str(position[1])+" "+str(ind))

    def changeSel(self, position, ind):
        sel_items = self.listWidget.selectedItems()

        for item in sel_items:
            item.setText(str(position[0])+","+str(position[1])+" "+str(ind))

    def refresh(self):
        self.listWidget.clear()
        for x in range(len(self.list_point)):
            current = str(self.list_point[x])
            current = current.replace("]", " ")
            current = current.replace("[", " ")
            current = current.replace(",", " ")
            lst = current.split()
            self.listWidget.addItem(str(lst[0]) + "," + str(lst[1]) + " " + str(x))

    def refresh_support(self):
        self.listWidget_2.clear()
        for x in range(len(self.list_support_point)):
            current = str(self.list_support_point[x])
            current = current.replace("]", " ")
            current = current.replace("[", " ")
            current = current.replace(",", " ")
            lst = current.split()
            self.listWidget_2.addItem(str(lst[0]) + "," + str(lst[1]) + " " + str(x))

    def delete(self):
        font = QFont("Calibri", 13)
        try:
            current = self.listWidget.currentItem().text()
            current = current.replace(",", " ")
            lst = current.split()
            ind = self.list_point.index("[" + lst[0] + ", " + lst[1] + "]")
            self.graphicsView.setScene(self.scene)
            self.graphicsView.update()
            self.list_support_point.pop(ind*2)
            self.list_support_point.pop(ind*2)
            self.list_point.remove("[" + lst[0] + ", " + lst[1] + "]")
            self.Bezier.delete_point(ind)
            scene = QGraphicsScene(0, 0, 1400, 1000)
            for i in range(len(self.list_point)):
                x = self.list_point[i]
                x = x.replace("[", "")
                x = x.replace("]", "")
                x = x.replace(",", " ")
                y = x.split()

                x_sup = self.list_support_point[i * 2]
                x_sup = x_sup.replace("[", "").replace("]", "").replace(",", " ").split()
                textItem = QGraphicsTextItem(str(i * 2))
                textItem.setPos(int(x_sup[0]) + 5, int(x_sup[1]) + 5)
                scene.addItem(textItem)
                scene.addItem(Point_support(int(x_sup[0]), int(x_sup[1])))
                x1 = int(x_sup[0])
                y1 = int(x_sup[1])

                x_sup = self.list_support_point[i * 2 + 1]
                x_sup = x_sup.replace("[", "").replace("]", "").replace(",", " ").split()
                textItem = QGraphicsTextItem(str(i * 2 + 1))
                textItem.setPos(int(x_sup[0]) + 5, int(x_sup[1]) + 5)
                scene.addItem(textItem)
                scene.addItem(Point_support(int(x_sup[0]), int(x_sup[1])))
                self.listWidget_2.update()
                x2 = int(x_sup[0])
                y2 = int(x_sup[1])

                scene.addLine(x1, y1, x2, y2, pen=QPen(QColor(138, 127, 142)))
                scene.addItem(Point(int(y[0]), int(y[1])))
                textItem = QGraphicsTextItem(str(i))
                textItem.setPos(int(y[0])+2, int(y[1])+2)
                scene.addItem(textItem)

            self.graphicsView.setScene(scene)
            self.graphicsView.update()
            scene = self.graphicsView.scene()

            if len(self.list_point) > 1:
                line = self.Bezier.get_line()
                for x in range(line.shape[0] - 1):
                    scene.addLine(QLineF(line[x, 0], line[x, 1], line[x+1, 0], line[x+1, 1]))

            self.graphicsView.setScene(scene)
            self.graphicsView.update()
            self.refresh()
            self.refresh_support()
        except AttributeError:
            pass

    def change(self):
        try:
            self.change = True
            current = self.listWidget.currentItem().text()
            current = current.replace(",", " ")
            lst = current.split()
            self.point_for_cahnge = (lst[0], lst[1])
        except AttributeError:
            pass

    def change_support(self):
        try:
            self.bool_change_support = True
            current = self.listWidget_2.currentItem().text()
            current = current.replace(",", " ")
            lst = current.split()
            self.support_point_for_cahnge = (lst[0], lst[1])
        except AttributeError:
            pass

    def mousePressEvent(self, event):
        try:
            if self.bool_change_support:
                if event.button() == QtCore.Qt.LeftButton:
                    ind = self.list_support_point.index("[" + self.support_point_for_cahnge[0] + ", " + self.support_point_for_cahnge[1] + "]")
                    self.changeSel_support((event.x(), event.y()), ind)
                    if ind & 1 == 0:   # Если меняем левую
                        result_update = self.Bezier.update_support_point(ind // 2, ind & 1 == 0, (event.x(), event.y()))
                        self.list_support_point[ind+1] = "[" + str(result_update[0]) + ", " + str(result_update[1]) + "]"
                        self.list_support_point[ind] = "[" + str(event.x()) + ", " + str(event.y()) + "]"
                    else:
                        result_update = self.Bezier.update_support_point(ind // 2, ind & 1 == 0, (event.x(), event.y()))
                        self.list_support_point[ind] = "[" + str(event.x()) + ", " + str(event.y()) + "]"
                        self.list_support_point[ind-1] = "[" + str(result_update[0]) + ", " + str(result_update[1]) + "]"

                    scene = QGraphicsScene(0, 0, 1400, 1000)
                    for i in range(len(self.list_point)):
                        x = self.list_point[i]
                        x = x.replace("[", "")
                        x = x.replace("]", "")
                        x = x.replace(",", " ")
                        y = x.split()

                        x_sup = self.list_support_point[i*2]
                        x_sup = x_sup.replace("[", "").replace("]", "").replace(",", " ").split()
                        textItem = QGraphicsTextItem(str(i*2))
                        textItem.setPos(int(x_sup[0]) + 5, int(x_sup[1]) + 5)
                        scene.addItem(textItem)
                        scene.addItem(Point_support(int(x_sup[0]), int(x_sup[1])))
                        x1 = int(x_sup[0])
                        y1 = int(x_sup[1])

                        x_sup = self.list_support_point[i*2+1]
                        x_sup = x_sup.replace("[", "").replace("]", "").replace(",", " ").split()
                        textItem = QGraphicsTextItem(str(i*2+1))
                        textItem.setPos(int(x_sup[0]) + 5, int(x_sup[1]) + 5)
                        scene.addItem(textItem)
                        scene.addItem(Point_support(int(x_sup[0]), int(x_sup[1])))
                        x2 = int(x_sup[0])
                        y2 = int(x_sup[1])
                        self.listWidget_2.update()

                        scene.addLine(x1, y1, x2, y2, pen=QPen(QColor(138, 127, 142)))

                        scene.addItem(Point(int(y[0]), int(y[1])))
                        textItem = QGraphicsTextItem(str(i))
                        textItem.setPos(int(y[0]) + 5, int(y[1]) + 5)
                        scene.addItem(textItem)

                    self.graphicsView.setScene(scene)
                    self.graphicsView.update()
                    scene = self.graphicsView.scene()

                    if len(self.list_point) > 1:
                        line = self.Bezier.get_line()
                        for x in range(len(line) - 1):
                            scene.addLine(QLineF(line[x][0], line[x][1], line[x + 1][0], line[x + 1][1]))
                    self.graphicsView.setScene(scene)
                    self.bool_change_support = False
                    self.support_point_for_cahnge = None
                self.refresh_support()
            else:
                if not self.change:
                    if event.button() == QtCore.Qt.LeftButton:
                        ((x1, y1), (x2, y2)) = self.Bezier.add_point((event.x(), event.y()))
                        scene = self.graphicsView.scene()
                        scene.addItem(Point(event.x(), event.y()))
                        scene.addItem(Point_support(x1, y1))
                        scene.addItem(Point_support(x2, y2))

                        textItem = QGraphicsTextItem(str(len(self.list_point)))
                        textItem.setPos(event.x()+5, event.y()+5)
                        scene.addItem(textItem)

                        self.listWidget.addItem(str(event.x()) + "," + str(event.y())+" "+str(len(self.list_point)))
                        self.listWidget.update()

                        textItem = QGraphicsTextItem(str(len(self.list_support_point)))
                        textItem.setPos(x1 + 5, y1 + 5)
                        scene.addItem(textItem)
                        self.listWidget_2.addItem(str(x1) + "," + str(y1) + " " + str(len(self.list_support_point)))
                        self.list_support_point.append(str([x1, y1]))

                        textItem = QGraphicsTextItem(str(len(self.list_support_point)))
                        textItem.setPos(x2 + 5, y2 + 5)
                        scene.addItem(textItem)
                        self.listWidget_2.addItem(str(x2) + "," + str(y2) + " " + str(len(self.list_support_point)))
                        self.list_support_point.append(str([x2, y2]))
                        self.listWidget_2.update()

                        scene.addLine(x1, y1, x2, y2, pen=QPen(QColor(138, 127, 142)))

                        self.graphicsView.update()
                        self.list_point.append(str([event.x(), event.y()]))
                        self.list_object.append(Point(event.x(), event.y()))
                        if len(self.list_point) > 1:
                            line = self.Bezier.get_line()
                            for x in range(len(line) - 1):
                                scene.addLine(QLineF(line[x][0], line[x][1], line[x + 1][0], line[x + 1][1]))

                        self.graphicsView.setScene(scene)
                        self.refresh_support()
                else:
                    if event.button() == QtCore.Qt.LeftButton:
                        ind = self.list_point.index("[" + self.point_for_cahnge[0] + ", " + self.point_for_cahnge[1] + "]")
                        self.changeSel((event.x(), event.y()), ind)
                        result = self.Bezier.update_point(ind, (event.x(), event.y()))
                        self.list_support_point[ind*2] = "[" + str(result[0][0]) + ", " + str(result[0][1]) + "]"
                        self.list_support_point[ind*2+1] = "[" + str(result[1][0]) + ", " + str(result[1][1]) + "]"
                        self.list_point[ind] = "[" + str(event.x()) + ", " + str(event.y()) + "]"
                        scene = QGraphicsScene(0, 0, 1400, 1000)
                        for i in range(len(self.list_point)):
                            x = self.list_point[i]
                            x = x.replace("[", "")
                            x = x.replace("]", "")
                            x = x.replace(",", " ")
                            y = x.split()


                            x_sup = self.list_support_point[i * 2]
                            x_sup = x_sup.replace("[", "").replace("]", "").replace(",", " ").split()
                            textItem = QGraphicsTextItem(str(i * 2))
                            textItem.setPos(int(x_sup[0]) + 5, int(x_sup[1]) + 5)
                            scene.addItem(textItem)
                            scene.addItem(Point_support(int(x_sup[0]), int(x_sup[1])))
                            x1 = int(x_sup[0])
                            y1 = int(x_sup[1])

                            x_sup = self.list_support_point[i * 2 + 1]
                            x_sup = x_sup.replace("[", "").replace("]", "").replace(",", " ").split()
                            textItem = QGraphicsTextItem(str(i * 2 + 1))
                            textItem.setPos(int(x_sup[0]) + 5, int(x_sup[1]) + 5)
                            scene.addItem(textItem)
                            scene.addItem(Point_support(int(x_sup[0]), int(x_sup[1])))
                            x2 = int(x_sup[0])
                            y2 = int(x_sup[1])
                            self.listWidget_2.update()

                            scene.addLine(x1, y1, x2, y2, pen=QPen(QColor(138, 127, 142)))

                            scene.addItem(Point(int(y[0]), int(y[1])))
                            textItem = QGraphicsTextItem(str(i))
                            textItem.setPos(int(y[0]) + 5, int(y[1]) + 5)
                            scene.addItem(textItem)

                        self.graphicsView.setScene(scene)
                        self.graphicsView.update()
                        scene = self.graphicsView.scene()

                        if len(self.list_point) > 1:
                            line = self.Bezier.get_line()
                            for x in range(len(line) - 1):
                                scene.addLine(QLineF(line[x][0], line[x][1], line[x + 1][0], line[x + 1][1]))
                        self.graphicsView.setScene(scene)
                        self.graphicsView.update()
                        self.change = False
                        self.point_for_cahnge = None
                        self.refresh_support()
        except AttributeError:
            pass
        except TypeError:
            pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.setFixedSize(1600, 1000)
    window.show()
    sys.exit(app.exec_())

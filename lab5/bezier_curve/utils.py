import numpy as np
import sys
sys.path.append("./../")
sys.path.append("./../../")
sys.path.append("./../../../")
from lab4.pyFiles.affine_transfromation import rotation, point_position


def coef_init(size: int):
    t: np.ndarray = np.divide(np.arange(start=1, stop=size, step=1), float(size))
    t: np.ndarray = np.array([t, t])
    t_2: np.ndarray = np.power(t, 2)
    t_3: np.ndarray = np.power(t, 3)

    reverse_t: np.ndarray = np.flip(t, axis=-1)
    reverse_t_2: np.ndarray = np.power(reverse_t, 2)
    reverse_t_3: np.ndarray = np.power(reverse_t, 3)

    return [reverse_t_3.transpose(), np.multiply(np.multiply(3, reverse_t_2), t).transpose(),
                                       np.multiply(np.multiply(3, reverse_t), t_2).transpose(), t_3.transpose()]


def line_cos(line1_start: tuple, line1_end: tuple, line2_start: tuple, line2_end: tuple):
    upper = (line1_end[0] - line1_start[0]) * (line2_end[0] - line2_start[0]) + (line1_end[1] - line1_start[1]) * (
                line2_end[1] - line2_start[1])
    lower = np.sqrt(np.power(line1_end[0] - line1_start[0], 2) + np.power(line1_end[1] - line1_start[1], 2)) * np.sqrt(
        np.power(line2_end[0] - line2_start[0], 2) + np.power(line2_end[1] - line2_start[1], 2)
    )
    return upper / lower


def rotate_around_point(support_point: tuple, cos_value: float, point: tuple, sign: int):
    angle = np.arccos([cos_value]) * sign
    return rotation(np.array([support_point]), angle, point)[0]


def rotation_dir(line: list, point: tuple):
    if point_position(line, point) > 0:
        return -1
    else:
        return 1


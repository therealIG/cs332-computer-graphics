from diamond_logic import diamond_square
from matplotlib.widgets import Slider, Button
from matplotlib import pyplot as plt
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
#  needed to be run from console with /*python3 diamond_interface.py*/ due to interactive interface


def update(val):
    global ax, iterations, current_it
    current_it = 0
    size = 2**(int(size_slider.val))
    roughness = int(roughness_slider.val)
    iterations = diamond_square(size, roughness)
    x = np.arange(0, size)
    y = np.arange(0, size)
    X, Y = np.meshgrid(x, y)
    ax.clear()
    ax.plot_surface(X, Y, iterations[current_it], cmap=cm.coolwarm, linewidth=0, antialiased=False)
    current_it += 1
    plt.draw()


def next_step(event):
    global ax, current_it, iterations
    if current_it >= len(iterations):
        print('can\'t move forward:', current_it, ' is final step')
        return
    size = 2 ** (int(size_slider.val))
    x = np.arange(0, size)
    y = np.arange(0, size)
    X, Y = np.meshgrid(x, y)
    ax.clear()
    ax.plot_surface(X, Y, iterations[current_it], cmap=cm.coolwarm, linewidth=0, antialiased=False)
    current_it += 1
    plt.draw()


def final_step(event):
    global ax, current_it, iterations
    ax.clear()
    size = 2 ** (int(size_slider.val))
    x = np.arange(0, size)
    y = np.arange(0, size)
    X, Y = np.meshgrid(x, y)
    ax.plot_surface(X, Y, iterations[-1], cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.draw()


fig = plt.subplots(1, 1)
plt.subplots_adjust(left=0.1, bottom=0.2)
plt.subplot(1, 1, 1)
ax = fig[0].gca(projection='3d')

size_axe = plt.axes([0.15, 0.08, 0.7, 0.025])
roughness_axe = plt.axes([0.15, 0.12, 0.7, 0.025])
size_slider = Slider(size_axe, 'size', 1, 10, 3, valstep=1)
roughness_slider = Slider(roughness_axe, 'roughness', 0, 1000, 200, valstep=1)
next_it = plt.axes([0.15, 0.01, 0.15, 0.04])
final_it = plt.axes([0.45, 0.01, 0.15, 0.04])
button_next = Button(next_it, 'Get next step',  color='lightgoldenrodyellow', hovercolor='0.975')
button_final = Button(final_it, 'Get final step', color='lightgoldenrodyellow', hovercolor='0.975')
button_next.on_clicked(next_step)
button_final.on_clicked(final_step)


size_slider.on_changed(update)
roughness_slider.on_changed(update)

current_it = 0
iterations = []

plt.show()
import random
from collections import deque
import numpy as np


def diamond_square(size: int, roughness: int):
    """
    implements diamond square with midpoints displacement algorithm (task2)
    :param size: recursion depth
    :param roughness: limits of random increasing
    :return: list[heightmap_0, ... , heightmap_(size-1)]
    """
    print('got new params, calculating ...')

    heightmap = np.array([[0] * size for i in range(size)])

    heightmap[0][0] = random.randint(-roughness, roughness)
    heightmap[size - 1][0] = random.randint(-roughness, roughness)
    heightmap[0][size - 1] = random.randint(-roughness, roughness)
    heightmap[size - 1][size - 1] = random.randint(-roughness, roughness)

    q = deque()
    q.append((0, 0, size - 1, size - 1, roughness))
    iterative = []

    iterative.append(heightmap.copy())

    while len(q) != 0:
        top, left, bottom, right, _roughness = q.popleft()

        centerX = (left + right) // 2
        centerY = (top + bottom) // 2

        heightmap[centerX][top] = (heightmap[left][top] + heightmap[right][top]) // 2 + random.randint(-_roughness,
                                                                                                       _roughness)
        heightmap[centerX][bottom] = (heightmap[left][bottom] + heightmap[right][bottom]) // 2 + random.randint(
            -_roughness, _roughness)
        heightmap[left][centerY] = (heightmap[left][top] + heightmap[left][bottom]) // 2 + random.randint(-_roughness,
                                                                                                          _roughness)
        heightmap[right][centerY] = (heightmap[right][top] + heightmap[right][bottom]) // 2 + random.randint(
            -_roughness, _roughness)

        heightmap[centerX][centerY] = (heightmap[left][top] +
                                       heightmap[right][top] +
                                       heightmap[left][bottom] +
                                       heightmap[right][bottom]) // 4 + random.randint(-_roughness, _roughness)

        if right - left > 2:
            q.append((top, left, centerY, centerX, _roughness // 2))
            q.append((top, centerX, centerY, right, _roughness // 2))
            q.append((centerY, left, bottom, centerX, _roughness // 2))
            q.append((centerY, centerX, bottom, right, _roughness // 2))
        iterative.append(heightmap.copy())
    print('calculated!')
    return iterative

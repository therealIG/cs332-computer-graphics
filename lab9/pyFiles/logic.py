import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from lab8.pyFiles.camera import Camera


def two_var_func():
    X = np.arange(-5, 5, 0.25)
    Y = np.arange(-5, 5, 0.25)
    X, Y = np.meshgrid(X, Y)
    R = np.sqrt(X ** 2 + Y ** 2)
    Z = np.sin(R)
    result = np.array([X, Y, Z])
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    plt.show()
    return result.reshape((result.shape[1], result.shape[2], result.shape[0]))


def main():
    eye_position = np.array([0, 0, 0], dtype=float)
    target_position = np.array([0.4, 0, 0.4], dtype=float)
    projection_mat = np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ], dtype=float)

    camera = Camera(eye_position, target_position, projection_mat)
    print(camera)
    lookat_matrix = camera.lookat()
    pts = two_var_func()
    prepared_pts = np.concatenate([pts, np.ones(shape=(pts.shape[0], pts.shape[1], 1), dtype=float)], axis=-1)
    pts_after_view_translation = np.dot(prepared_pts, lookat_matrix.T)[:, :, :2]
    X = pts_after_view_translation[:, :, 0]
    Y = pts_after_view_translation[:, :, 1]
    plt.scatter(X, Y)
    plt.show()



if __name__ == "__main__":
    main()

import sys

import cv2
import matplotlib
import datetime
import h5py

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from lab6.pyFiles.affine_transform import *
from lab6.pyFiles.shapes.shapes import *
from lab6.pyFiles.shapes.shapes import Hexahedron

from lab8.pyFiles.camera import *
from lab8.pyFiles.utils import *
from lab8.pyFiles.simpleRender.graphic_utils import shape_render, create_empty_scene
from lab8.main import *

import re
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.collections import PolyCollection
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from lab6.pyFiles.projection import isometric_projection, orto_projection

matplotlib.use('Qt5Agg')


class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        QWidget.__init__(self)
        layout = QGridLayout()
        self.setLayout(layout)
        self.data = None
        self.setStyleSheet("background-color: gray;")
        layout.setSpacing(10)

        # self.flag_for_display = 2

        self.with_cnf = False
        self.with_z_buffer = False
        self.with_light = False
        self.coordinate_non_face = None

        """
        ----------------------------------------------------------------------
        --------- Размеры сцены; положение камеры ----------------------------
        ----------------------------------------------------------------------
        """

        self.scene_center_x = 950
        self.scene_center_y = 500
        self.scene_center_z = 0

        camera_x = self.scene_center_x
        camera_y = self.scene_center_y
        camera_z = self.scene_center_z + 500

        self.coordinate_camera = np.array([camera_x, camera_y, camera_z], dtype=float)
        self.coordinate_target = np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z], dtype=float)

        self.matrix_shift_xy = np.array([
            [1, 0, 0, self.scene_center_x],
            [0, 1, 0, self.scene_center_y],
            [0, 0, 1, self.scene_center_z],
            [0, 0, 0, 1]
        ])

        self.window_width = self.scene_center_x * 2
        self.window_height = self.scene_center_y * 2

        self.camera = Camera(self.coordinate_camera, self.coordinate_target, default_orthographic_projection())

        self.type_figure = None
        self.value_for_scale = [1 / 4, 1 / 3, 1 / 2, 1, 2, 3, 4, 5]

        self.type_projection = "orto_projection"
        self.set_figures = []  # список имен типов всех фигур на сцене
        self.set_dates = []  # список всех фигур на сцене
        self.default_light_dir = np.array([0, 1, 0])

        self.log_file = "logs"
        self.log_line_number = 1
        with open(self.log_file, "a") as log_file:
            log_file.write("\n------------------------------\n------------------------------\nNew session:\n{0}".format(
                datetime.datetime.now()))

        self._handling_type_info = "INFO"
        self._handling_type_warning = "WARNING"
        self._handling_type_error = "ERROR"
        self._open_handling_type = [self._handling_type_info,
                                    self._handling_type_warning]  # Выводит в консоль только эти категории, печатает в файл все
        self.logger(
            log_info="Scene size: Width = {0:>5}; Height = {1:>5}".format(self.window_width, self.window_height),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(camera_x, camera_y, camera_z),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Target position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(self.scene_center_x,
                                                                                  self.scene_center_y,
                                                                                  self.scene_center_z),
            handling_type=self._handling_type_info
        )
        # -------------------------------------------------------------------

        # --------------------------------------------------------------------------

        # Canvas
        self.figure = QGraphicsView()
        self.figure.setMinimumSize(900, 900)
        layout.addWidget(self.figure, 0, 2, 34, 9)
        # --------------------------------------------------------------------------
        self.type_projection = None
        self.corner_first = 0
        self.corner_second = 0
        self.axis = 0

        # Определение слайдеров положения камеры
        self.label_camera_position = QLabel(" Camera Position X,Y,Z ")
        font = self.label_camera_position.font()
        font.setPointSize(10)
        self.label_camera_position.setFont(font)
        layout.addWidget(self.label_camera_position, 20, 0)
        # X
        self.slider_position_x = QSlider(Qt.Horizontal)
        self.slider_position_x.setFixedSize(300, 20)
        self.slider_position_x.setRange(-1000, 2000)
        self.slider_position_x.setValue(camera_x)
        self.slider_position_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_x.setTickInterval(100)
        self.slider_position_x.setSingleStep(1)
        layout.addWidget(self.slider_position_x, 21, 0)
        self.label_position_value_x = QLabel(str(self.slider_position_x.value()))
        layout.addWidget(self.label_position_value_x, 21, 1)
        self.slider_position_x.valueChanged.connect(self.update_slider_position_x)
        self.label_position_value_x.setFixedSize(27, 10)

        # Y
        self.slider_position_y = QSlider(Qt.Horizontal)
        self.slider_position_y.setFixedSize(300, 20)
        self.slider_position_y.setRange(-1000, 2000)
        self.slider_position_y.setValue(camera_y)
        self.slider_position_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_y.setTickInterval(100)
        self.slider_position_y.setSingleStep(1)
        layout.addWidget(self.slider_position_y, 22, 0)
        self.label_position_value_y = QLabel(str(self.slider_position_y.value()))
        layout.addWidget(self.label_position_value_y, 22, 1)
        self.slider_position_y.valueChanged.connect(self.update_slider_position_y)
        self.label_position_value_y.setFixedSize(27, 10)

        # Z
        self.slider_position_z = QSlider(Qt.Horizontal)
        self.slider_position_z.setFixedSize(300, 20)
        self.slider_position_z.setRange(-1000, 2000)
        self.slider_position_z.setValue(camera_z)
        self.slider_position_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_z.setTickInterval(100)
        self.slider_position_z.setSingleStep(1)
        layout.addWidget(self.slider_position_z, 23, 0)
        self.label_position_value_z = QLabel(str(self.slider_position_z.value()))
        layout.addWidget(self.label_position_value_z, 23, 1)
        self.slider_position_z.valueChanged.connect(self.update_slider_position_z)
        self.label_position_value_z.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Определение слайдеров положения цели
        self.label_camera_target = QLabel(" Camera Target X,Y,Z ")
        self.label_camera_target.setFont(font)
        layout.addWidget(self.label_camera_target, 24, 0)
        # X
        self.slider_position_target_x = QSlider(Qt.Horizontal)
        self.slider_position_target_x.setFixedSize(300, 20)
        self.slider_position_target_x.setRange(-1000, 2000)
        self.slider_position_target_x.setValue(self.scene_center_x)
        self.slider_position_target_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_x.setTickInterval(100)
        self.slider_position_target_x.setSingleStep(1)
        layout.addWidget(self.slider_position_target_x, 25, 0)
        self.label_position_target_value_x = QLabel(str(self.slider_position_target_x.value()))
        layout.addWidget(self.label_position_target_value_x, 25, 1)
        self.slider_position_target_x.valueChanged.connect(self.update_slider_position_target_x)
        self.label_position_target_value_x.setFixedSize(27, 10)

        # Y
        self.slider_position_target_y = QSlider(Qt.Horizontal)
        self.slider_position_target_y.setFixedSize(300, 20)
        self.slider_position_target_y.setRange(-1000, 2000)
        self.slider_position_target_y.setValue(self.scene_center_y)
        self.slider_position_target_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_y.setTickInterval(100)
        self.slider_position_target_y.setSingleStep(1)
        layout.addWidget(self.slider_position_target_y, 26, 0)
        self.label_position_target_value_y = QLabel(str(self.slider_position_target_y.value()))
        layout.addWidget(self.label_position_target_value_y, 26, 1)
        self.slider_position_target_y.valueChanged.connect(self.update_slider_position_target_y)
        self.label_position_target_value_y.setFixedSize(27, 10)

        # Z
        self.slider_position_target_z = QSlider(Qt.Horizontal)
        self.slider_position_target_z.setFixedSize(300, 20)
        self.slider_position_target_z.setRange(-1000, 2000)
        self.slider_position_target_z.setValue(self.scene_center_z)
        self.slider_position_target_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_position_target_z.setTickInterval(100)
        self.slider_position_target_z.setSingleStep(10)
        layout.addWidget(self.slider_position_target_z, 27, 0)
        self.label_position_target_value_z = QLabel(str(self.slider_position_target_z.value()))
        layout.addWidget(self.label_position_target_value_z, 27, 1)
        self.slider_position_target_z.valueChanged.connect(self.update_slider_position_target_z)
        self.label_position_target_value_z.setFixedSize(27, 10)

        # --------------------------------------------------------------------------

    def plot(self):
        """
        Отрисовка изображения в 2D
        :return: None
        """
        scene, z_buffer = create_empty_scene(height=self.window_height, width=self.window_width)
        lookat_matrix = self.camera.lookat()
        scene = floating_horizon(scene, self.matrix_shift_xy, lookat_matrix)


        graphic_scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(cv2.cvtColor(scene.astype(np.uint8), cv2.COLOR_BGR2RGB), self.window_width, self.window_height,
                   QImage.Format_RGB888))
        graphic_scene.addPixmap(pixmap1)
        self.figure.setScene(graphic_scene)

    def logger(self, log_info: str, handling_type: str = "UNKNOWN"):
        # ------------------------------------------------------------------------
        # ------------------------- LOGGER ---------------------------------------

        for line in log_info.split("\n"):
            current_line = "[{0}]::[{1}]:: {2}".format(self.log_line_number, handling_type, line)

            if handling_type in self._open_handling_type:
                print(current_line)

            with open(self.log_file, "a") as log_file:
                log_file.write(current_line + "\n")
            self.log_line_number += 1

        # ------------------------------------------------------------------------

    def update_slider_position_x(self):
        self.label_position_value_x.setText(str(self.slider_position_x.value()))
        previous_coordinate = self.coordinate_camera
        self.coordinate_camera[0] = self.slider_position_x.value()
        self.logger(
            log_info="Update slider position X, update camera position.\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_camera[0], self.coordinate_camera[1], self.coordinate_camera[2]),
            handling_type=self._handling_type_info
        )
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_y(self):
        self.label_position_value_y.setText(str(self.slider_position_y.value()))
        previous_coordinate = self.coordinate_camera
        self.coordinate_camera[1] = self.slider_position_y.value()
        self.logger(
            log_info="Update slider position Y, update camera position.\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_camera[0], self.coordinate_camera[1], self.coordinate_camera[2]),
            handling_type=self._handling_type_info
        )
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_z(self):
        self.label_position_value_z.setText(str(self.slider_position_z.value()))
        previous_coordinate = self.coordinate_camera
        self.coordinate_camera[2] = self.slider_position_z.value()
        self.logger(
            log_info="Update slider position Z, update camera position.\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_camera[0], self.coordinate_camera[1], self.coordinate_camera[2]),
            handling_type=self._handling_type_info
        )
        self.camera.eye_update(self.coordinate_camera)
        self.plot()

    def update_slider_position_target_x(self):
        self.label_position_target_value_x.setText(str(self.slider_position_target_x.value()))
        previous_coordinate = self.coordinate_target
        self.coordinate_target[0] = self.slider_position_target_x.value()
        self.logger(
            log_info="Update slider position target X\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_target[0], self.coordinate_target[1], self.coordinate_target[2]),
            handling_type=self._handling_type_info
        )
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_slider_position_target_y(self):
        self.label_position_target_value_y.setText(str(self.slider_position_target_y.value()))
        previous_coordinate = self.coordinate_target
        self.coordinate_target[1] = self.slider_position_target_x.value()
        self.logger(
            log_info="Update slider position target Y\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_target[0], self.coordinate_target[1], self.coordinate_target[2]),
            handling_type=self._handling_type_info
        )
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_slider_position_target_z(self):
        self.label_position_target_value_z.setText(str(self.slider_position_target_z.value()))
        previous_coordinate = self.coordinate_target
        self.coordinate_target[2] = self.slider_position_target_x.value()
        self.logger(
            log_info="Update slider position target Z\nPrevious camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                previous_coordinate[0], previous_coordinate[1], previous_coordinate[2]),
            handling_type=self._handling_type_info
        )
        self.logger(
            log_info="Current camera position: X = {0:>4}; Y = {1:>4}; Z = {2:>4}".format(
                self.coordinate_target[0], self.coordinate_target[1], self.coordinate_target[2]),
            handling_type=self._handling_type_info
        )
        self.camera.target_update(self.coordinate_target)
        self.plot()

    def update_add_figure(self):
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', '../data/task3/')[0]
            with h5py.File(fname, 'r') as hf:
                fname = fname.split("/")[-1].split(".")[0]
                if fname == "Dodecahedron":
                    self.type_figure = "Dodecahedron"
                    cube = Dodecahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]),
                                        100.0)
                if fname == "Hexahedron":
                    self.type_figure = "Hexahedron"
                    cube = Hexahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                if fname == "Icocahedron":
                    self.type_figure = "Icocahedron"
                    cube = Icosahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                if fname == "Octahedron":
                    self.type_figure = "Octahedron"
                    cube = Octahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                if fname == "Tetrahedron":
                    self.type_figure = "Tetrahedron"
                    cube = Tetrahedron(np.array([self.scene_center_y, self.scene_center_x, self.scene_center_z]), 100.0)
                self.logger(
                    log_info="Update add figure. Adding new shape to scene.\nShape:{0}".format(cube),
                    handling_type=self._handling_type_info
                )
                self.list_figures.addItem(fname)
                self.set_figures.append(fname)
                self.set_dates.append(cube)

                self.value_turn_x.append(0)
                self.value_turn_y.append(0)
                self.value_turn_z.append(0)

                self.value_shift_x.append(0)
                self.value_shift_y.append(0)
                self.value_shift_z.append(0)

                self.value_scale.append(1)
                self.value_scale_x.append(1)
                self.value_scale_y.append(1)
                self.value_scale_z.append(1)

                self.slider_shift_x.setValue(0)
                self.slider_shift_y.setValue(0)
                self.slider_shift_z.setValue(0)

                self.slider_turn_x.setValue(0)
                self.slider_turn_y.setValue(0)
                self.slider_turn_z.setValue(0)

                self.slider_scale.setValue(3)
                self.slider_scale_x.setValue(3)
                self.slider_scale_y.setValue(3)
                self.slider_scale_z.setValue(3)

                self.plot()
        except FileNotFoundError:
            pass
        except ValueError:
            pass

    def update_check_box(self, state):
        with_cnf_prev_state = self.with_cnf
        with_z_buffer_prev_state = self.with_z_buffer

        self.with_cnf = self.check_box_cnf.isChecked()
        self.with_z_buffer = self.check_box_z.isChecked()

        self.logger(
            log_info="Render method changed\nWith CNF: {0}\nWith Z-Buffer: {1}".format(self.with_cnf,
                                                                                       self.with_z_buffer),
            handling_type=self._handling_type_info
        )

        if not with_cnf_prev_state and self.with_cnf:
            self.coordinate_non_face = self.camera.eye()
            # self.coordinate_non_face = np.expand_dims(self.coordinate_non_face, axis=0)
            # self.coordinate_non_face = np.expand_dims(self.coordinate_non_face, axis=0)
            # self.coordinate_non_face = to4d(self.coordinate_non_face)
            # self.coordinate_non_face = np.dot(self.coordinate_non_face, np.transpose(self.matrix_shift_xy))[0, 0, :3]

        elif with_cnf_prev_state and not self.with_cnf:
            self.coordinate_non_face = None
        self.logger(
            log_info="Current coordinate of overview for CNF: {}".format(self.coordinate_non_face),
            handling_type=self._handling_type_info
        )
        # ^ is XOR
        render_restart = (with_z_buffer_prev_state ^ self.with_z_buffer) or (
                with_cnf_prev_state ^ self.with_cnf and not self.with_z_buffer)
        if render_restart:
            self.plot()

    def update_light(self, state):
        self.with_light = self.check_box_light_on.isChecked()

        if self.with_light:
            self.logger(
                log_info="Light ON",
                handling_type=self._handling_type_info
            )

        if not self.with_light:
            self.logger(
                log_info="Light OFF",
                handling_type=self._handling_type_info
            )

        if self.with_z_buffer:
            self.plot()

    def update_check_box_plot(self):
        pass


app = QApplication(sys.argv)
screen = Window()
screen.setGeometry(0, 0, 900, 900)
screen.show()
sys.exit(app.exec_())

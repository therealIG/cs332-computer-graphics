import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene
from PyQt5 import uic, QtCore
from PyQt5.QtGui import QImage, QPixmap
import cv2 as cv
import numpy as np


def pow2(x):
    return np.power(x, 2)


qtCreatorFile = 'plotter.ui'

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.setFixedSize(800, 663)
        self.range_from.setText('-10')
        self.range_to.setText('10')
        self.button_plot.clicked.connect(self.visualize)
        self.scene = QGraphicsScene()
        self.view.setScene(self.scene)
        self.view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.view.setFrameStyle(0)

    def fix_range(self, range_from, range_to):
        self.statusBar().showMessage('unavailable range! Setting to default value')
        self.range_from.setText(str(range_from))
        self.range_to.setText(str(range_to))
        self.range_from.update()
        self.range_to.update()

    def plot(self, func, range_from, range_to):
        height = self.view.viewport().size().height()
        width = self.view.viewport().size().width()
        x_step = (range_to - range_from) / width
        x_vals = np.arange(range_from, range_to, x_step)
        y_vals = np.array([func(x) for x in x_vals])
        y_step = height / (max(y_vals) - min(y_vals))
        x_vals = 1/x_step * x_vals
        y_vals = y_step * y_vals
        x_vals += -x_vals[0]
        y_vals -= max(y_vals)
        y_vals *= -1
        image = 255 * np.ones(shape=[height, width, 1], dtype=np.uint8)
        for i in range(len(x_vals)-1):
            cv.line(image, (int(x_vals[i]), int(y_vals[i])), (int(x_vals[i+1]), int(y_vals[i+1])), 0)
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        image = QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(image)
        return pixmap

    def visualize(self):
        self.statusBar().clearMessage()
        try:
            range_from = float(self.range_from.toPlainText())
            range_to = float(self.range_to.toPlainText())
        except ValueError:
            range_from = -10
            range_to = 10
            self.fix_range(range_from, range_to)

        if range_to <= range_from:
            range_from = -10
            range_to = 10
            self.fix_range(range_from, range_to)
        try:
            function_name = self.list_functions.currentItem().text()
        except AttributeError:
            self.statusBar().showMessage('function isn\'t selected! Selecting default')
            function_name = 'sin(x)'

        if function_name == 'sin(x)':
            func = np.sin
        elif function_name == 'x^2':
            func = pow2
        elif function_name == 'cos(x)':
            func = np.cos
        elif function_name == 'abs(x)':
            func = np.abs
        elif function_name == 'arctan(x)':
            func = np.arctan
        elif function_name == 'tanh(x)':
            func = np.tanh

        pixmap = self.plot(func, range_from, range_to)
        self.scene.addPixmap(pixmap)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())

import sys
sys.path.append("./../../")
from lab4.pyFiles.utils import _preparing_points, np, _full_affine_mat, _center_of_polygon, point_position


def _shifting(x: int, y: int) -> np.ndarray:
    """
    Matrix for shifting center of coordinates (0, 0) to new point (x, y)
    :param x: new value X
    :param y: new value Y
    :return: np.ndarray
    """
    return np.array([
        [1, 0, 0],
        [0, 1, 0],
        [x, y, 1]
    ], dtype=np.float)


def translation(points: np.ndarray, shifts: list) -> np.ndarray:
    """
    Translation points by shifts
    :param points: np.ndarray with shape (N, 2) where N is number of points.
    :param shifts: list with 2 number: shifts on axis x, shifts on axis y.
    :return: np.ndarray, points after translation
    """
    return np.matmul(_preparing_points(points), _shifting(*shifts)).astype(np.float)[:, :2]


def rotation(points: np.ndarray, angle: float, pivot: tuple = None) -> np.ndarray:
    """
    Rotation for group of points by angle relative to pivot
    :param points: np.ndarray with shape (N, 2) where N is number of points.
    :param angle: angle of rotation in rad.
    :param pivot: center of rotation. If is None, average point will be used as.
    :return: np.ndarray, points after rotation
    """
    if pivot is None:
        pivot = _center_of_polygon(points)

    cs, sn = np.cos(angle), np.sin(angle)
    rotate_mat: np.ndarray = np.array([
        [cs, sn, 0],
        [-1*sn, cs, 0],
        [0, 0, 1]
    ], dtype=np.float)

    return np.matmul(_preparing_points(points), _full_affine_mat(rotate_mat, _shifting, pivot[0], pivot[1]))[:, :2]


def dilatation(points: np.ndarray, scale: tuple, pivot: tuple = None) -> np.ndarray:
    """
    dilatation for group of points.
    :param points: np.ndarray with shape (N, 2) where N is number of points.
    :param scale: scale[0] is value for dilatation on x, scale[1] is value for dilatation on y.
    :param pivot: center of dilatation. If is None, average point will be used as.
    :return: np.ndarray, points after dilatation
    """
    if pivot is None:
        pivot = _center_of_polygon(points)

    scale_mat: np.ndarray = np.array([
        [scale[0], 0, 0],
        [0, scale[1], 0],
        [0, 0, 1]
    ], dtype=np.float)

    return np.matmul(_preparing_points(points), _full_affine_mat(scale_mat, _shifting, pivot[0], pivot[1]))[:, :2]


import numpy as np


def point_position(line: list, point: list) -> int:
    """
    если yb·xa - xb·ya > 0 => point b слева от line Oa
    если yb·xa - xb·ya < 0 => point b справа от line Oa
    :param line: list, start point is first element, end point is second
    :param point: list of two int.
    :return: int
    """
    return (point[0] - line[0][0]) * (line[1][1] - line[0][1]) - (point[1] - line[0][1]) * (line[1][0] - line[0][0])


# Hey, nothing to do here! Go away! All these pretty functions work.
def _preparing_points(points: np.ndarray) -> np.ndarray:
    return np.concatenate((
        points,
        np.ones((points.shape[0], 1), dtype=np.int8)
    ), -1)


def _full_affine_mat(mat: np.ndarray, transposing: callable, x: int, y: int) -> np.ndarray:
    full_affine_mat: np.ndarray = np.matmul(transposing(-1*x, -1*y), mat)
    return np.matmul(full_affine_mat, transposing(x, y))


def _center_of_polygon(points: np.ndarray) -> tuple:
    x = np.average(points[:, 0])
    y = np.average(points[:, 1])

    return x, y


def point_inside_convex_polygon(polygon: list, point: tuple) -> bool:
    """
    checks, if point located inside convex polygon
    :param polygon: list like: [[x1, y1], [x2, y2], ... ]
    :param point: tuple like: (x, y)
    :return: bool
    """
    side_check = []
    for i in range(len(polygon) - 1):
        pt1 = polygon[i]
        pt2 = polygon[i + 1]
        line = [pt1, pt2]
        side_check.append(point_position(line, [point[0], point[1]]))

    inside = all(side < 0 for side in side_check)
    return inside


def point_inside_nonconvex_polygon(polygon: list, point: tuple) -> bool:
    """
    checks, if point located inside nonconvex polygon
    :param polygon: list like: [[x1, y1], [x2, y2], ... ]
    :param point: tuple like: (x, y)
    :return: bool
    """
    intersections = 0
    for i in range(len(polygon) - 1):
        pt1 = polygon[i]
        pt2 = polygon[i + 1]
        ax = pt1[0] - point[0]
        ay = pt1[1] - point[1]
        bx = pt2[0] - point[0]
        by = pt2[1] - point[1]
        if ay*by > 0:
            continue
        s = np.sign(ax * by - ay * bx)
        if s == 0:
            if ax*bx <= 0:
                intersections += 1
            continue
        if ay < 0:
            if -s == -1:
                intersections += 1
        if by < 0:
            if s == -1:
                intersections += 1
    return intersections % 2 != 0


def line_intersection(line1: list, line2: list):
    """
    finds point of line intersection
    :param line1: list like: [[x1, y1], [x2, y2]]
    :param line2: list like: [[x1, y1], [x2, y2]]
    :return: tuple (x, y) / (0, 0) if can't be sloved
    """

    # line1 = AB, line2 = CD
    a_pt, b_pt = line1[0], line1[1]
    c_pt, d_pt = line2[0], line2[1]
    a1 = a_pt[1] - b_pt[1]  # coefficient of line equation
    b1 = b_pt[0] - a_pt[0]  # coefficient of line equation
    c1 = a_pt[0]*b_pt[1] - b_pt[0]*a_pt[1]  # coefficient of line equation
    a2 = c_pt[1] - d_pt[1]  # coefficient of line equation
    b2 = d_pt[0] - c_pt[0]  # coefficient of line equation
    c2 = c_pt[0] * d_pt[1] - d_pt[0] * c_pt[1]  # coefficient of line equation

    if a1*b2 - a2*b1 == 0:
        return 0, 0

    x = (c1*b2 - c2*b1) / (a1*b2 - a2*b1)
    y = (a1*c2 - a2*c1) / (a1*b2 - a2*b1)
    return -x, -y





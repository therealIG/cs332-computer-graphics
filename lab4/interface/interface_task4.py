import sys
from PyQt5.QtCore import QPoint
from PyQt5.QtGui import QPixmap, QImage, QPen, QBrush
from PyQt5.QtWidgets import QMainWindow, QApplication, QGraphicsScene, QGraphicsLineItem, QWidget
from PyQt5 import QtCore, uic
from lab4.pyFiles.utils import *
from lab4.pyFiles.affine_transfromation import *
from math import *
qtCreatorFile = "design_task4.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


def setMouseMoveEventDelegate(setQWidget):
    def subWidgetMouseMoveEvent(eventQMouseEvent):
        QWidget.mouseMoveEvent(setQWidget, eventQMouseEvent)

    setQWidget.setMouseTracking(True)
    setQWidget.mouseMoveEvent = subWidgetMouseMoveEvent


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.clearButton.clicked.connect(self.clear)
        self.chooseButton.clicked.connect(self.apply)
        # CheckBox
        self.drawRect.toggled.connect(self.draw_rectangles)
        self.drawPoint.toggled.connect(self.draw_points)
        self.drawLine.toggled.connect(self.draw_lines)

        self.boolRect = False
        self.boolPoint = False
        self.boolLine = False
        #

        self.listPoint = []
        self.list_point_line = []
        self.list_point_second_line = []
        self.lastPoint = QPoint()
        self.firstPoint = QPoint()
        self.drawing = False
        blank_image2 = 255 * np.ones(shape=[600, 800, 3], dtype=np.uint8)
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(blank_image2, blank_image2.shape[1], blank_image2.shape[0], blank_image2.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        self.graphicsView.setSceneRect(0, 0, 800, 600)
        self.graphicsView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.graphicsView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setMouseTracking(True)
        self.textEdit.setText("0")
        self.textEdit_2.setText("0")
        self.textEdit_3.setText("0")
        setMouseMoveEventDelegate(self.graphicsView)

    def clear(self):
        clear_img = 255 * np.ones(shape=[600, 800, 3], dtype=np.uint8)
        scene = QGraphicsScene()
        pixmap1 = QPixmap.fromImage(
            QImage(clear_img, clear_img.shape[1], clear_img.shape[0], clear_img.strides[0],
                   QImage.Format_RGB888))
        scene.addPixmap(pixmap1)
        self.graphicsView.setScene(scene)
        self.graphicsView.viewport().update()
        self.lastPoint = QPoint()
        self.listPoint = []
        self.textBrowser.clear()
        self.list_point_second_line = []
        self.list_point_line = []

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            if self.boolPoint:
                self.point(event.pos())
                self.lastPoint = event.pos()
            if self.boolLine:
                if self.firstPoint == QPoint():
                    self.firstPoint = event.pos()
                else:
                    self.list_point_second_line = self.list_point_line.copy()
                    self.list_point_line = []
                    scene = self.graphicsView.scene()
                    scene.addItem(QGraphicsLineItem(QtCore.QLineF(self.firstPoint, event.pos())))
                    self.list_point_line.append([self.firstPoint.x(), self.firstPoint.y()])
                    self.list_point_line.append([event.x(), event.y()])
                    self.graphicsView.viewport().update()
                    self.firstPoint = QPoint()

    def mouseMoveEvent(self, event):
        if self.lastPoint == QPoint():
            self.lastPoint = event.pos()
            return
        if event.buttons() and QtCore.Qt.LeftButton and self.drawing:
            scene = self.graphicsView.scene()
            scene.addItem(QGraphicsLineItem(QtCore.QLineF(self.lastPoint, event.pos())))
            self.graphicsView.viewport().update()
            self.lastPoint = event.pos()
            if 800 >= event.x() > 0 and 600 >= event.y() > 0:
                self.listPoint.append([event.x(), event.y()])
            self.update()

    def point(self, position):
        self.graphicsView.scene().addEllipse(position.x() - 1, position.y() - 1, 1 * 2.0, 1 * 2.0, QPen(),
                                             QBrush(QtCore.Qt.SolidPattern))
        self.graphicsView.viewport().update()

    def draw_rectangles(self, state):
        if state:
            self.drawLine.setChecked(False)
            self.drawPoint.setChecked(False)
            self.boolRect = True
            self.drawing = True
        else:
            self.boolRect = False
            self.drawing = False

    def draw_lines(self, state):
        if state:
            self.drawRect.setChecked(False)
            self.drawPoint.setChecked(False)
            self.boolLine = True
        else:
            self.boolLine = False

    def draw_points(self, state):
        if state:
            self.drawLine.setChecked(False)
            self.drawRect.setChecked(False)
            self.boolPoint = True
        else:
            self.boolPoint = False

    def delete_line(self):
        scene = self.graphicsView.scene()
        MyPen = QPen(QtCore.Qt.white)
        ln = QGraphicsLineItem(
            QtCore.QLineF(self.list_point_line[0][0], self.list_point_line[0][1], self.list_point_line[1][0],
                          self.list_point_line[1][1]))
        ln.setPen(MyPen)
        scene.addItem(ln)

    def delete_rect(self):
        scene = self.graphicsView.scene()
        MyPen = QPen(QtCore.Qt.white)
        for x in range(len(self.listPoint)-1):
            ln = QGraphicsLineItem(
            QtCore.QLineF(self.listPoint[x][0], self.listPoint[x][1], self.listPoint[x+1][0],
                          self.listPoint[x+1][1]))
            ln.setPen(MyPen)
            scene.addItem(ln)

    def apply(self):
        action_name = self.listWidget.currentItem().text()

        if action_name == "Смещение полигона":
            result = translation(np.array(self.list_point_line), [int(self.textEdit_2.toPlainText()), int(self.textEdit_3.toPlainText())])
            scene = self.graphicsView.scene()
            # self.delete_rect()
            for x in range(len(result) - 1):
                scene.addItem(
                    QGraphicsLineItem(QtCore.QLineF(result[x][0], result[x][1], result[x + 1][0], result[x + 1][1])))
            self.graphicsView.viewport().update()
            self.listPoint = result

        elif action_name == "Поворот полигона вокруг произвольной точки":
            result = rotation(np.array(self.listPoint), radians(float(self.textEdit.toPlainText())), (self.lastPoint.x(), self.lastPoint.y()))
            scene = self.graphicsView.scene()
            self.delete_rect()
            for x in range(len(result) - 1):
                scene.addItem(
                    QGraphicsLineItem(QtCore.QLineF(result[x][0], result[x][1], result[x + 1][0], result[x + 1][1])))
            self.graphicsView.viewport().update()
            self.listPoint = result

        elif action_name == "Поворот полигона вокруг своего центра":
            result = rotation(np.array(self.listPoint), radians(float(self.textEdit.toPlainText())))#1.5708)
            scene = self.graphicsView.scene()
            self.delete_rect()
            for x in range(len(result) - 1):
                scene.addItem(
                    QGraphicsLineItem(QtCore.QLineF(result[x][0], result[x][1], result[x + 1][0], result[x + 1][1])))
            self.graphicsView.viewport().update()
            self.listPoint = result

        elif action_name == "Масштабирование полгона относительно произвольной точки":
            result = dilatation(np.array(self.listPoint), (0.5, 0.5), (self.lastPoint.x(), self.lastPoint.y()))
            scene = self.graphicsView.scene()
            self.delete_rect()
            for x in range(len(result) - 1):
                scene.addItem(
                    QGraphicsLineItem(QtCore.QLineF(result[x][0], result[x][1], result[x + 1][0], result[x + 1][1])))
            self.graphicsView.viewport().update()
            self.listPoint = result

        elif action_name == "Масштабирование полгона относительно своего центра":
            result = dilatation(np.array(self.listPoint), (0.5,  0.5))
            scene = self.graphicsView.scene()
            self.delete_rect()
            for x in range(len(result)-1):
                scene.addItem(QGraphicsLineItem(QtCore.QLineF(result[x][0], result[x][1], result[x+1][0], result[x+1][1])))
            self.graphicsView.viewport().update()
            self.listPoint = result

        elif action_name == "Поворот ребра на 90 градусов вокруг своего центра":
            result = rotation(np.array(self.list_point_line), 1.5708)
            scene = self.graphicsView.scene()
            self.delete_line()
            scene.addItem(QGraphicsLineItem(QtCore.QLineF(result[0][0], result[0][1], result[1][0], result[1][1])))
            self.graphicsView.viewport().update()
            self.list_point_line = result

        elif action_name == "Поиск точки пересечения двух ребер":
            x, y = line_intersection(self.list_point_line, self.list_point_second_line)
            if x < 0 or x > 800 or y < 0 or y > 600:
                self.textBrowser.append("Прямые не пересекаются")
            else:
                self.graphicsView.scene().addEllipse(x - 1, y - 1, 1 * 5.0, 1 * 5.0, QPen(QtCore.Qt.red),
                                                     QBrush(QtCore.Qt.SolidPattern))
                self.graphicsView.viewport().update()

        elif action_name == "Принадлежит ли точка выпуклому многоугольнику?":
            self.textBrowser.clear()
            location = point_inside_convex_polygon(self.listPoint, (self.lastPoint.x(), self.lastPoint.y()))
            if location:
                self.textBrowser.append("Точка принадлежит выпуклому многоугольнику")
            else:
                self.textBrowser.append("Точка не принадлежит выпуклому многоугольнику")

        elif action_name == "Принадлежит ли точка невыпуклому многоугольнику?":
            self.textBrowser.clear()
            location = point_inside_nonconvex_polygon(self.listPoint, (self.lastPoint.x(), self.lastPoint.y()))
            if location:
                self.textBrowser.append("Точка принадлежит невыпуклому многоугольнику")
            else:
                self.textBrowser.append("Точка не принадлежит невыпуклому многоугольнику")

        elif action_name == "Классифицировать положение точки относительно ребра":
            result = point_position(self.list_point_line, [self.lastPoint.x(), self.lastPoint.y()])
            if result < 0:
                self.textBrowser.append("точка находится справа от линии")
            else:
                self.textBrowser.append("точка находится слева от линии")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.setFixedSize(800, 800)
    window.show()
    sys.exit(app.exec_())

from lab7.pyFiles.logic import func3d
from lab7.pyFiles.math_parser import *
from lab7.pyFiles.functions import Function


if __name__ == '__main__':
    ranges = [[0, 2], [1, 5]]
    partitions = [100, 10]
    f = Function("x**2 + y - 1")
    result = func3d(ranges, partitions, f)
    print(result[1])
    # assert len(result) == (partitions[0] + 1) * (partitions[1] + 1)
    # assert result[0, 0] == 0 and result[0, 1] == 1
    #
    # partitions = [253, 14]
    # result = func3d(ranges, partitions, f)
    # assert len(result) == (partitions[0] + 1) * (partitions[1] + 1)
    #
    # print(result)
    # assert eval_expr("5**2") == 25
    # try:
    #     eval_expr("print('Hacked!')")
    #     assert 0 == 1
    # except Exception as e:
    #     pass
    # assert eval_expr("5 + 2 - 3 * 4") == -5
    # f = "x**2 - 10*y".replace("x", str(1)).replace("y", str(2))
    # assert eval_expr(f) == -19
    # f = "cos(x) - sin(y)".replace("x", str(1)).replace("y", str(2))
    # assert isinstance(eval_expr(f), float)
    # f = "tan(x) - ctan(y)".replace("x", str(1)).replace("y", str(2))
    # assert isinstance(eval_expr(f), float)
    # f = "(x + y)/(x - y)".replace("x", str(1)).replace("y", str(2))
    # assert eval_expr(f) == (1 + 2)/(1 - 2)

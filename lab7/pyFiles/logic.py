import os
os.sys.path.append("./../../")
from lab6.pyFiles.affine_transform import rotate_around_straight
from lab6.pyFiles.shapes.geometric_primitives import Point
from lab6.pyFiles.simple_plotter.simple_plotter import simple_plot_points
from lab7.pyFiles.functions import Function
import matplotlib.pyplot as plt
import numpy as np


def get_generatrix(fname: str) -> (int, np.ndarray):
    """
    Считывает точки образующей из файла
    :param fname: имя файла
    :return: количество разбиений: int образующая: np.ndarray
    """
    file = open(fname, 'r')
    partitions_cnt = int(file.readline())
    points = []
    for line in file:
        points.append([float(x) for x in line.replace(' ', '').replace('[', '').replace(']', '').split(',')])
    file.close()
    fig = np.array(points).reshape((1, len(points), 3))
    for i in range(0, 3):
        fig[0, :, i] -= np.min(fig[0, :, i])
    return partitions_cnt, fig


def rotation_figure(fname: str, axis: str) -> np.ndarray:
    """
    Строит фигуру вращения
    :param fname: имя файла
    :param axis: ось вращения in range = [X, Y, Z]
    :return: фигура вращения: np.ndarray
    """
    partitions_cnt, generatrix = get_generatrix(fname)
    angle = np.float64(360.0 / partitions_cnt)
    current_angle = np.float64(0.)
    fig = [np.copy(generatrix[0]).tolist()]
    pt1 = Point(0, 0, 0)
    if axis == 'X' or axis == 'x':
        pt2 = Point(1, 0, 0)
    elif axis == 'Y' or axis == 'y':
        pt2 = Point(0, 1, 0)
    elif axis == 'Z' or axis == 'z':
        pt2 = Point(0, 0, 1)
    else:
        raise AssertionError
    for i in range(0, partitions_cnt+1):
        current_angle += angle
        new_rotation = rotate_around_straight(generatrix, pt1, pt2, current_angle)[0].tolist()
        tmp = []
        for j in range(len(fig[i])):
            tmp.append(fig[i][j])
            if tmp[-1] != new_rotation[j]:
                tmp.append(new_rotation[j])
        fig[i] = tmp
        fig.append(rotate_around_straight(generatrix, pt1, pt2, current_angle)[0].tolist())
    return np.array(fig[:len(fig)-1])


def func3d(ranges: list, partitions: list, function: Function) -> np.ndarray:
    """
    результат функции от двух переменных для промежутков с шагами
    :param ranges: список промежутков для OX, OY
    :param partitions: список разбиений по осям
    :param function: функция от двух переменных
    :return: numpy.ndarray
    """
    steps = [(ranges[0][1] - ranges[0][0]) / partitions[0], (ranges[1][1] - ranges[1][0]) / partitions[1]]
    x = np.arange(ranges[0][0], ranges[0][1] + steps[0], steps[0])
    y = np.arange(ranges[1][0], ranges[1][1] + steps[1], steps[1])
    cartesian_product = np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])
    z = np.expand_dims(np.apply_along_axis(function, -1, cartesian_product), -1)
    return np.concatenate([cartesian_product, z], axis=-1)

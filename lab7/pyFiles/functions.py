from lab7.pyFiles.math_parser import eval_expr


class Function:
    def __init__(self, expr: str):
        self._expr = expr

    def __call__(self, values: list):
        return eval_expr(self._expr.replace("x", str(values[0])).replace("y", str(values[1])))

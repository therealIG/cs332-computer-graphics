import ast
import operator as op
import math

operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul, ast.Div: op.truediv, ast.Pow: op.pow,
             ast.BitXor: op.xor, ast.USub: op.neg}


def eval_expr(expr):
    return eval_(ast.parse(expr, mode="eval").body)


def eval_(node):
    if isinstance(node, ast.Num):
        return node.n
    elif isinstance(node, ast.BinOp):
        return operators[type(node.op)](eval_(node.left), eval_(node.right))
    elif isinstance(node, ast.UnaryOp):
        return operators[type(node.op)](eval_(node.operand))
    elif isinstance(node, ast.Call) and node.func.id == "sin":
        return math.sin(eval_(node.args[0]))
    elif isinstance(node, ast.Call) and node.func.id == "cos":
        return math.cos(eval_(node.args[0]))
    elif isinstance(node, ast.Call) and node.func.id == "tan":
        return math.tan(eval_(node.args[0]))
    elif isinstance(node, ast.Call) and node.func.id == "ctan":
        return 1/math.tan(eval_(node.args[0]))
    else:
        raise TypeError(node)

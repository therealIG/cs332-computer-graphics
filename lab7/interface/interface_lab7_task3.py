import sys
import matplotlib
import re

matplotlib.use('Qt5Agg')
import h5py
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from lab6.pyFiles.affine_transform import *
from lab6.pyFiles.projection import isometric_projection, orto_projection
from lab7.pyFiles.logic import func3d
from lab7.pyFiles.functions import Function


class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        QWidget.__init__(self)
        layout = QGridLayout()
        self.setLayout(layout)
        self.data = None
        self.setStyleSheet("background-color: gray;")
        layout.setSpacing(10)

        # Значения слайдеров
        self.value_shift_x = 0
        self.value_shift_y = 0
        self.value_shift_z = 0

        self.value_scale = 1
        self.value_scale_x = 1
        self.value_scale_y = 1
        self.value_scale_z = 1

        self.value_turn_x = 0
        self.value_turn_y = 0
        self.value_turn_z = 0
        # -------------------------------------------------------------------

        self.flag_for_display = 2
        self.value_for_scale = [1 / 4, 1 / 3, 1 / 2, 1, 2, 3, 4, 5]

        self.value_interval_x = None
        self.value_interval_y = None
        self.value_steps = None
        self.value_function = None
        # -------------------------------------------------------------------
        # --------------------------------------------------------------------------

        # Слайдеры смещения
        self.label_offset_x = QLabel(" Shift X")
        layout.addWidget(self.label_offset_x, 3, 0)
        self.slider_shift_x = QSlider(Qt.Horizontal)
        self.slider_shift_x.setFixedSize(300, 20)
        self.slider_shift_x.setRange(-50, 50)
        self.slider_shift_x.setValue(0)
        self.slider_shift_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_x.setTickInterval(5)
        self.slider_shift_x.setSingleStep(1)
        layout.addWidget(self.slider_shift_x, 4, 0)
        self.label_shift_x_value = QLabel(str(self.slider_shift_x.value()))
        layout.addWidget(self.label_shift_x_value, 4, 1)
        self.slider_shift_x.valueChanged.connect(self.update_slider_shift_x)
        self.label_shift_x_value.setFixedSize(27, 10)

        self.label_offset_y = QLabel(" Shift Y")
        layout.addWidget(self.label_offset_y, 5, 0)
        self.slider_shift_y = QSlider(Qt.Horizontal)
        self.slider_shift_y.setFixedSize(300, 20)
        self.slider_shift_y.setRange(-50, 50)
        self.slider_shift_y.setValue(0)
        self.slider_shift_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_y.setTickInterval(5)
        self.slider_shift_y.setSingleStep(1)
        layout.addWidget(self.slider_shift_y, 6, 0)
        self.label_shift_y_value = QLabel(str(self.slider_shift_y.value()))
        layout.addWidget(self.label_shift_y_value, 6, 1)
        self.slider_shift_y.valueChanged.connect(self.update_slider_shift_y)
        self.label_shift_y_value.setFixedSize(27, 10)

        self.label_offset_z = QLabel(" Shift Z")
        layout.addWidget(self.label_offset_z, 7, 0)
        self.slider_shift_z = QSlider(Qt.Horizontal)
        self.slider_shift_z.setFixedSize(300, 20)
        self.slider_shift_z.setRange(-50, 50)
        self.slider_shift_z.setValue(0)
        self.slider_shift_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_shift_z.setTickInterval(5)
        self.slider_shift_z.setSingleStep(1)
        layout.addWidget(self.slider_shift_z, 8, 0)
        self.label_shift_z_value = QLabel(str(self.slider_shift_z.value()))
        layout.addWidget(self.label_shift_z_value, 8, 1)
        self.slider_shift_z.valueChanged.connect(self.update_slider_shift_z)
        self.label_shift_z_value.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Слайдеры поворота
        self.label_turn_x = QLabel(" Turn X")
        layout.addWidget(self.label_turn_x, 9, 0)
        self.slider_turn_x = QSlider(Qt.Horizontal)
        self.slider_turn_x.setFixedSize(300, 20)
        self.slider_turn_x.setRange(-180, 180)
        self.slider_turn_x.setValue(0)
        self.slider_turn_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_x.setTickInterval(10)
        self.slider_turn_x.setSingleStep(1)
        layout.addWidget(self.slider_turn_x, 10, 0)
        self.label_turn_x_value = QLabel(str(self.slider_turn_x.value()))
        layout.addWidget(self.label_turn_x_value, 10, 1)
        self.slider_turn_x.valueChanged.connect(self.update_slider_turn_x)
        self.label_turn_x_value.setFixedSize(27, 10)

        self.label_turn_y = QLabel(" Turn Y")
        layout.addWidget(self.label_turn_y, 11, 0)
        self.slider_turn_y = QSlider(Qt.Horizontal)
        self.slider_turn_y.setFixedSize(300, 20)
        self.slider_turn_y.setRange(-180, 180)
        self.slider_turn_y.setValue(0)
        self.slider_turn_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_y.setTickInterval(10)
        self.slider_turn_y.setSingleStep(1)
        layout.addWidget(self.slider_turn_y, 12, 0)
        self.label_turn_y_value = QLabel(str(self.slider_turn_y.value()))
        layout.addWidget(self.label_turn_y_value, 12, 1)
        self.slider_turn_y.valueChanged.connect(self.update_slider_turn_y)
        self.label_turn_y_value.setFixedSize(27, 10)

        self.label_turn_z = QLabel(" Turn Z")
        layout.addWidget(self.label_turn_z, 13, 0)
        self.slider_turn_z = QSlider(Qt.Horizontal)
        self.slider_turn_z.setFixedSize(300, 20)
        self.slider_turn_z.setRange(-180, 180)
        self.slider_turn_z.setValue(0)
        self.slider_turn_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_turn_z.setTickInterval(10)
        self.slider_turn_z.setSingleStep(1)
        layout.addWidget(self.slider_turn_z, 14, 0)
        self.label_turn_z_value = QLabel(str(self.slider_turn_z.value()))
        layout.addWidget(self.label_turn_z_value, 14, 1)
        self.slider_turn_z.valueChanged.connect(self.update_slider_turn_z)
        self.label_turn_z_value.setFixedSize(27, 10)
        # --------------------------------------------------------------------------

        # Слайдеры масшатибрования
        self.label_scale = QLabel(" Scale ")
        layout.addWidget(self.label_scale, 15, 0)
        self.slider_scale = QSlider(Qt.Horizontal)
        self.slider_scale.setFixedSize(300, 20)
        self.slider_scale.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale.setRange(0, 7)
        self.slider_scale.setValue(3)
        self.slider_scale.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale.setTickInterval(1)
        self.slider_scale.setSingleStep(1)
        layout.addWidget(self.slider_scale, 16, 0)
        self.label_scale_value = QLabel(str(self.value_for_scale[self.slider_scale.value()]))
        layout.addWidget(self.label_scale_value, 16, 1)
        self.slider_scale.valueChanged.connect(self.update_slider_scale)
        self.label_scale_value.setFixedSize(27, 10)

        self.label_scale_x = QLabel(" Scale X")
        layout.addWidget(self.label_scale_x, 17, 0)
        self.slider_scale_x = QSlider(Qt.Horizontal)
        self.slider_scale_x.setFixedSize(300, 20)
        self.slider_scale_x.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_x.setRange(0, 7)
        self.slider_scale_x.setValue(3)
        self.slider_scale_x.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_x.setTickInterval(1)
        self.slider_scale_x.setSingleStep(1)
        layout.addWidget(self.slider_scale_x, 18, 0)
        self.label_scale_value_x = QLabel(str(self.value_for_scale[self.slider_scale_x.value()]))
        layout.addWidget(self.label_scale_value_x, 18, 1)
        self.slider_scale_x.valueChanged.connect(self.update_slider_scale_x)
        self.label_scale_value_x.setFixedSize(27, 10)

        self.label_scale_y = QLabel(" Scale Y")
        layout.addWidget(self.label_scale_y, 19, 0)
        self.slider_scale_y = QSlider(Qt.Horizontal)
        self.slider_scale_y.setFixedSize(300, 20)
        self.slider_scale_y.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_y.setRange(0, 7)
        self.slider_scale_y.setValue(3)
        self.slider_scale_y.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_y.setTickInterval(1)
        self.slider_scale_y.setSingleStep(1)
        layout.addWidget(self.slider_scale_y, 20, 0)
        self.label_scale_value_y = QLabel(str(self.value_for_scale[self.slider_scale_y.value()]))
        layout.addWidget(self.label_scale_value_y, 20, 1)
        self.slider_scale_y.valueChanged.connect(self.update_slider_scale_y)
        self.label_scale_value_y.setFixedSize(27, 10)

        self.label_scale_z = QLabel(" Scale Z")
        layout.addWidget(self.label_scale_z, 21, 0)
        self.slider_scale_z = QSlider(Qt.Horizontal)
        self.slider_scale_z.setFixedSize(300, 20)
        self.slider_scale_z.setFocusPolicy(Qt.StrongFocus)
        self.slider_scale_z.setRange(0, 7)
        self.slider_scale_z.setValue(3)
        self.slider_scale_z.setTickPosition(QSlider.TicksBothSides)
        self.slider_scale_z.setTickInterval(1)
        self.slider_scale_z.setSingleStep(1)
        layout.addWidget(self.slider_scale_z, 22, 0)
        self.label_scale_value_z = QLabel(str(self.value_for_scale[self.slider_scale_z.value()]))
        layout.addWidget(self.label_scale_value_z, 22, 1)
        self.slider_scale_z.valueChanged.connect(self.update_slider_scale_z)
        self.label_scale_value_z.setFixedSize(27, 10)
        # -------------------------------------------------------------------------

        # Кнопка сохранения
        self.save_button = QPushButton(" Save ")
        self.save_button.setFixedSize(90, 50)
        self.save_button.clicked.connect(self.save)
        layout.addWidget(self.save_button, 23, 0)
        # --------------------------------------------------------------------------

        # Кнопка загрузки
        self.load_button = QPushButton(" Load ")
        self.load_button.setFixedSize(90, 50)
        self.load_button.clicked.connect(self.showDialog)
        layout.addWidget(self.load_button, 24, 0)
        # --------------------------------------------------------------------------

        # Canvas
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        layout.addWidget(self.canvas, 0, 2, 27, 9)
        # --------------------------------------------------------------------------

        self.label_function = QLabel(" Function ")
        layout.addWidget(self.label_function, 25, 1)
        self.text_function = QLineEdit()
        self.text_function.setStyleSheet("QLineEdit { background-color : white; color : black; }")
        self.text_function.setFixedSize(100, 30)
        self.text_function.editingFinished.connect(self.update_text_function)
        layout.addWidget(self.text_function, 25, 0)

        self.label_interval_x = QLabel(" Interval: X0,X1 ")
        layout.addWidget(self.label_interval_x, 26, 1)
        self.text_x = QLineEdit()
        self.text_x.setStyleSheet("QLineEdit { background-color : white; color : black; }")
        self.text_x.setFixedSize(100, 30)
        self.text_x.editingFinished.connect(self.update_text_x)
        layout.addWidget(self.text_x, 26, 0)

        self.label_interval_y = QLabel(" Interval: Y0,Y1 ")
        layout.addWidget(self.label_interval_y, 27, 1)
        self.text_y = QLineEdit()
        self.text_y.setStyleSheet("QLineEdit { background-color : white; color : black; }")
        self.text_y.setFixedSize(100, 30)
        self.text_y.editingFinished.connect(self.update_text_y)
        layout.addWidget(self.text_y, 27, 0)

        self.label_count = QLabel(" Number of partitions ")
        layout.addWidget(self.label_count, 28, 1)
        self.text_count = QLineEdit()
        self.text_count.setStyleSheet("QLineEdit { background-color : white; color : black; }")
        self.text_count.setFixedSize(100, 30)
        self.text_count.editingFinished.connect(self.update_count)
        layout.addWidget(self.text_count, 28, 0)

        self.label_projection = QLabel(" Type of projection ")
        layout.addWidget(self.label_projection, 27, 5)

        self.type_projection = None

        self.list_widget = QListWidget()
        self.list_widget.setStyleSheet("QLineEdit { background-color : white; color : black; }")
        self.list_widget.addItems(["isometric_projection", "orto_projection"])
        self.list_widget.setFixedSize(150, 50)
        self.list_widget.itemActivated.connect(self.itemActivated_event)
        layout.addWidget(self.list_widget, 28, 5)

        self.corner_first = 0
        self.corner_second = 0
        self.axis = 0

        self.label_value_projection = QLabel(" Value for projection ")
        layout.addWidget(self.label_value_projection, 27, 6)
        self.text_value_projection = QLineEdit()
        self.text_value_projection.setStyleSheet("QLineEdit { background-color : white; color : black; }")
        self.text_value_projection.setFixedSize(200, 30)
        self.text_value_projection.editingFinished.connect(self.update_value_projection)
        layout.addWidget(self.text_value_projection, 28, 6)

        self.text_function.setText(" None ")
        self.text_x.setText("0,0")
        self.text_y.setText("0,0")
        self.text_count.setText("0,0")
        self.text_value_projection.setText(" None ")

    def plot(self):
        """
        Выбор типа изображения (2D or 3D) и вызов соотвествующей функции
        :return: None
        """
        if self.flag_for_display == 2:
            self.plot_2D()
        elif self.flag_for_display == 3:
            self.plot_3D()

    def plot_3D(self):
        """
        Отрисовка изображения в 3D
        :return: None
        """
        self.figure.clear()
        ax = self.figure.add_subplot(111, projection='3d')
        X = self.data[:, :, 0]
        Y = self.data[:, :, 1]
        Z = self.data[:, :, 2]
        ax.scatter3D(X, Y, Z)
        ax.add_collection3d(Poly3DCollection(self.data, facecolors='cyan', linewidths=0.5, edgecolors='r', alpha=.25))
        ax.set_xlim3d(-50, 50)
        ax.set_ylim3d(-50, 50)
        ax.set_zlim3d(-50, 50)
        ax.set_zticks(np.arange(-50, 60, 10))
        ax.set_yticks(np.arange(-50, 60, 10))
        ax.set_xticks(np.arange(-50, 60, 10))
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        self.canvas.draw()

    def plot_2D(self):
        """
        Отрисовка изображения в 2D
        :return: None
        """
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        shape_on_perspective = isometric_projection(self.data, 90, 0)
        if self.type_projection == "isometric_projection":
            shape_on_perspective = isometric_projection(self.data, int(self.corner_first), int(self.corner_second))
            ax.add_collection(
                PolyCollection(shape_on_perspective[:, :, :2], facecolors='cyan', linewidths=1, edgecolors='r'))
            ax.set_xticks(np.arange(-50, 60, 10))
            ax.set_yticks(np.arange(-50, 60, 10))
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
        elif self.type_projection == "orto_projection":
            shape_on_perspective = orto_projection(self.data, int(self.axis))[:, :, :3]
            if int(self.axis) == 0:
                ax.add_collection(PolyCollection(shape_on_perspective[:, :, 1:], facecolors='cyan', linewidths=1, edgecolors='r'))
                ax.set_xticks(np.arange(-50, 60, 10))
                ax.set_yticks(np.arange(-50, 60, 10))
                ax.set_xlabel('Y')
                ax.set_ylabel('Z')
            if int(self.axis) == 1:
                ax.add_collection(PolyCollection(shape_on_perspective[:, :, ::2], facecolors='cyan', linewidths=1, edgecolors='r'))
                ax.set_xticks(np.arange(-50, 60, 10))
                ax.set_yticks(np.arange(-50, 60, 10))
                ax.set_xlabel('X')
                ax.set_ylabel('Z')
            if int(self.axis) == 2:
                ax.add_collection(PolyCollection(shape_on_perspective[:, :, :2], facecolors='cyan', linewidths=1, edgecolors='r'))
                ax.set_xticks(np.arange(-50, 60, 10))
                ax.set_yticks(np.arange(-50, 60, 10))
                ax.set_xlabel('X')
                ax.set_ylabel('Y')

        self.canvas.draw()

    def save(self):
        """
        Создание Диалогового окна для задания имени сохраняемого файла
        :return: None
        """
        try:
            text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter file name for save:')
            if ok:
                with h5py.File('../data/task3/' + str(text) + '.h5', 'w') as hf:
                    hf.create_dataset("test", data=self.data)
        except TypeError:
            pass

    def onClicked(self):
        """
        Переключение между типами отрисовки изображения ( 2D or 3D )
        :return: None
        """
        radioButton = self.sender()
        if radioButton.isChecked():
            if radioButton.country == "3D":
                self.flag_for_display = 3
                if self.data is not None:
                    self.plot()
            if radioButton.country == "2D":
                self.flag_for_display = 2
                if self.data is not None:
                    self.plot()

    def update_slider_turn_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_turn_x_value.setText(str(self.slider_turn_x.value()))
            self.data = rotate_around_x(self.data, self.value_turn_x - self.slider_turn_x.value())
            self.value_turn_x = self.slider_turn_x.value()
            self.plot()

    def update_slider_turn_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_turn_y_value.setText(str(self.slider_turn_y.value()))
            self.data = rotate_around_y(self.data, self.value_turn_y - self.slider_turn_y.value())
            self.value_turn_y = self.slider_turn_y.value()
            self.plot()

    def update_slider_turn_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_turn_z_value.setText(str(self.slider_turn_z.value()))
            self.data = rotate_around_z(self.data, self.value_turn_z - self.slider_turn_z.value())
            self.value_turn_z = self.slider_turn_z.value()
            self.plot()

    def update_slider_shift_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_shift_x_value.setText(str(self.slider_shift_x.value()))
            self.data = shift_x(self.data, self.slider_shift_x.value() - self.value_shift_x)
            self.value_shift_x = self.slider_shift_x.value()
            self.plot()

    def update_slider_shift_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_shift_y_value.setText(str(self.slider_shift_y.value()))
            self.data = shift_y(self.data, self.slider_shift_y.value() - self.value_shift_y)
            self.value_shift_y = self.slider_shift_y.value()
            self.plot()

    def update_slider_shift_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_shift_z_value.setText(str(self.slider_shift_z.value()))
            self.data = shift_z(self.data, self.slider_shift_z.value() - self.value_shift_z)
            self.value_shift_z = self.slider_shift_z.value()
            self.plot()

    def update_slider_scale(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_scale_value.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.label_scale_value_x.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.label_scale_value_y.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.label_scale_value_z.setText(str(self.value_for_scale[self.slider_scale.value()]))
            self.data = scale_center(self.data, self.value_for_scale[self.slider_scale.value()] / self.value_scale)
            self.value_scale = self.value_for_scale[self.slider_scale.value()]
            self.value_scale_x = self.value_for_scale[self.slider_scale.value()]
            self.value_scale_y = self.value_for_scale[self.slider_scale.value()]
            self.value_scale_z = self.value_for_scale[self.slider_scale.value()]
            self.slider_scale_x.setValue(self.slider_scale.value())
            self.slider_scale_y.setValue(self.slider_scale.value())
            self.slider_scale_z.setValue(self.slider_scale.value())
            self.plot()

    def update_slider_scale_x(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_scale_value_x.setText(str(self.value_for_scale[self.slider_scale_x.value()]))
            self.data = scale_x(self.data, self.value_for_scale[self.slider_scale_x.value()] / self.value_scale_x)
            self.value_scale_x = self.value_for_scale[self.slider_scale_x.value()]
            self.plot()

    def update_slider_scale_y(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_scale_value_y.setText(str(self.value_for_scale[self.slider_scale_y.value()]))
            self.data = scale_y(self.data, self.value_for_scale[self.slider_scale_y.value()] / self.value_scale_y)
            self.value_scale_y = self.value_for_scale[self.slider_scale_y.value()]
            self.plot()

    def update_slider_scale_z(self):
        """
        Обновление значений слайдер и обработка нового значения
        :return: None
        """
        if self.data is not None:
            self.label_scale_value_z.setText(str(self.value_for_scale[self.slider_scale_z.value()]))
            self.data = scale_z(self.data, self.value_for_scale[self.slider_scale_z.value()] / self.value_scale_z)
            self.value_scale_z = self.value_for_scale[self.slider_scale_z.value()]
            self.plot()

    def showDialog(self):
        """
        Создание Диалогового окна для выбора файла и обнуление значений переменных/ слайдеров
        :return: None
        """
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', '../data/task3/')[0]
            with h5py.File(fname, 'r') as hf:
                self.data = hf['test'][:]
                # Обнуление значений слайдеров
                self.value_shift_x = 0
                self.value_shift_y = 0
                self.value_shift_z = 0
                self.slider_shift_x.setValue(0)
                self.slider_shift_y.setValue(0)
                self.slider_shift_z.setValue(0)

                self.value_scale = 1
                self.value_scale_x = 1
                self.value_scale_y = 1
                self.value_scale_z = 1
                self.slider_scale.setValue(3)
                self.slider_scale_x.setValue(3)
                self.slider_scale_y.setValue(3)
                self.slider_scale_z.setValue(3)

                self.value_turn_x = 0
                self.value_turn_y = 0
                self.value_turn_z = 0
                self.slider_turn_x.setValue(0)
                self.slider_turn_y.setValue(0)
                self.slider_turn_z.setValue(0)

                self.text_function.setText(" None ")
                self.text_x.setText("0,0")
                self.text_y.setText("0,0")
                self.text_count.setText("0,0")
                self.value_interval_x = None
                self.value_interval_y = None
                self.value_function = None
                self.value_steps = None

                self.value_for_scale = [1 / 4, 1 / 3, 1 / 2, 1, 2, 3, 4, 5]
                # -------------------------------------------------------------------
                self.plot()
        except FileNotFoundError:
            pass
        except ValueError:
            pass

    def update_text_function(self):
        self.value_function = Function(self.text_function.text().replace(" ", ""))
        if self.value_interval_y is not None and self.value_interval_x is not None and self.value_steps is not None and self.value_function is not None:
            self.data = func3d([self.value_interval_x, self.value_interval_y], self.value_steps, self.value_function)
            self.data = np.expand_dims(self.data, axis=0)
            self.plot()

    def update_count(self):
        if re.fullmatch(r'[-]?\d+,[-]?\d+', self.text_y.text()):
            self.value_steps = [int(self.text_count.text().split(",")[0]), int(self.text_count.text().split(",")[1])]
            if self.value_interval_y is not None and self.value_interval_x is not None and self.value_steps is not None and self.value_function is not None:
                self.data = func3d([self.value_interval_x, self.value_interval_y], self.value_steps,
                                   self.value_function)
                self.data = np.expand_dims(self.data, axis=0)
                self.plot()

    def update_text_x(self):
        if re.fullmatch(r'[-]?\d+,[-]?\d+', self.text_y.text()):
            self.value_interval_x = [int(self.text_x.text().split(",")[0]), int(self.text_x.text().split(",")[1])]
            if self.value_interval_y is not None and self.value_interval_x is not None and self.value_steps is not None and self.value_function is not None:
                self.data = func3d([self.value_interval_x, self.value_interval_y], self.value_steps,
                                   self.value_function)
                self.data = np.expand_dims(self.data, axis=0)
                self.plot()

    def update_text_y(self):
        if re.fullmatch(r'[-]?\d+,[-]?\d+', self.text_y.text()):
            self.value_interval_y = [int(self.text_y.text().split(",")[0]), int(self.text_y.text().split(",")[1])]
            if self.value_interval_y is not None and self.value_interval_x is not None and self.value_steps is not None and self.value_function is not None:
                self.data = func3d([self.value_interval_x, self.value_interval_y], self.value_steps,
                                   self.value_function)
                self.data = np.expand_dims(self.data, axis=0)
                self.plot()

    def update_value_projection(self):
        try:
            if self.type_projection is not None:
                if self.type_projection == "isometric_projection":
                    if re.fullmatch(r'[-]?\d+,[-]?\d+', self.text_value_projection.text()):
                        self.corner_first = self.text_value_projection.text().split(",")[0]
                        self.corner_second = self.text_value_projection.text().split(",")[1]
                elif self.type_projection == "orto_projection":
                    if self.text_value_projection.text() in ["0", "1", "2"]:
                        self.axis = self.text_value_projection.text()
            self.plot()
        except TypeError:
            self.text_value_projection.setText(" None")
        except AttributeError:
            self.text_value_projection.setText(" None")

    def itemActivated_event(self, item):
        try:
            self.type_projection = item.text()
            self.plot()
        except AttributeError:
            pass


app = QApplication(sys.argv)
screen = Window()
screen.setGeometry(0, 0, 900, 900)
screen.show()
sys.exit(app.exec_())
